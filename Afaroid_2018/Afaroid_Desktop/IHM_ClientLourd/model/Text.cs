
/// <summary>
/// Classe representant un texte sous forme d'une ressource.
/// Represente �galement un enregistrement dans la table Resources
/// </summary>
public class Text : Resource
{
    /// <summary>
    /// Constructeur par defaut : cr�e une instance de type texte
    /// </summary>
    public Text()
    {
        this.Type = RC_TYPE.TEXT;
    }
    /// <summary>
    /// Constructeur avec argument
    /// </summary>
    /// <param name="idResource">L'identifiant de la ressource dans la base de donn�es</param>
    /// <param name="type">Le type de la ressource. Dans ce cas, le type est texte</param>
    /// <param name="textToken">Chemin vers la ressource</param>
    public Text(int idResource, RC_TYPE type, string textToken)
    {
        this.IDresource = idResource;
        this.Type = type;
        this.TextToken = textToken;
        this.ListOfTextTranslations = getTextTranslation();
    }

}
