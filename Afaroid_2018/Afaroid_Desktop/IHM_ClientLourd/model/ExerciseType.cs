using System;
public enum ExerciseType {
    FINDING_IMAGE = 2,
    CHOOSING_WORD = 1,
    MATCHING_WORD = 3,
    LISTENING_CHOOSING = 4,
    LISTENING_WRITING = 5
}
