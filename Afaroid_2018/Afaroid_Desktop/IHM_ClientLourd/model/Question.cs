using MySql.Data.MySqlClient;
using System.Collections.Generic;

public class Question
{

    private int idQuestion;
    private string answer;

    private Exercise exercise;
    private List<Resource> resources = new List<Resource>();
    private StatePush statePush = StatePush.NONE;

    /*
     * Properties
     * */
    public int IDquestion
    {
        get { return idQuestion; }
    }

    public string Answer
    {
        get { return answer; }
        set { answer = value; }
    }

    public StatePush STATEPush
    {
        get { return statePush; }
        set { statePush = value; }
    }
    public List<Resource> MyResource
    {
        get { return resources; }
        set { resources = value; }
    }

    public Exercise Exercice
    {
        get { return exercise; }
        set { exercise = value; }
    }

    /*
     * Constructors
     * */

    public Question(int id, string answer, List<Resource> resources)
    {
        this.idQuestion = id;
        this.answer = answer;
        this.resources = resources;
    }

    public Question(int id, string answer)
    {
        this.idQuestion = id;
        this.answer = answer;
    }

    /*
     * Public methods
     * */
    public void AddResource(Resource rc)
    {
        rc.STATEPush = StatePush.NEW;
        resources.Add(rc);
        rc.ListOfQuestions.Add(this);

    }
    public void DeleteResource(int idResource)
    {
        foreach (Resource rc in resources)
        {
            if (rc.IDresource == idResource)
            {
                foreach (Question qst in rc.ListOfQuestions)
                {
                    if (qst.IDquestion == this.idQuestion)
                    {
                        qst.STATEPush = StatePush.DELETE;
                        rc.ListOfQuestions.Remove(qst);
                    }
                }
                rc.STATEPush = StatePush.DELETE;
                resources.Remove(rc);

            }
        }

    }

    public void pushResource(Resource rc)
    {
        rc.add();
    }

    public List<Resource> getListOfResources()
    {

        // Database connection 
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("select * from Questions where QuestionID = " + this.idQuestion,
                                                     MyConnection);

            myReader = myCommand.ExecuteReader();

            List<int> QuestionsIDList = new List<int>();
            while (myReader.Read())
            {
                QuestionsIDList.Add((int)myReader["QuestionID"]);
            }
            myReader.Close();

            for (int i = 0; i < QuestionsIDList.Count; i++)
            {

                MySqlDataReader ResourceReader = null;
                MySqlCommand QuestionResourcesCommand = new MySqlCommand("select * from Resources where Resources.ResourceID IN (select Resources_ResourceID from QuestionsHasResources where Questions_QuestionID = " + QuestionsIDList[i] + ")",
                                                         MyConnection);
                ResourceReader = QuestionResourcesCommand.ExecuteReader();

                while (ResourceReader.Read())
                {

                    switch ((int)ResourceReader["ResourceTypes_ResourceTypeID"])
                    {
                        case 3:
                            Resource myText = new Text((int)ResourceReader["ResourceID"], RC_TYPE.TEXT, null);
                            //  myText.ListOfTextTranslations = myText.getTextTranslation();
                            resources.Add(myText);
                            break;
                        case 2:
                            Resource myImage = new Image((int)ResourceReader["ResourceID"], RC_TYPE.IMAGE, (string)ResourceReader["TextToken"]);
                            //   myImage.ListOfTextTranslations = myImage.getTextTranslation();
                            resources.Add(myImage);
                            break;
                        case 1:
                            Resource myAudio = new Audio((int)ResourceReader["ResourceID"], RC_TYPE.AUDIO, (string)ResourceReader["TextToken"]);
                            //  myAudio.ListOfTextTranslations = myAudio.getTextTranslation();
                            resources.Add(myAudio);
                            break;
                        default:
                            break;
                    }

                }
                ResourceReader.Close();

            }
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
        return resources;
    }
}
