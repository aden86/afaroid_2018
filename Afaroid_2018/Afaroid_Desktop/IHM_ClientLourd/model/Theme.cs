using MySql.Data.MySqlClient;
using System.Collections.Generic;
/// <summary>
/// Classe represantant la table theme. Elle permet de la modifier 
/// en ajoutant, modifiant ou supprimant des membres.
/// </summary>
public class Theme
{

    /*
     * Members
     * */
    /// <summary>
    /// La liste des sous-themes de ce theme
    /// </summary>

    private List<SubTheme> listOfSubThemes = new List<SubTheme>();
    /// <summary>
    /// L'identifiant du theme dans la base de données
    /// </summary>
    private int idTheme;
    /// <summary>
    /// Le nom du theme en français
    /// </summary>
    private string name;
    /// <summary>
    /// L'etat du theme pour savoir s'il est nouveau, existant, mis à jour ou supprimé
    /// </summary>
    private StatePush statePush = StatePush.NONE;
    /// <summary>
    /// L'image du theme pour l'application mobile
    /// </summary>

    private Image image;
    private TreeAfaroid treeAfaroid;

    /*
     * Properties
     * */

    /// <summary>
    /// Obtient ou definit l'identifiant du theme
    /// </summary>
    public int IdTheme { get { return idTheme; } set { idTheme = value; } }
    /// <summary>
    /// Obtient ou definit le nom du theme
    /// </summary>
    public string Name { get { return name; } set { name = value; } }
    /// <summary>
    /// Permet d'obtenir l'etat du theme,
    /// si c'est un nouveau theme, un theme ajouté, un theme mis à jour, ou un theme où aucune modification
    /// n'a était faite.
    /// Permet aussi de le modfier
    /// </summary>
    public StatePush MyStatePush { get { return statePush; } set { statePush = value; } }
    /// <summary>
    /// Obtient ou definit la ressource image du theme
    /// </summary>
    public Image Image { get { return image; } set { image = value; } }
    /// <summary>
    /// Obtient ou definit la liste des sous-themes
    /// </summary>
    public List<SubTheme> ListOfSubThemes { get { return listOfSubThemes; } set { listOfSubThemes = value; } }
    /// <summary>
    /// Permet d'obtenir son parent.
    /// Permet aussi de le modfier
    /// </summary>
    public TreeAfaroid TreeAfaroid { get { return treeAfaroid; } set { treeAfaroid = value; } }

    /*
     * Constructors
     * */

    /// <summary>
    /// Constructeur de la classe
    /// </summary>
    public Theme() { }

    /// <summary>
    /// Constructeur de la classe avec arguments
    /// </summary>
    /// <param name="id"> l'identifiant du theme</param>
    /// <param name="name">le nom du theme</param>
    public Theme(int id, string name)
    {
        this.idTheme = id;
        this.name = name;
    }

    /// <summary>
    /// Constructeur de la classe avec arguments
    /// </summary>
    /// <param name="id">l'identifiant du theme</param>
    /// <param name="name">le nom du theme</param>
    /// <param name="image">L'image ressource du theme</param>
    public Theme(int id, string name, Image image)
    {
        this.idTheme = id;
        this.name = name;
        this.image = image;
    }

    /*
     * Public Methods
     * */
    /// <summary>
    /// Permet de récuperer de la liste des sous theme du theme courant.
    /// Permet le remplissage de ces sous theme avec leurs ressources et text translation depuis la base de données
    /// </summary>
    /// <returns></returns>

    public List<SubTheme> GetListOfSubThemes()
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("select * from SubThemes where Themes_ThemeID=" + this.idTheme,
                                                     MyConnection);
            myReader = myCommand.ExecuteReader();
            List<SubThemeDB> SubThemeDBList = new List<SubThemeDB>();
            while (myReader.Read())
            {
                SubThemeDB st;
                st.SubThemeID = (int)myReader["SubThemeID"];
                st.ResourceID = (int)myReader["Resources_ResourceID"];
                SubThemeDBList.Add(st);
            }
            myReader.Close();

            for (int i = 0; i < SubThemeDBList.Count; i++)
            {
                MySqlDataReader myImageRead = null;
                MySqlCommand myImage = new MySqlCommand("select * from Resources where Resources.ResourceID = "
                                                    + SubThemeDBList[i].ResourceID,
                                                     MyConnection);
                string SubThemeName = null;

                myImageRead = myImage.ExecuteReader();
                Image MyImageSubTheme = null;

                ResourceDB rc = new ResourceDB();
                while (myImageRead.Read())
                {
                    rc.ResourceID = (int)myImageRead["ResourceID"];
                    rc.TextToken = myImageRead["TextToken"].ToString();
                    rc.TextTranslations_TextID = (int)myImageRead["TextTranslations_TextID"];
                }
                myImageRead.Close();

                MyImageSubTheme = new Image(rc.ResourceID, RC_TYPE.IMAGE, rc.TextToken);

                MySqlDataReader SubThemeNameReader = null;
                MySqlCommand SubThemeNameCommand = new MySqlCommand("select * from TextTranslations "
                    + " where TextID = " + rc.TextTranslations_TextID
                    , MyConnection);

                SubThemeNameReader = SubThemeNameCommand.ExecuteReader();

                while (SubThemeNameReader.Read())
                {
                    // On prends que le nom subtheme en fran�ais
                    if ((int)SubThemeNameReader["Languages_LanguageID"] == (int)Language.FR)
                    {
                        SubThemeName = (string)SubThemeNameReader["Translation"];
                    }

                }
                SubTheme st = new SubTheme(SubThemeDBList[i].SubThemeID, SubThemeName, MyImageSubTheme);
                st.monTheme = this;
                SubThemeNameReader.Close();
                st.ListOfCourses = st.GetListOfCourses();
                listOfSubThemes.Add(st);

            }
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }


        return listOfSubThemes;
    }


    /// <summary>
    /// Permet d'ajouter un sous-theme dans l'application
    /// </summary>
    /// <param name="st">Le sous theme à ajouter dans l'application</param>

    public void AddSubTheme(SubTheme st)
    {
        st.MyStatePush = StatePush.NEW;
        st.monTheme = this;
        listOfSubThemes.Add(st);
    }
    /// <summary>
    /// Permet d'inserer un sous-theme dans la base de données
    /// </summary>
    /// <param name="st">Le sous theme à inserer</param>

    public void PushSubTheme(SubTheme st)
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            string query = "INSERT INTO `subthemes`(`SubThemeID`, `Themes_ThemeID`, `Resources_ResourceID`) VALUES(NULL,'" + this.idTheme + "','" + st.Image.IDresource + "')";

            MySqlCommand myCommand = new MySqlCommand(query, MyConnection);

            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

    }
    /// <summary>
    /// Permet d'ajouter une image au theme
    /// </summary>
    /// <param name="img">L'image à rajouter</param>

    public void AddImage(Image img)
    {
        image = img;
    }


    /// <summary>
    /// Permet de mettre à jour le sous-theme dans la base de données
    /// </summary>
    /// <param name="st">le sous-theme à modifier</param>
    public void UpdateSubTheme(SubTheme st)
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlCommand myCommand = new MySqlCommand("update SubThemes set  Themes_ThemeID = " + this.idTheme
                + ", Resources_ResourceID =" + st.Image.IDresource +
                " where SubThemes.SubThemeID =  " + st.IdSubTheme,
                MyConnection);
            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

        st.MyStatePush = StatePush.UPDATE;
    }
    /// <summary>
    /// Permet  de suprimmer le sous-theme de la base de données et ses enfant en cascade (cours , ressources)
    /// </summary>
    /// <param name="st">le sous-theme à supprimer</param>
    public void DeleteSubTheme(SubTheme st)
    {
        // Supression des cours associés au subtheme
        foreach (Course course in st.ListOfCourses)
        {
            if (course.MyStatePush == StatePush.NONE)
            {
                st.DeleteCourse(course);
            }

        }

        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("delete from SubThemes where SubThemeID =" + st.IdSubTheme,
                                                     MyConnection);
            myReader = myCommand.ExecuteReader();
            myReader.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

        st.MyStatePush = StatePush.DELETE;
    }


    /// <summary>
    /// Permet de mettre à jour le theme
    /// </summary>
    public void update()
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlCommand myCommand = new MySqlCommand("update Themes set  Resources_ResourceID = " + this.image.IDresource
                 + " where ThemeID =  " + this.idTheme,
                MyConnection);
            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
        this.statePush = StatePush.UPDATE;
    }


    /// <summary>
    /// Une structure utilisé dans le code interne pour stocker des informations concernant la table Resources afin d'effectuer
    /// des jointures
    /// </summary>
    private struct ResourceDB
    {
        public int ResourceID;
        public string TextToken;
        public int TextTranslations_TextID;
    }

    /// <summary>
    /// Une structure utilisé dans le code interne pour stocker des informations concernant la table subTheme afin d'effectuer
    /// des jointures
    /// </summary>
    private struct SubThemeDB
    {
        public int SubThemeID;
        public int ResourceID;
    }
}







