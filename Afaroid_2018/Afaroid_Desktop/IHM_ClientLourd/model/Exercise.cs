using MySql.Data.MySqlClient;
using System.Collections.Generic;

public class Exercise
{


    /* 
     * Members 
     */
    private int idExercice;
    private StatePush statePush = StatePush.NONE;
    private ExerciseType type;
    private List<Question> listOfQuestions = new List<Question>();
    private Course course;


    /*
     * 
     * Constructors
     */

    public Exercise(int id, ExerciseType type)
    {
        this.idExercice = id;
        this.type = type;
    }
    public Exercise(int id, ExerciseType type, List<Question> listOfQuestions)
    {
        this.idExercice = id;
        this.type = type;
        this.listOfQuestions = listOfQuestions;
    }

    /*  
     * Properties 
     *  
     * */
    public int IdExercice
    {
        get { return idExercice; }
        set { idExercice = value; }
    }
    public StatePush STATEPush
    {
        get { return statePush; }
        set { statePush = value; }
    }

    public ExerciseType Type
    {
        get { return type; }
        set { type = value; }
    }

    public List<Question> ListOfQuestions
    {
        get { return listOfQuestions; }
        set { listOfQuestions = value; }
    }

    public Course Course
    {
        get { return course; }
        set { course = value; }
    }
    /* 
     * Public Methods 
     * */

    public void AddQuestion(Question qst)
    {
        qst.STATEPush = StatePush.NEW;
        qst.Exercice = this;
        listOfQuestions.Add(qst);
    }


    public List<Question> GetListOfQuestions()
    {

        // Database connexion 
        try
        {

            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader QuestionsRead = null;
            MySqlCommand ExerciceQuestionsCommand = new MySqlCommand("select * from Questions where Exercises_ExerciseID =" + this.idExercice,
                                                 MyConnection);

            QuestionsRead = ExerciceQuestionsCommand.ExecuteReader();
            List<Resource> ExerciceQuestions = new List<Resource>();

            List<QuestionDB> QuestionDBList = new List<QuestionDB>();
            while (QuestionsRead.Read())
            {
                QuestionDB qst;
                qst.idQuestion = (int)QuestionsRead["QuestionID"];
                qst.answer = (string)QuestionsRead["Answer"];
                QuestionDBList.Add(qst);
            }
            QuestionsRead.Close();

            for (int i = 0; i < QuestionDBList.Count; i++)
            {
                Question qst = new Question(QuestionDBList[i].idQuestion, QuestionDBList[i].answer);
                qst.MyResource = qst.getListOfResources();
                listOfQuestions.Add(qst);
            }

        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

        return listOfQuestions;
    }

    public void pushQuestion(Question qst)
    {

        // Database connexion 
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader Reader = null;
            MySqlCommand ExerciceQuestionsCommand = new MySqlCommand("INSERT INTO Questions values('" + this.idExercice + "','" + qst.Answer + "')"
                , MyConnection);

            Reader = ExerciceQuestionsCommand.ExecuteReader();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

    }

    public void updateQuestion(Question qst)
    {
        // Database connexion 
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader Reader = null;
            MySqlCommand ExerciceQuestionsCommand = new MySqlCommand("update Questions set Answer = '" + qst.Answer
                + "' where QuestionID =" + qst.IDquestion
                + " AND Exercises_ExerciseID =" + this.idExercice
                , MyConnection);

            Reader = ExerciceQuestionsCommand.ExecuteReader();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
    }

    public void deleteQuestion(Question qst)
    {
        // Database connexion 
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            MySqlDataReader Reader1 = null;
            MySqlCommand ExerciceQuestionsCommand1 = new MySqlCommand("delete from QuestionsHasResources where Questions_QuestionID ="
                + qst.IDquestion, MyConnection);

            Reader1 = ExerciceQuestionsCommand1.ExecuteReader();
            Reader1.Close();

            MySqlDataReader Reader2 = null;
            MySqlCommand ExerciceQuestionsCommand2 = new MySqlCommand("delete from Questions where QuestionID = "
                + qst.IDquestion, MyConnection);

            Reader2 = ExerciceQuestionsCommand2.ExecuteReader();
            Reader2.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
    }

    private struct QuestionDB
    {
        public int idQuestion;
        public string answer;
    }

}