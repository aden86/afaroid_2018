using MySql.Data.MySqlClient;
using System.Threading;

/// <summary>
/// Classe representant un singleton pour se connecter � la base de donnees de l'application
/// </summary>
public sealed class DBSingleton
{
    /// <summary>
    /// l'url du serveur 
    /// </summary>
    private static string Server = "localhost";
    /// <summary>
    /// le nom de la base de donnees
    /// </summary>
    private static string Database = "afaroiddb";
    /// <summary>
    /// l'identifiant de l'utilisateur
    /// </summary>
    private static string ID = "root";
    /// <summary>
    /// Mot de passe
    /// </summary>
    private static string PASS = "";

    /// <summary>
    /// Parametres de la connexion
    /// </summary>
    static string conf = "Server=" + Server + ";Database=" + Database + ";Uid=" + ID + ";Pwd=" + PASS;
    static MySqlConnection db;

    /// <summary>
    /// Obtient une connexion � la base de donnees
    /// </summary>
    public static MySqlConnection Connection
    {
        get
        {
            if (db == null)
            {
                LazyInitializer.EnsureInitialized(ref db, CreateConnection);
            }
            return db;
        }
    }

    /// <summary>
    /// Permet de changer la connexion
    /// </summary>
    /// <param name="s">l'url du serveur</param>
    /// <param name="id">l'identifiant de l'utilisateur</param>
    /// <param name="pw">le mot de passe</param>
    public static void setConn(string s, string dbname, string id, string pw)
    {
        Server = s;
        Database = dbname;
        ID = id;
        PASS = pw;
    }

    /// <summary>
    /// Permet de cr�er une connexion � la base donn�es
    /// </summary>
    /// <returns>retourne une connexion Mysql si les parametres de la connexion sont valides</returns>
    static MySqlConnection CreateConnection()
    {
        var db = new MySqlConnection(conf);
        db.Open();
        return db;
    }
}
