﻿using MySql.Data.MySqlClient;
using System.Windows;


/// <summary>
/// Represente les differentes traductions dans les differentes langues d'une resource (Image ou Audio).
/// Represente également un enregistrement de la table TextTranslations dans la base de données
/// </summary>
public class TextTranslations
{
    /// <summary>
    /// Identifiant de la traduction dans la base de données.
    /// </summary>
    private int textTranslationID;
    /// <summary>
    /// Le lien entre la resource et sa traduction dans les differentes langues.
    /// Il est identique pour toutes les traductions d'un même mot.
    /// </summary>
    private int textID;
    /// <summary>
    /// La traduction de la resource.
    /// </summary>
    private string translation;
    /// <summary>
    /// Le language dans lequel la resource est traduite.
    /// </summary>
    private Language language;

    /*
     * Proprietés
     * */

    /// <summary>
    /// Obtient ou definit l'dentifiant de la traduction dans la base de données.
    /// </summary>
    public int TextTranslationID { get { return textTranslationID; } set { textTranslationID = value; } }
    /// <summary>
    /// Obtient ou definit le lien entre la resource et sa traduction dans les differentes langues.
    /// </summary>
    public int TextID { get { return textID; } set { textID = value; } }
    /// <summary>
    /// Obtient ou definit la traduction de la resource.
    /// </summary>
    public string Translation { get { return translation; } set { translation = value; } }
    /// <summary>
    /// Obtient ou definit le language dans lequel la resource est traduite.
    /// </summary>
    public Language Language { get { return language; } set { language = value; } }

    /*
     * Constructeurs
     * */

    /// <summary>
    /// Constructeur : Creer une traduction d'une resource dans un language
    /// </summary>
    /// <param name="textTranslationID">l'identifiant de la traduction dans la base de données</param>
    /// <param name="textID">le lien entre la resource et sa traduction </param>
    /// <param name="translation">la traduction de la resource</param>
    /// <param name="language">le language de la traduction</param>
    public TextTranslations(int textTranslationID, int textID, string translation, Language language)
    {
        this.textTranslationID = textTranslationID;
        this.textID = textID;
        this.translation = translation;
        this.language = language;
    }

    /*
     * Public methods
     * */
    /// <summary>
    /// Permet d'inserer la traduction dans la base données 
    /// </summary>
    public void add()
    {
        try
        {
            // Database connexion 
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();

            MySqlCommand myCommand = new MySqlCommand("INSERT INTO texttranslations(`TextTranslationID`, `TextID`, `Languages_LanguageID`, `Translation`) VALUES (NULL,\"" + this.textID + "\",\"" + (int)this.language + "\",\"" + this.translation + "\")"
                , MyConnection);

            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
            MessageBox.Show("fail to insert textTranslation ! :" + exp.ToString() + "                  ");
        }

    }
    /// <summary>
    /// Permet de modifier la traduction dans la base de données 
    /// </summary>
    internal void update()
    {
        // Database connexion 
        try
        {

            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("UPDATE texttranslations set  `TextID`='" + this.textID + "', `Languages_LanguageID` = '" + (int)this.language + "', `Translation`= \"" + this.translation + "\" WHERE `TextTranslationID` = " + this.textTranslationID
                , MyConnection);

            myReader = myCommand.ExecuteReader();
            myCommand.Connection.Close();
            MyConnection.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
            MessageBox.Show("fail to insert textTranslation ! :" + exp.ToString() + "                  ");
        }
    }
}