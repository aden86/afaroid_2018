using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows;

/// <summary>
/// Classe abstraite represantant une resource de l'application, elle peut �tre un texte, audio ou une image
/// Represente �galement un enregistrement dans la table Resources
/// </summary>
[Serializable]
public abstract class Resource
{
    /*
     * Members
     * */

    /// <summary>
    /// L'identifiant de la ressource dans la base de donn�es
    /// </summary>
    private int idResource;
    /// <summary>
    /// Le chemin vers la ressource
    /// </summary>
    private string textToken;
    /// <summary>
    /// Le type de la ressource (Image, Texte ou Audio)
    /// </summary>
    private RC_TYPE type;
    /// <summary>
    /// L'�tat de la ressource dans l'application, elle peut �tre NEW :  nouvelle ressource, UPDATE : Ressource modifi�,
    /// DELETE : Ressource supprim�.
    /// Par d�faut c'est NONE, c'est � dire qu'elle est dans le serveur
    /// </summary>
    private StatePush statePush = StatePush.NONE;
    private List<Question> listOfQuestions;
    private List<Course> listOfCourses;
    /// <summary>
    /// La liste des traductions de la ressource dans les differents languages
    /// </summary>
    private List<TextTranslations> listOfTextTranslations = new List<TextTranslations>();


    /* 
     * Properties
     * */


    /// <summary>
    /// Obtient ou definit l'identifiant de la ressource dans la base de donn�es
    /// </summary>
    public int IDresource { get { return idResource; } set { idResource = value; } }

    /// <summary>
    /// Obtient ou definit le chemin vers la ressource
    /// </summary>
    public string TextToken { get { return textToken; } set { textToken = value; } }

    /// <summary>
    /// Obtient ou definit Le type de la ressource (Image, Texte ou Audio)
    /// </summary>
    public RC_TYPE Type { get { return type; } set { type = value; } }

    /// <summary>
    /// Obtient ou definit l'�tat de la ressource dans l'application
    /// </summary>
    public StatePush STATEPush { get { return statePush; } set { statePush = value; } }

    public List<Question> ListOfQuestions { get { return listOfQuestions; } set { listOfQuestions = value; } }

    public List<Course> ListOfCourses { get { return listOfCourses; } set { listOfCourses = value; } }

    /// <summary>
    /// Obtient ou definit la liste des traductions de la ressource dans les differents languages
    /// </summary>
    public List<TextTranslations> ListOfTextTranslations { get { return listOfTextTranslations; } set { listOfTextTranslations = value; } }


    /*
     * 
     * Public Methods
     * */



    public Resource clone()
    {
        return (Resource)this.MemberwiseClone();
    }


    /// <summary>
    /// Permet d'inserer une ressource avec toutes ses traductions dans les differents languages dans la base de donn�es
    /// </summary>
    public void add()
    {
        // Database connexion 
        this.listOfTextTranslations[0].TextID = maxIDinDB("texttranslations", "TextID") + 1;
        MySqlConnection MyConnection = DBSingleton.Connection;
        if (MyConnection.State == System.Data.ConnectionState.Closed)
            MyConnection.Open();
        for (int i = 1; i < Enum.GetNames(typeof(Language)).Length; i++)
        {
            this.ListOfTextTranslations[i].TextID = this.listOfTextTranslations[0].TextID;
        }
        foreach (TextTranslations txttrs in this.listOfTextTranslations)
        {
            txttrs.add();
        }
        try
        {
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            this.textToken = this.textToken.Replace("\\", "\\\\");
            MySqlCommand myCommand = new MySqlCommand("INSERT INTO Resources(`ResourceID`, `TextToken`, `ResourceTypes_ResourceTypeID`, `TextTranslations_TextID`) VALUES (NULL,'" + this.textToken + "','" + (int)this.type + "','" + this.listOfTextTranslations[0].TextID + "')"
                , MyConnection);
            myReader = myCommand.ExecuteReader();
            myCommand.Connection.Close();
            MyConnection.Close();
            this.textToken = this.textToken.Replace("\\\\", "\\");

            System.IO.File.AppendAllText("cache.txt", this.textToken + "\r\n");

        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
            MessageBox.Show("fail to insert resources ! :" + exp.ToString() + "                  ");
        }
    }


    /// <summary>
    /// Permet de modifier la resource avec toutes ses traductions dans les differents languages dans 
    /// la base de donn�es
    /// </summary>
    public void updateCascade()
    {
        // Database connection 
        foreach (TextTranslations txttrs in this.listOfTextTranslations)
        {
            txttrs.update();
        }
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            this.textToken = this.textToken.Replace("\\", "\\\\");
            MySqlCommand myCommand = new MySqlCommand("update Resources set TextToken = \"" + this.textToken
                + "\",ResourceTypes_ResourceTypeID = '" + (int)this.type
                + "',TextTranslations_TextID = '" + this.listOfTextTranslations[0].TextID
                + "' where ResourceID =" + this.idResource
                , MyConnection);

            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();
            this.textToken = this.textToken.Replace("\\\\", "\\");
            System.IO.File.AppendAllText("cache.txt", this.textToken + "\r\n");

        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
    }

    /// <summary>
    /// Permet de modifier uniquement la ressource sans ses differentes traductions dans la base donn�es
    /// </summary>
    public void update()
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();

            MySqlCommand myCommand = new MySqlCommand("update Resources set TextToken = \"" + this.textToken
                + "\",ResourceTypes_ResourceTypeID = '" + (int)this.type
                + "',TextTranslations_TextID = '" + this.listOfTextTranslations[0].TextID
                + "' where ResourceID =" + this.idResource
                , MyConnection);

            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();
            System.IO.File.AppendAllText("cache.txt", this.textToken + "\r\n");


        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
    }

    /// <summary>
    /// Permet de supprimer uniquement la ressource dans la base de donn�es
    /// </summary>
    public void delete()
    {
        // Database connection 
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlCommand myCommand = new MySqlCommand("delete from Resources where ResourceID = " + this.idResource,
                                                     MyConnection);

            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

    }

    /// <summary>
    /// Permet d'obtenir les traductions de la ressource dans les differents languages
    /// </summary>
    /// <returns>retourne une liste de TextTranslations contenant les traductions de la ressource </returns>
    public List<TextTranslations> getTextTranslation()
    {
        // Database connection 
        try
        {

            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("select * from Resources where ResourceID = " + this.idResource,
                                                     MyConnection);

            myReader = myCommand.ExecuteReader();
            List<int> resourceTypeList = new List<int>();
            while (myReader.Read())
            {
                resourceTypeList.Add((int)myReader["TextTranslations_TextID"]);
            }
            myReader.Close();

            for (int i = 0; i < resourceTypeList.Count; i++)
            {
                MySqlDataReader TextTranslationReader = null;
                MySqlCommand TextTranslationCommand = new MySqlCommand("select * from TextTranslations where TextID = " + resourceTypeList[i],
                                                    MyConnection);
                TextTranslationReader = TextTranslationCommand.ExecuteReader();

                while (TextTranslationReader.Read())
                {
                    ListOfTextTranslations.Add(new TextTranslations((int)TextTranslationReader["TextTranslationID"],
                                                                    (int)TextTranslationReader["TextID"],
                                                                     (string)TextTranslationReader["Translation"],
                                                                     (Language)TextTranslationReader["Languages_LanguageID"]));
                }
                TextTranslationReader.Close();
            }
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

        return ListOfTextTranslations;
    }

    /// <summary>
    /// Permet de modifier les traductions de la ressource dans la base de donn�es
    /// </summary>
    public void updateTextTranslations()
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            foreach (TextTranslations trans in listOfTextTranslations)
            {
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();
                MySqlCommand myCommand = new MySqlCommand("UPDATE TextTranslations SET TextID = '" + trans.TextID
                    + "',Languages_LanguageID = '" + (int)trans.Language
                    + "',Translation = \"" + trans.Translation
                    + "\" where TextTranslationID =" + trans.TextTranslationID
                    , MyConnection);
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
            }

        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

    }

    /// <summary>
    /// Fonction priv� de la classe permettant d'obtenir le maximum d'une colonne dans une table dans la base
    /// de donn�es. Elle sera utilis� pour recuperer l'identifiant du dernier enregistrement dans une table.
    /// </summary>
    /// <param name="tableName">le nom de la table</param>
    /// <param name="colonneName">le nom de la colonne</param>
    /// <returns>retourne -1 en cas d'erreur, sinon elle retourne la max de la colonne sp�cifi� en parametre de la
    /// fonction</returns>
    private int maxIDinDB(string tableName, string colonneName)
    {
        int max = 1;
        try
        {

            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            string query = "SELECT MAX(" + colonneName + ") AS MAXID FROM " + tableName;
            MySqlCommand myCommand = new MySqlCommand(query, MyConnection);
            myReader = myCommand.ExecuteReader();
            if (myReader.Read())
            {
                if (!myReader["MAXID"].ToString().Equals(""))
                    max = (int)myReader["MAXID"];
            }
            myReader.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
        return max;
    }

}
