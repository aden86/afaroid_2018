﻿using CustomForm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace IHM_ClientLourd
{
    /// <summary>
    /// Classe Permet de parcourir les fichies depuis l'explorateur windows. Les fichiers autorisé à parcourir
    /// sont les fichiers texte, les images et les audios mp3 
    /// </summary>
    public class FolderBrowserDialogExampleForm
    {
        //private FolderBrowserDialog folderBrowserDialog1;
        private Image image;
        public Image Image { get { return image; } }
        private OpenFileDialog openFileDialog1;
        public OpenFileDialog OpenFileDialog1 { get { return openFileDialog1; } set { openFileDialog1 = value; } }
        LanguageIHM putTrad;
        public int SPACER { get; private set; }
        public List<Resource> ListesOfResOfCourse { get { return listesOfResOfCourse; } }
        private int textImageAudio;
        //private RichTextBox richTextBox1;



        private string openFileName, folderName;
        public Text textadd;
        private bool fileOpened = false;
        FlowLayoutPanel flowLayoutPanel1;
        private List<Resource> listesOfResOfCourse;
        private List<TextTranslations> listTextadtranslation;
        private Image imgadd;
        private Audio audioadd;
        private Course newCourse;
        private Audio audio;

        public Audio Audio { get { return audio; } }


        // Constructor.
        public FolderBrowserDialogExampleForm(FlowLayoutPanel flowLayoutPanel1, List<Resource> listesOfResOfCourse)
        {
            this.flowLayoutPanel1 = flowLayoutPanel1;
            this.listesOfResOfCourse = listesOfResOfCourse;
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.Filter = "files (*.txt)|*.txt|(*.png)|*.png|(*.jpg)|*.jpg|(*.mp3)|*.mp3";
            this.openMenuItem_Click();
        }
        // Constructor.
        public FolderBrowserDialogExampleForm(Image image)
        {
            this.image = image;
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.Filter = "files (*.png)|*.png|(*.jpg)|*.jpg";
            this.openMenuItem_Click_image();
        }
        public FolderBrowserDialogExampleForm(Image image, bool edit)
        {
            this.image = image;
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.Filter = "files (*.png)|*.png|(*.jpg)|*.jpg";
            this.openMenuItem_Click_image_edit();
        }

        public FolderBrowserDialogExampleForm(FlowLayoutPanel flowLayoutPanel1, Course newCourse)
        {
            this.flowLayoutPanel1 = flowLayoutPanel1;
            this.newCourse = newCourse;
            this.listesOfResOfCourse = newCourse.ListOfResources;
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.Filter = "files (*.txt)|*.txt|(*.png)|*.png|(*.jpg)|*.jpg|(*.mp3)|*.mp3";
            this.openMenuItem_Click();
        }

        // Bring up a dialog to open a file.
        private void openMenuItem_Click()
        {

            // If a file is not opened, then set the initial directory to the
            // FolderBrowserDialog.SelectedPath value.

            // Display the openFile dialog.
            DialogResult result = openFileDialog1.ShowDialog();

            // OK button was pressed.
            if (result == DialogResult.OK)
            {
                openFileName = openFileDialog1.FileName;
                try
                {

                    // Output the requested file in flowLayoutPanel1.
                    Stream s = openFileDialog1.OpenFile();
                    string directory = openFileDialog1.FileName;

                    Content content = new Content(newCourse);
                    Content previous;

                    flowLayoutPanel1.Controls.Add(content);
                    if (flowLayoutPanel1.Controls.Count < 2)
                    {
                        content.Location = new Point(0, 0);
                    }
                    else
                    {
                        previous = (Content)flowLayoutPanel1.Controls[flowLayoutPanel1.Controls.Count - 2];
                        if (flowLayoutPanel1.Controls.Count > 2)
                        {
                            flowLayoutPanel1.Height += content.Size.Height + SPACER;
                        }

                        content.Location = new Point(0, previous.Location.Y + previous.Height + SPACER);
                    }
                    content.onRemoveSite += new Content.RemoveSiteEventHandler(RemoveSite_Click);
                    content.index = flowLayoutPanel1.Controls.Count - 1;
                    content.Width = flowLayoutPanel1.Width;
                    content.Anchor = ((System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top
                        | System.Windows.Forms.AnchorStyles.Left
                        | System.Windows.Forms.AnchorStyles.Right));
                    //flowLayoutPanel1.LoadFile(s, RichTextBoxStreamType.PlainText);
                    content.AppendMyText(Environment.NewLine + DateTime.Today + directory);
                    FileAttributes fileAttributes = File.GetAttributes(directory);
                    //File.Copy(filepath, "\\\\192.168.1.28\\Files");
                    s.Close();
                    Regex r = new Regex("(.txt)");
                    Match m = r.Match(directory);
                    if (m.Success)
                    {
                        textImageAudio = 3;
                        string[] lines = System.IO.File.ReadAllLines(directory);
                        textadd = new Text(0, RC_TYPE.TEXT, string.Join("\r\n", lines));
                        textadd.STATEPush = StatePush.NEW;
                        TextTranslations textadtranslation = new TextTranslations(0, textadd.IDresource, textadd.TextToken, Language.FR);
                        List<TextTranslations> listTextadtranslation = new List<TextTranslations>();
                        listTextadtranslation.Add(textadtranslation);
                        textadd.ListOfTextTranslations = listTextadtranslation;
                        putTrad = new LanguageIHM(textadd);
                        textadd.TextToken = directory;
                        content.AddTxt(textadd);


                    }
                    else
                    {
                        r = new Regex("(.png)|(.PNG)|(.jpg)");
                        m = r.Match(directory);
                        if (m.Success)
                        {
                            textImageAudio = 2;
                            imgadd = new Image(0, RC_TYPE.IMAGE, directory);
                            imgadd.STATEPush = StatePush.NEW;
                            TextTranslations textadtranslation = new TextTranslations(0, imgadd.IDresource, imgadd.TextToken, Language.FR);
                            List<TextTranslations> listTextadtranslation = new List<TextTranslations>();
                            listTextadtranslation.Add(textadtranslation);
                            imgadd.ListOfTextTranslations = listTextadtranslation;
                            putTrad = new LanguageIHM(imgadd);
                            content.AddImg(imgadd);

                        }
                        else
                        {
                            textImageAudio = 1;
                            r = new Regex("(.mp3)");
                            m = r.Match(directory);
                            if (m.Success)
                            {
                                audioadd = new Audio(0, RC_TYPE.AUDIO, directory);
                                audioadd.STATEPush = StatePush.NEW;
                                TextTranslations textadtranslation = new TextTranslations(0, audioadd.IDresource, audioadd.TextToken, Language.FR);
                                List<TextTranslations> listTextadtranslation = new List<TextTranslations>();
                                listTextadtranslation.Add(textadtranslation);
                                audioadd.ListOfTextTranslations = listTextadtranslation;
                                putTrad = new LanguageIHM(audioadd);
                                content.AddAudio(audioadd);

                            }
                        }
                    }
                    putTrad.Show();
                    switch (textImageAudio)
                    {
                        case 3:
                            content.AddTxt(textadd);
                            listesOfResOfCourse.Add(textadd);
                            break;
                        case 2:
                            content.AddImg(imgadd);
                            listesOfResOfCourse.Add(imgadd);
                            break;
                        case 1:
                            content.AddAudio(audioadd);
                            listesOfResOfCourse.Add(audioadd);
                            break;
                        default:
                            break;
                    }


                    fileOpened = true;

                }
                catch (Exception exp)
                {
                    MessageBox.Show("An error occurred while attempting to load the file. The error is:"
                                    + System.Environment.NewLine + exp.ToString() + System.Environment.NewLine);
                    fileOpened = false;
                }
                //Invalidate();

                //closeMenuItem.Enabled = fileOpened;
            }

            // Cancel button was pressed.
            else if (result == DialogResult.Cancel)
            {
                return;
            }
        }

        // Constructor.

        public FolderBrowserDialogExampleForm(Audio audio)

        {

            this.audio = audio;

            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();

            this.openFileDialog1.DefaultExt = "txt";

            this.openFileDialog1.Filter = "files (*.mp3)|*.mp3|(*.wav)|*.wav";

            this.openMenuItem_Click_audio();

        }
        private void openMenuItem_Click_audio()

        {

            DialogResult result = openFileDialog1.ShowDialog();

            // OK button was pressed.

            if (result == DialogResult.OK)

            {

                openFileName = openFileDialog1.FileName;
                try
                {
                    // Output the requested file in flowLayoutPanel1.
                    Stream s = openFileDialog1.OpenFile();
                    string directory = openFileDialog1.FileName;
                    s.Close();
                    audio.TextToken = directory;
                    //image.STATEPush = StatePush.NEW;
                    fileOpened = true;
                }

                catch (Exception exp)
                {
                    MessageBox.Show("An error occurred while attempting to load the file. The error is:"

                                    + System.Environment.NewLine + exp.ToString() + System.Environment.NewLine);

                    fileOpened = false;

                }

            }

            // Cancel button was pressed.

            else if (result == DialogResult.Cancel)

            {

                return;

            }

        }



        private void openMenuItem_Click_image()
        {
            DialogResult result = openFileDialog1.ShowDialog();
            // OK button was pressed.
            if (result == DialogResult.OK)
            {
                openFileName = openFileDialog1.FileName;
                try
                {
                    // Output the requested file in flowLayoutPanel1.
                    Stream s = openFileDialog1.OpenFile();
                    string directory = openFileDialog1.FileName;

                    s.Close();
                    image = new Image(0, RC_TYPE.IMAGE, directory);
                    //image.STATEPush = StatePush.NEW;
                    fileOpened = true;

                }
                catch (Exception exp)
                {
                    MessageBox.Show("An error occurred while attempting to load the file. The error is:"
                                    + System.Environment.NewLine + exp.ToString() + System.Environment.NewLine);
                    fileOpened = false;
                }
            }
            // Cancel button was pressed.
            else if (result == DialogResult.Cancel)
            {
                return;
            }
        }
        private void openMenuItem_Click_image_edit()
        {
            DialogResult result = openFileDialog1.ShowDialog();
            // OK button was pressed.
            if (result == DialogResult.OK)
            {
                openFileName = openFileDialog1.FileName;
                try
                {
                    // Output the requested file in flowLayoutPanel1.
                    Stream s = openFileDialog1.OpenFile();
                    string directory = openFileDialog1.FileName;
                    s.Close();
                    image.TextToken = directory;
                    fileOpened = true;

                }
                catch (Exception exp)
                {
                    MessageBox.Show("An error occurred while attempting to load the file. The error is:"
                                    + System.Environment.NewLine + exp.ToString() + System.Environment.NewLine);
                    fileOpened = false;
                }
            }
            // Cancel button was pressed.
            else if (result == DialogResult.Cancel)
            {
                return;
            }
        }
        private void RemoveSite_Click(Object sender, ContentArgs e)
        {
            Content content = (Content)sender;
            Content updateList;

            for (int i = e.index; i < flowLayoutPanel1.Controls.Count; i++)
            {
                updateList = (Content)flowLayoutPanel1.Controls[i];
                updateList.Location = new Point(0, updateList.Location.Y - updateList.Height - SPACER);
                updateList.index = i - 1;
            }
            flowLayoutPanel1.Controls.RemoveAt(e.index);
            flowLayoutPanel1.Height -= content.Height;
        }
        // Close the current file.
        private void closeMenuItem_Click(object sender, System.EventArgs e)
        {
            //flowLayoutPanel1.Text = "";
            fileOpened = false;
        }



    }
}
