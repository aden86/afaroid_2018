using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

/// <summary>
/// Classe representant un sous-theme. Represente egalement un enregistrement de la table Subtheme dans la base de donn�es
/// </summary>
public class SubTheme
{
    /*
     * Members
     * */


    /// <summary>
    /// L'identifiant dans la base de donn�es du sous-theme
    /// </summary>
    private int idSubTheme;
    /// <summary>
    /// Le nom du sous-theme en fran�ais
    /// </summary>

    private string name;
    /// <summary>
    /// La liste des cours de ce sous-theme
    /// </summary>
    private List<Course> listOfCourses = new List<Course>();
    /// <summary>
    /// L'etat du sous theme
    /// </summary>
    private StatePush statePush = StatePush.NONE;
    /// <summary>
    /// L'image ressource associ� au sous-theme
    /// </summary>
    private Image image;
    /// <summary>
    /// Le theme parent de ce sous-theme
    /// </summary>
    private Theme theme;




    /*
     * Properties
     * */


    /// <summary>
    /// Obtient ou definit l'identifiant du sous-theme
    /// </summary>
    public int IdSubTheme { get { return idSubTheme; } set { idSubTheme = value; } }
    /// <summary>
    /// Obtient ou definit le nom du sous-theme
    /// </summary>
    public string Name { get { return name; } set { name = value; } }
    /// <summary>
    /// Permet d'obtenir l'etat du sous-theme
    /// si c'est un nouveau theme, un theme ajout�, 
    /// un theme mis � jour, ou un theme o� aucune modification n'a �tait faite.
    /// Permet aussi de le modifier
    /// </summary>
    public StatePush MyStatePush { get { return statePush; } set { statePush = value; } }
    /// <summary>
    /// Obtient ou definit la ressource image du sous-theme
    /// </summary>
    public Image Image { get { return image; } set { image = value; } }
    /// <summary>
    /// Obtient ou definit son parent
    /// </summary>
    public Theme monTheme { get { return theme; } set { theme = value; } }

    public List<Course> ListOfCourses { get { return listOfCourses; } set { listOfCourses = value; } }
    /*
     * Constructors
     * */

    /// <summary>
    /// Constructeur de la classe
    /// </summary>
    public SubTheme() { }

    /// <summary>
    /// Constructeur de la classe avec arguments
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    public SubTheme(int id, string name)
    {
        this.idSubTheme = id;
        this.name = name;
    }

    /// <summary>
    /// Constructeur de la classe
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    /// <param name="image"></param>
    public SubTheme(int id, string name, Image image)
    {
        this.idSubTheme = id;
        this.image = image;
        this.name = name;
    }
    /*
     * Public Methods
     * */


    /// <summary>
    ///  Permet la recuperation de la liste des cours
    /// </summary>
    /// <returns></returns>
    public List<Course> GetListOfCourses()
    {
        // Database Connection
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("select * from Courses where SubThemes_SubThemeID =" + this.idSubTheme,
                                                     MyConnection);

            myReader = myCommand.ExecuteReader();

            List<CourseDB> courseDBList = new List<CourseDB>();
            while (myReader.Read())
            {
                CourseDB course;
                course.idCourse = (int)myReader["CourseID"];
                course.difficulty = (Difficulty)(myReader["Difficulties_DifficultyID"]);
                if ((int)(SByte)myReader["IsDynamic"] == 1)
                    course.isDynamic = true;
                else
                    course.isDynamic = false;
                courseDBList.Add(course);
            }
            myReader.Close();

            for (int i = 0; i < courseDBList.Count; i++)
            {

                MySqlDataReader ResoucesOfCourseRead = null;
                MySqlCommand ResoucesOfCourse = new MySqlCommand("select * from Resources where Resources.ResourceID IN (select Resources_ResourceID from CoursesHasResources where Courses_CourseID = " + courseDBList[i].idCourse + ")",
                                                     MyConnection);

                ResoucesOfCourseRead = ResoucesOfCourse.ExecuteReader();

                List<Resource> MyResourceCourseList = new List<Resource>();
                List<ResourceDB> resourceDBList = new List<ResourceDB>();
                while (ResoucesOfCourseRead.Read())
                {
                    ResourceDB rc;
                    rc.ResourceID = (int)ResoucesOfCourseRead["ResourceID"];
                    rc.TextToken = (string)ResoucesOfCourseRead["TextToken"];
                    rc.ResourceType = (int)ResoucesOfCourseRead["ResourceTypes_ResourceTypeID"];
                    resourceDBList.Add(rc);
                }
                ResoucesOfCourseRead.Close();

                for (int j = 0; j < resourceDBList.Count; j++)
                {

                    switch (resourceDBList[j].ResourceType)
                    {
                        case 3:
                            MyResourceCourseList.Add(new Text(resourceDBList[j].ResourceID, RC_TYPE.TEXT, resourceDBList[j].TextToken));
                            break;
                        case 2:
                            MyResourceCourseList.Add(new Image(resourceDBList[j].ResourceID, RC_TYPE.IMAGE, resourceDBList[j].TextToken));
                            break;
                        case 1:
                            MyResourceCourseList.Add(new Audio(resourceDBList[j].ResourceID, RC_TYPE.AUDIO, resourceDBList[j].TextToken));
                            break;
                        default:
                            break;
                    }
                }




                MySqlDataReader ExercisesOfCourseRead = null;
                MySqlCommand ExercisesOfCourse = new MySqlCommand("select * from Exercises where Exercises.Courses_CourseId = "
                                                        + courseDBList[i].idCourse, MyConnection);

                ExercisesOfCourseRead = ExercisesOfCourse.ExecuteReader();
                List<Exercise> MyExercisesCourse = new List<Exercise>();
                List<ExeriseDB> exerciseDBList = new List<ExeriseDB>();
                while (ExercisesOfCourseRead.Read())
                {
                    ExeriseDB ex;
                    ex.exerciseID = (int)ExercisesOfCourseRead["ExerciseID"];
                    ex.exerciseType = (ExerciseType)ExercisesOfCourseRead["ExerciseTypes_ExerciseTypeID"];
                    exerciseDBList.Add(ex);
                }
                ExercisesOfCourseRead.Close();

                for (int k = 0; k < exerciseDBList.Count; k++)
                {
                    switch ((int)exerciseDBList[k].exerciseType)
                    {
                        case 2:
                            Exercise findImage = new Exercise(exerciseDBList[k].exerciseID, ExerciseType.FINDING_IMAGE);
                            findImage.ListOfQuestions = findImage.GetListOfQuestions();
                            MyExercisesCourse.Add(findImage);
                            break;
                        case 4:
                            Exercise listenAndChoose = new Exercise(exerciseDBList[k].exerciseID, ExerciseType.LISTENING_CHOOSING);
                            listenAndChoose.ListOfQuestions = listenAndChoose.GetListOfQuestions();
                            MyExercisesCourse.Add(listenAndChoose);
                            break;
                        case 5:
                            Exercise listenAndWrite = new Exercise(exerciseDBList[k].exerciseID, ExerciseType.LISTENING_WRITING);
                            listenAndWrite.ListOfQuestions = listenAndWrite.GetListOfQuestions();
                            MyExercisesCourse.Add(listenAndWrite);
                            break;
                        case 1:
                            Exercise readAndChoose = new Exercise(exerciseDBList[k].exerciseID, ExerciseType.CHOOSING_WORD);
                            readAndChoose.ListOfQuestions = readAndChoose.GetListOfQuestions();
                            MyExercisesCourse.Add(readAndChoose);
                            break;
                        case 3:
                            Exercise wordConnection = new Exercise(exerciseDBList[k].exerciseID, ExerciseType.MATCHING_WORD);
                            wordConnection.ListOfQuestions = wordConnection.GetListOfQuestions();
                            MyExercisesCourse.Add(wordConnection);
                            break;
                        default:
                            break;
                    }
                }

                Course c = new Course(courseDBList[i].idCourse, courseDBList[i].difficulty, courseDBList[i].isDynamic, MyResourceCourseList);
                c.MySubTheme = this;
                listOfCourses.Add(c);
            }


        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

        return listOfCourses;
    }
    /// <summary>
    /// Permet l'ajout d'un cours dans l'application
    /// </summary>
    /// <param name="course"></param>

    public void AddCourse(Course course)
    {
        course.MyStatePush = StatePush.NEW;
        course.MySubTheme = this;
        listOfCourses.Add(course);
    }
    /// <summary>
    /// Permet d'associer une image � ce sous-theme
    /// </summary>
    /// <param name="img"></param>
    public void AddImage(Image img)
    {
        this.image = img;
    }
    /// <summary>
    ///  Permet d'ajouter un cours dans la base de donn�es local
    /// </summary>
    /// <param name="course"></param>
    public void PushCourse(Course course)
    {
        if (course.MyStatePush == StatePush.NEW)
        {


            //Insertion des nouveaux resources li� au cours, dans la base de donn�es
            foreach (Resource item in course.ListOfResources)
            {
                if (item.STATEPush == StatePush.NEW)
                    course.PushResource(item);
            }

            // Database connexion
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();
                int i = course.IsDynamic ? 1 : 0;
                MySqlCommand myCommand = new MySqlCommand("INSERT INTO `courses` (`CourseID`, `SubThemes_SubThemeID`, `Difficulties_DifficultyID`, `IsDynamic`) VALUES (NULL,'" + this.idSubTheme + "','" + (int)course.Mydifficulty + "','" + i + "')"
                                                            , MyConnection);
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString() + "                  " + (int)course.Mydifficulty);
            }
            try
            {
                course.IdCourse = maxIDinDB("Courses", "CourseID");
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString() + "                  " + (int)course.Mydifficulty);
            }

        }
    }
    /// <summary>
    ///  Permet la mise a jour du cours dans la base de donn�es
    /// </summary>
    public void UpdateCourse(Course course)
    {
        List<Resource> ListofdeletedResources = new List<Resource>();
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlCommand myCommand = new MySqlCommand("update Courses set  SubThemes_SubThemeID = " + this.idSubTheme
                + ", Difficulties_DifficultyID = " + (int)course.Mydifficulty +
                " where CourseID =  " + course.IdCourse,
                MyConnection);

            myCommand.ExecuteNonQuery();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }


        foreach (Resource item in course.ListOfResources)
        {
            if (item.STATEPush == StatePush.NEW)
            {
                item.add();
                item.IDresource = maxIDinDB("Resources", "ResourceID");
                try
                {
                    MySqlConnection MyConnection = DBSingleton.Connection;
                    if (MyConnection.State == System.Data.ConnectionState.Closed)
                        MyConnection.Open();

                    MySqlCommand myCommand = new MySqlCommand("INSERT INTO CoursesHasResources(`Courses_CourseID`, `Resources_ResourceID`, `OrderRes`) VALUES (" + course.IdCourse + ",\"" + item.IDresource + "\", NULL)"
                        , MyConnection);
                    myCommand.ExecuteNonQuery();
                    myCommand.Connection.Close();
                    MyConnection.Close();
                }
                catch (MySqlException exp)
                {
                    System.Console.WriteLine(exp);
                    MessageBox.Show("fail to insert ! :" + exp.ToString());
                }

            }
            else if (item.STATEPush == StatePush.DELETE)
            {
                try
                {
                    MySqlConnection MyConnection = DBSingleton.Connection;
                    if (MyConnection.State == System.Data.ConnectionState.Closed)
                        MyConnection.Open();

                    MySqlCommand myCommand = new MySqlCommand("delete from CoursesHasResources where Courses_CourseID ="
                                                                + course.IdCourse + " AND Resources_ResourceID = " +
                                                               item.IDresource, MyConnection);
                    myCommand.ExecuteNonQuery();
                    myCommand.Connection.Close();
                }
                catch (MySqlException exp)
                {
                    System.Console.WriteLine(exp);
                    MessageBox.Show("fail to delete ! :" + exp.ToString());
                }
                ListofdeletedResources.Add(item);
                item.delete();
            }
            else if (item.STATEPush == StatePush.UPDATE)
            {
                item.updateCascade();
            }

            course.MyStatePush = StatePush.UPDATE;
        }
        foreach (Resource rs in ListofdeletedResources) { course.DeleteResourceOfList(rs); }
    }
    /// <summary>
    /// Permet la supression du cours dans la base de donn�es
    /// </summary>
    /// <param name="course"></param>
    public void DeleteCourse(Course course)
    {

        // listOfCourses.Remove(course);


        // Supression des exercices associ�s au cours


        // DataBase connexion
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("delete from CoursesHasResources where Courses_CourseID ="
                                                        + course.IdCourse, MyConnection);
            myReader = myCommand.ExecuteReader();
            myReader.Close();

            /*MySqlCommand myCommand3 = new MySqlCommand("delete from CoursesHasResources AS CHR INNER JOIN Resources AS RC " +
                                                        " ON CHR.Resources_ResourceID = where Courses_CourseID ="
                                            + course.IdCourse, MyConnection);
            myReader = myCommand3.ExecuteReader();
            myReader.Close();*/

            MySqlCommand myCommand2 = new MySqlCommand("delete from Courses where CourseID ="
                                                        + course.IdCourse, MyConnection);
            myReader = myCommand2.ExecuteReader();
            myReader.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

        course.MyStatePush = StatePush.DELETE;
    }

    /// <summary>
    /// Une structure utilis� dans le code interne pour stocker des informations concernant la table Courses afin d'effectuer
    /// des jointures
    /// </summary>
    private struct CourseDB
    {
        public int idCourse;
        public Difficulty difficulty;
        public bool isDynamic;
    }
    private struct ResourceDB
    {
        public int ResourceID;
        public string TextToken;
        public int ResourceType;
    }

    /// <summary>
    /// Une structure utilis� dans le code interne pour stocker des informations concernant la table Exercice afin d'effectuer
    /// des jointures
    /// </summary>
    private struct ExeriseDB
    {
        public int exerciseID;
        public ExerciseType exerciseType;
    }
    /// <summary>
    /// recupere l'id le plus haut dans la table et le nom de colonne sp�cifi�
    /// </summary>
    /// <param name="tableName"></param>
    /// <param name="colonneName"></param>
    /// <returns></returns>


    /// <summary>
    /// Fonction priv� de la classe permettant d'obtenir le maximum d'une colonne dans une table dans la base
    /// de donn�es. Elle sera utilis� pour recuperer l'identifiant du dernier enregistrement dans une table.
    /// </summary>
    /// <param name="tableName">le nom de la table</param>
    /// <param name="colonneName">le nom de la colonne</param>
    /// <returns>retourne -1 en cas d'erreur, sinon elle retourne la max de la colonne sp�cifi� en parametre de la
    /// fonction</returns>
    private int maxIDinDB(string tableName, string colonneName)
    {
        int max = 1;
        try
        {

            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            string query = "SELECT MAX(" + colonneName + ") AS MAXID FROM " + tableName;
            MySqlCommand myCommand = new MySqlCommand(query, MyConnection);
            myReader = myCommand.ExecuteReader();
            if (myReader.Read())
            {
                if (!myReader["MAXID"].ToString().Equals(""))
                    max = (int)myReader["MAXID"];
            }
            myReader.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
        return max;
    }

}


