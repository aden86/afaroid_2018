﻿using Renci.SshNet;
using Renci.SshNet.Common;

namespace IHM_ClientLourd.model
{
    /// <summary>
    /// Classe Permettant de se connecter au serveur FTP distant de l'application
    /// </summary>
    public class User
    {
        /// <summary>
        /// le nom de l'utilisateur
        /// </summary>
        private string FTPusername;
        /// <summary>
        /// le mot de passe pour se connecter
        /// </summary>
        private string FTPpassword;


        public string FTPUsername { get { return FTPusername; } set { FTPusername = value; } }

        public string FTPPassword { get { return FTPpassword; } set { FTPpassword = value; } }



        public User(string FTPusername, string FTPpassword)
        {
            this.FTPusername = FTPusername;
            this.FTPpassword = FTPpassword;
        }

        public bool connect()
        {
            string ftpServerIP = "ftp.cluster023.hosting.ovh.net";
            using (SftpClient client = new SftpClient(ftpServerIP, 22, this.FTPusername, this.FTPpassword))
            {
                try
                {
                    client.Connect();
                    return true;
                }
                catch (SshAuthenticationException ex)
                {
                    return false;
                }
            }
        }
    }
}
