using System.Collections.Generic;

/// <summary>
/// Classe representant un cours (ou un vocabulaire).
/// Repesente Egalement un enregistrement de la table courses
/// </summary>
public class Course
{
    /*
     * Members
     * */
    /// <summary>
    /// l'id du cours dans la base de donn�es
    /// </summary>
    private int idCourse;
    /// <summary>
    /// d�ffini la difficult� du cours
    /// </summary>
    private Difficulty difficulty;
    /// <summary>
    /// contient la liste de ses ressources
    /// </summary>
    private List<Resource> listOfResources = new List<Resource>();
    /// <summary>
    ///  le type de cours (cours ou vocabulaire)
    /// </summary>
    private bool isDynamic = false;
    /// <summary>
    /// defini l'etat du cours
    /// si c'est un nouveau theme un theme ajout�, 
    ///  un theme mis � jour,  ou un theme o� aucune modification n'a �tait faite.
    /// </summary>
    private StatePush statePush = StatePush.NONE;
    /// <summary>
    /// defini le sous-theme parent
    /// </summary>
    private SubTheme SubTheme;

    /*
     * Properties
     * */
    /// <summary>
    /// Permet d'obtenir l'id du cours.
    /// Permet aussi de le modifier
    /// </summary>
    public int IdCourse { get { return idCourse; } set { idCourse = value; } }
    /// <summary>
    /// Permet d'obtenir la difficult� du cours
    /// Permet aussi de la modifier
    /// </summary>
    public Difficulty Mydifficulty { get { return difficulty; } set { difficulty = value; } }
    /// <summary>
    /// Permet d'obtenir le type de cours (cours ou vocabulaire)
    /// Permet aussi de le modifier
    /// </summary>
    public bool IsDynamic { get { return isDynamic; } set { isDynamic = value; } }
    /// <summary>
    /// permet d'obtenir l'etat du cours
    /// si c'est un nouveau theme un theme ajout�, 
    ///  un theme mis � jour,  ou un theme o� aucune modification n'a �tait faite.
    /// permet aussi de le modifier
    /// </summary>
    public StatePush MyStatePush { get { return statePush; } set { statePush = value; } }
    /// <summary>
    /// permet d'obtenir son parent
    /// permet aussi de le modifier
    /// </summary>
    public SubTheme MySubTheme { get { return SubTheme; } set { SubTheme = value; } }
    /// <summary>
    /// permet d'obtenir la liste de ses ressources
    /// permet aussi de le modifier
    /// </summary>
    public List<Resource> ListOfResources { get { return listOfResources; } set { listOfResources = value; } }

    /*
     *Constructors
     **/

    /// <summary>
    /// Constructeur de la classe
    /// </summary>
    public Course()
    {
    }


    /// <summary>
    /// Constructeur de la classe avec argument
    /// </summary>
    /// <param name="id"></param>
    /// <param name="difficulty"></param>
    /// <param name="isDynamic"></param>
    /// <param name="listOfResources"></param>
    public Course(int id, Difficulty difficulty, bool isDynamic, List<Resource> listOfResources)
    {
        this.idCourse = id;
        this.difficulty = difficulty;
        this.isDynamic = isDynamic;
        this.listOfResources = listOfResources;
    }


    /// <summary>
    /// Constructeur de la classe avec argument
    /// </summary>
    /// <param name="id"></param>
    /// <param name="difficulty"></param>
    /// <param name="isDynamic"></param>
    public Course(int id, Difficulty difficulty, bool isDynamic)
    {
        this.idCourse = id;
        this.difficulty = difficulty;
        this.isDynamic = isDynamic;
    }


    /*
     * Public Methods
     * */
    /// <summary>
    /// recuperation de la liste des ressources li�es � ce cours
    /// </summary>
    /// <returns></returns>
    public List<Resource> GetListOfResource()
    {
        return this.listOfResources;
    }
    /// <summary>
    /// suprimme la ressource vis�e
    /// </summary>
    /// <param name="rc"></param>
    public void DeleteResourceOfList(Resource rc)
    {
        this.listOfResources.Remove(rc);
    }
    /// <summary>
    /// ajout une ressource dans l'application
    /// </summary>
    /// <param name="rc"></param>
    public void AddResource(Resource rc)
    {
        rc.STATEPush = StatePush.NEW;
        listOfResources.Add(rc);
    }
    /// <summary>
    /// ajout d'une ressource dans la base de donn�es
    /// </summary>
    /// <param name="rc"></param>
    public void PushResource(Resource rc)
    {
        rc.add();
    }
}
