﻿/// <summary>
/// Represente les differentes langues de l'application
/// </summary>
public enum Language
{
    /// <summary>
    /// Afar
    /// </summary>
    AA = 1,
    /// <summary>
    /// Français
    /// </summary>
    FR = 2,
    /// <summary>
    /// Anglais
    /// </summary>
    EN = 3,
    /// <summary>
    /// Suédois
    /// </summary>
    SV = 4,
    /// <summary>
    /// Allemand
    /// </summary>
    DE = 5,
    /// <summary>
    /// Néerlandais
    /// </summary>
    NL = 6,
    /// <summary>
    /// Arabe
    /// </summary>
    AR = 7
}