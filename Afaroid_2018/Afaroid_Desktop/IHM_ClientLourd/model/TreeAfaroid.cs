using MySql.Data.MySqlClient;

using System.Collections.Generic;
/// <summary>
/// Classe representant la tete du programme. Elle contient tous les themes et permet d'en ajouter ou 
/// d'en supprimer et de les modifier
/// </summary>
public class TreeAfaroid

{
    /*
     * Members
     * */
    /// <summary>
    /// La liste de tous les themes de l'application 
    /// utilisés pour l'arbre 
    /// </summary>
    private List<Theme> listOfThemes = new List<Theme>();

    /*
     * Properties
     * */
    /// <summary>
    /// Obtient ou defint la liste des themes
    /// </summary>
    public List<Theme> ListOfThemes
    {
        get { return listOfThemes; }
        set { listOfThemes = value; }
    }

    /*
     * Constructors
     * */

    /// <summary>
    /// Constructeur de la classe 
    /// </summary>
    public TreeAfaroid()
    {
        this.listOfThemes = GetListOfThemes();
    }

    /*
     * public method
     * */

    /// <summary>
    /// Permet de recuperer tous les themes depuis la base de données.
    /// </summary>
    /// <returns></returns>
    public List<Theme> GetListOfThemes()
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("select * from Themes", MyConnection);
            myReader = myCommand.ExecuteReader();

            List<ThemeDB> ThemeDBList = new List<ThemeDB>();
            while (myReader.Read())
            {
                ThemeDB th;
                th.ThemeID = (int)myReader["ThemeID"];
                th.ResourceID = (int)myReader["Resources_ResourceID"];
                ThemeDBList.Add(th);
            }
            myReader.Close();

            for (int i = 0; i < ThemeDBList.Count; i++)
            {
                MySqlDataReader myImageRead = null;
                MySqlCommand myImage = new MySqlCommand("select * from Resources where Resources.ResourceID = "
                                                        + ThemeDBList[i].ResourceID,
                                                         MyConnection);
                myImageRead = myImage.ExecuteReader();

                string themeName = null;
                Image MyImageTheme = null;


                ResourceDB rc = new ResourceDB();
                while (myImageRead.Read())
                {
                    rc.ResourceID = (int)myImageRead["ResourceID"];
                    rc.TextToken = (string)myImageRead["TextToken"];
                    rc.TextTranslations_TextID = (int)myImageRead["TextTranslations_TextID"];
                }
                myImageRead.Close();


                MyImageTheme = new Image(rc.ResourceID, RC_TYPE.IMAGE, rc.TextToken);
                MySqlDataReader ThemeNameReader = null;
                MySqlCommand ThemeNameCommand = new MySqlCommand("select * from TextTranslations"
                    + " where TextID = " + rc.TextTranslations_TextID
                    , MyConnection);
                ThemeNameReader = ThemeNameCommand.ExecuteReader();


                while (ThemeNameReader.Read())
                {
                    // On prends que le nom subtheme en fran�ais
                    if ((int)ThemeNameReader["Languages_LanguageID"] == (int)Language.FR)
                    {
                        themeName = (string)ThemeNameReader["Translation"];
                    }
                }
                ThemeNameReader.Close();
                Theme theme = new Theme(ThemeDBList[i].ThemeID, themeName, MyImageTheme);
                theme.ListOfSubThemes = theme.GetListOfSubThemes();
                listOfThemes.Add(theme);

            }


        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }



        return listOfThemes;
    }
    /// <summary>
    /// Permet d'ajouter un theme dans l'application
    /// </summary>
    /// <param name="theme"></param>
    public void AddTheme(Theme theme)
    {
        theme.MyStatePush = StatePush.NEW;
        theme.TreeAfaroid = this;
        this.listOfThemes.Add(theme);
    }
    /// <summary>
    /// Permet d'ajouter uu theme s'il est nouveau dans la base de données
    /// </summary>
    /// <param name="theme"></param>
    public void PushTheme(Theme theme)
    {
        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlCommand myCommand = new MySqlCommand("INSERT INTO `themes`(`ThemeID`, `Resources_ResourceID`) VALUES (NULL, '" + theme.Image.IDresource + "')"
                                                            , MyConnection);

            myCommand.ExecuteNonQuery();
            myCommand.Connection.Close();

        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }
    }
    /// <summary>
    /// Permet de  mettre à jour un theme existant dans la base de données
    /// </summary>
    /// <param name="theme"></param>
    public void UpdateTheme(Theme theme)
    {

        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("update Themes set  Resources_ResourceID = " + theme.Image.IDresource
                 + " where ThemeID =  " + theme.IdTheme,
                MyConnection);

            myReader = myCommand.ExecuteReader();
            myReader.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

    }
    /// <summary>
    /// Permet la Suppression d'un theme de la base de données. 
    ///  La suppression fait un appelle en cascade sur les sous themes de celui ci
    /// ainsi que sur les cours de ceux ci.
    /// </summary>
    /// <param name="theme"></param>
    public void DeleteTheme(Theme theme)

    {
        foreach (SubTheme item in theme.ListOfSubThemes)
        {
            if (item.MyStatePush == StatePush.NONE)
            {
                theme.DeleteSubTheme(item);
            }
        }

        try
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            if (MyConnection.State == System.Data.ConnectionState.Closed)
                MyConnection.Open();
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("delete from Themes where ThemeID =" + theme.IdTheme,
                                                        MyConnection);
            myReader = myCommand.ExecuteReader();
            myReader.Close();
        }
        catch (MySqlException exp)
        {
            System.Console.WriteLine(exp);
        }

    }

    /// <summary>
    /// Une structure utilisé dans le code interne pour stocker des informations concernant la table Theme afin d'effectuer
    /// des jointures
    /// </summary>
    struct ThemeDB
    {
        public int ThemeID;
        public int ResourceID;
    }

    /// <summary>
    /// Une structure utilisé dans le code interne pour stocker des informations concernant la table Resource afin d'effectuer
    /// des jointures
    /// </summary>
    struct ResourceDB
    {
        public int ResourceID;
        public string TextToken;
        public int TextTranslations_TextID;
    }


}







