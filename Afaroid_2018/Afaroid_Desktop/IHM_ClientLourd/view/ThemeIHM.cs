﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace IHM_ClientLourd
{
    /// <summary>
    /// Classe representant une IHM pour la visualisation et la modification d'un Theme ou sous-theme
    /// L'IHM est la meme pour un theme que pour un sous-theme vu qu'ils representent la même chose
    /// </summary>
    public partial class ThemeIHM : Form
    {
        /// <summary>
        /// Le Theme à visualiser ou à modifier
        /// </summary>
        private Theme theme;
        /// <summary>
        /// Le sous-theme à visualiser ou à modifier
        /// </summary>
        private SubTheme subtheme;
        /// <summary>
        /// un flag qui indique si l'IHM va afficher et modifier un theme ou un sous-theme
        /// </summary>
        private bool isSubtheme = false;
        /// <summary>
        /// l'objet de l'IHM principale pour pouvoir acceder et synchroniser l'arbre de l'interface après
        /// chaque modification
        /// </summary>
        private Afaroid tree;
        /// <summary>
        /// L'image (ressource) du Theme ou sous-theme.
        /// A visualiser si le chemin vers l'image n'est pas changé
        /// </summary>
        private Image img = new Image();

        /// <summary>
        /// Constructeur de la classe. Il permet d'initialiser toutes les composantes graphique de l'IHM pour un theme
        /// </summary>
        /// <param name="theme">Le theme à visualiser ou à modifier</param>
        /// <param name="tree">l'objet de l'IHM principale pour pouvoir acceder et synchroniser l'arbre de l'interface après
        /// chaque modification</param>
        public ThemeIHM(Theme theme, Afaroid tree)
        {
            this.theme = theme;
            this.isSubtheme = false;
            this.tree = tree;
            InitializeComponent();
            this.Text = "Thème";
        }

        /// <summary>
        /// Constructeur de la classe. Il permet d'initialiser toutes les composante graphique de l'IHM pour un sous-theme
        /// </summary>
        /// <param name="subtheme">le sous-theme à visualiser ou à modifier</param>
        /// <param name="tree">l'objet de l'IHM principale pour pouvoir acceder et synchroniser l'arbre de l'interface après
        /// chaque modification</param>
        public ThemeIHM(SubTheme subtheme, Afaroid tree)
        {
            this.subtheme = subtheme;
            this.isSubtheme = true;
            this.tree = tree;
            InitializeComponent();
            this.Text = "Sous-thème";
        }

        /// <summary>
        /// Methode pour le chargement des composantes graphique du formulaire
        /// Elle sera appelé dans chaque lancement du formulaire
        /// </summary>
        private void ThemeIHM_Load(object sender, EventArgs e)
        {

            if (!isSubtheme)
            {
                showLabelName();
                loadComboBox(theme.Image.ListOfTextTranslations);
                loadImage(theme.Image.TextToken);
            }
            else
            {
                showLabelName();
                loadComboBox(subtheme.Image.ListOfTextTranslations);
                loadImage(subtheme.Image.TextToken);
            }

        }

        /// <summary>
        /// Permet de changer une image. Elle lance le folder browser pour pouvoir selectionner une autre image pour 
        /// le theme ou sous-theme 
        /// </summary>
        private void changeImage()
        {
            // s'il s'agit d'un theme
            if (!isSubtheme)

                img = new Image(theme.Image.IDresource, RC_TYPE.IMAGE, theme.Image.TextToken);
            // si il s'agit d'un sous theme
            else
                img = new Image(subtheme.Image.IDresource, RC_TYPE.IMAGE, subtheme.Image.TextToken);

            string s = img.TextToken;
            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(img, true);
            if (!img.TextToken.Equals(s))
            {
                loadImage(img.TextToken);
            }
            else
            {
                if (!isSubtheme)
                {
                    loadImage(theme.Image.TextToken);
                }
                else
                {
                    loadImage(subtheme.Image.TextToken);
                }
            }
        }

        /// <summary>
        /// Permet de montrer le nom du theme ou sous-theme dans le label correspond au titre
        /// </summary>
        private void showLabelName()
        {
            if (!isSubtheme)
                label1.Text = "Thème : " + theme.Name;
            else
                label1.Text = "Sous-thème : " + subtheme.Name;
        }

        /// <summary>
        /// Ferme le formulaire
        /// </summary>
        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Permet de charger une image dans le pictureBox de l'interface
        /// </summary>
        /// <param name="imagePath">le chemin vers l'image</param>
        private void loadImage(string imagePath)
        {
            try
            {
                Bitmap image1 = new Bitmap(imagePath, true);
                // Set the PictureBox to display the image.
                pictureBox1.Image = image1;
            }
            catch (Exception)
            {
                MessageBox.Show("L'image n'est pas présente sur votre PC ou son chemin a été changé !", "Afaroid",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                pictureBox1.BackColor = Color.Gray;
            }

        }

        /// <summary>
        /// Permet de modifier le nom d'un theme ou sous-theme dans un language
        /// </summary>
        private void buttonModifierNom_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Modified == true && nameTextBox.TextLength != 0)
            {
                /*if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {*/
                    //Action si l'utilisateur est sûr
                    ComboboxItem item = (ComboboxItem)this.comboBox1.SelectedItem;
                    if (item.Language == Language.FR)
                    {
                        if (!isSubtheme)
                            label1.Text = "Theme : " + nameTextBox.Text;
                        else
                            label1.Text = "SubTheme : " + nameTextBox.Text;
                    }
                    item.Text = item.Text.Substring(0, 5);
                    item.Text += nameTextBox.Text;
                    item.Translation = nameTextBox.Text;
                    this.comboBox1.Items[this.comboBox1.SelectedIndex] = item;
                //}
            }

        }

        /// <summary>
        /// Permet de remplir le textBox par le nom du theme ou sous-theme selectionné dans la liste deroulante
        /// qui contient les differents noms dans les differents languages
        /// </summary>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboboxItem item = (ComboboxItem)this.comboBox1.SelectedItem;
            string s = item.Text;
            string res = null;
            for (int i = 0; i < s.Length; ++i)
            {
                if (s[i] == ':')
                {
                    res = s.Substring(i + 1, s.Length - i - 1);
                    break;
                }
            }
            nameTextBox.Text = res;
        }

        /// <summary>
        /// Permet de charger la liste deroulante qui contient les differents noms dans les differents languages
        /// </summary>
        /// <param name="ListOfTextTranslations">La liste des noms du theme(ou sous-theme) dans les differentes langues</param>
        private void loadComboBox(List<TextTranslations> ListOfTextTranslations)
        {
            this.comboBox1.DisplayMember = "text";
            this.comboBox1.ValueMember = "language";

            foreach (TextTranslations item in ListOfTextTranslations)
            {
                if (item.Translation != "")
                {
                    switch ((int)item.Language)
                    {
                        case 1:
                            string text1 = "AA : " + item.Translation;
                            ComboboxItem comboItem1 = new ComboboxItem(text1, (Language)1, item.Translation);
                            this.comboBox1.Items.Add(comboItem1);
                            break;
                        case 2:
                            string text2 = "FR : " + item.Translation;
                            ComboboxItem comboItem2 = new ComboboxItem(text2, (Language)2, item.Translation);
                            this.comboBox1.Items.Add(comboItem2);
                            break;
                        case 3:
                            string text3 = "EN : " + item.Translation;
                            ComboboxItem comboItem3 = new ComboboxItem(text3, (Language)3, item.Translation);
                            this.comboBox1.Items.Add(comboItem3);
                            this.comboBox1.SelectedItem = comboItem3;
                            break;
                        case 4:
                            string text4 = "SV : " + item.Translation;
                            ComboboxItem comboItem4 = new ComboboxItem(text4, (Language)4, item.Translation);
                            this.comboBox1.Items.Add(comboItem4);
                            break;
                        case 5:
                            string text5 = "DE : " + item.Translation;
                            ComboboxItem comboItem5 = new ComboboxItem(text5, (Language)5, item.Translation);
                            this.comboBox1.Items.Add(comboItem5);
                            break;
                        case 6:
                            string text6 = "NL : " + item.Translation;
                            ComboboxItem comboItem6 = new ComboboxItem(text6, (Language)6, item.Translation);
                            this.comboBox1.Items.Add(comboItem6);
                            break;
                        case 7:
                            string text7 = "AR : " + item.Translation;
                            ComboboxItem comboItem7 = new ComboboxItem(text7, (Language)7, item.Translation);
                            this.comboBox1.Items.Add(comboItem7);
                            break;
                    }
                }

            }
            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// Permet de sauvegarder dans la base de données les modifications effectués d'un theme(ou sous-theme), et 
        /// synchroniser l'arbre de l'interface principale après ce changement
        /// </summary>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                //Action si l'utilisateur est sûr
                if (!isSubtheme)
                {
                    saveTheme();
                    MessageBox.Show("Le theme a été sauvegardé avec succès", "Afaroid",
                                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }

                else
                {
                    saveSubTheme();
                    MessageBox.Show("Le sous-theme a été sauvegardé avec succès", "Afaroid",
                                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }


                this.tree.refreshTree();
                this.Close();
            }
        }

        /// <summary>
        /// Permet le sauvegarde dans la base de données du theme
        /// </summary>
        private void saveTheme()
        {
            theme.Name = label1.Text.Substring(8, label1.Text.Length - 8);
            if (img.TextToken != null)
            {
                string s = img.TextToken.Replace("\\", "\\\\");
                theme.Image.TextToken = s;
            }

            foreach (ComboboxItem item in this.comboBox1.Items)
            {
                foreach (TextTranslations trans in theme.Image.ListOfTextTranslations)
                {
                    if ((int)item.Language == (int)trans.Language)
                    {
                        trans.Translation = item.Translation;
                    }
                }
            }

            // updates in database
            theme.Image.updateTextTranslations();
            theme.Image.update();
            theme.Image.TextToken = theme.Image.TextToken.Replace("\\\\", "\\");
            theme.Image.STATEPush = StatePush.UPDATE;
            theme.update();
        }

        /// <summary>
        /// Permet le sauvegarde dans la base de données du sous-theme
        /// </summary>
        private void saveSubTheme()
        {
            subtheme.Name = label1.Text.Substring(11, label1.Text.Length - 11);
            if (img.TextToken != null)
            {
                /* string res = null;
                 for (int i = img.TextToken.Length - 1; i > 0; --i)
                 {
                     if (img.TextToken[i] == '\\')
                     {
                         res = img.TextToken.Substring(i + 1, img.TextToken.Length - i - 1);
                         break;
                     }

                 }*/
                string s = img.TextToken.Replace("\\", "\\\\");
                subtheme.Image.TextToken = s;
            }

            foreach (ComboboxItem item in this.comboBox1.Items)
            {
                foreach (TextTranslations trans in subtheme.Image.ListOfTextTranslations)
                {
                    if ((int)item.Language == (int)trans.Language)
                    {
                        trans.Translation = item.Translation;
                    }
                }
            }

            // updates in database
            subtheme.Image.updateTextTranslations();
            subtheme.Image.update();
            subtheme.Image.TextToken = subtheme.Image.TextToken.Replace("\\\\", "\\");
            subtheme.Image.STATEPush = StatePush.UPDATE;
            subtheme.monTheme.UpdateSubTheme(subtheme);

        }

        /// <summary>
        /// Permet d'ajouter un nouveau nom dans une nouvelle langue
        /// </summary>
        private void buttonAjoutLangue_Click(object sender, EventArgs e)
        {
            this.comboBox2.Items.Clear();
            this.textBox1.Text = null;
            this.textBox1.Enabled = false;
            loadCombobox2();
            this.panel2.Visible = false;
            panel1.Visible = true;
            this.panel1.BringToFront();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.panel1.Visible = false;
            this.panel2.Visible = true;
            this.panel2.BringToFront();
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.textBox1.Enabled = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {

            if (textBox1.Modified == true && textBox1.TextLength != 0)
            {
                /*if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {*/
                    //Action si l'utilisateur est sûr
                    ComboboxItem combo = (ComboboxItem)this.comboBox2.SelectedItem;
                    combo.Text = combo.Language + " : " + this.textBox1.Text;
                    combo.Translation = this.textBox1.Text;
                    this.comboBox1.Items.Add(combo);
                    this.panel1.Visible = false;
                    this.panel2.Visible = true;
                    this.panel2.BringToFront();
                //}

            }
        }

        /// <summary>
        /// Permet de charger la liste deroulante qui contient les languages non utilisés
        /// </summary>
        private void loadCombobox2()
        {
            this.comboBox2.DisplayMember = "text";
            this.comboBox2.ValueMember = "language";

            foreach (Language lang in Enum.GetValues(typeof(Language)))
            {
                bool exist = false;
                foreach (ComboboxItem item in this.comboBox1.Items)
                {
                    if ((int)lang == (int)item.Language)
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    string text = lang.ToString();
                    ComboboxItem combobox = new ComboboxItem(text, lang, null);
                    this.comboBox2.Items.Add(combobox);
                }
            }
            this.comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// Permet de modifier l'image associé au theme(ou sous-theme)
        /// </summary>
        private void buttonModifierImage_Click(object sender, EventArgs e)
        {
            changeImage();
        }

        /// <summary>
        /// Classe interne representant un Item de la liste deroulante des noms. On stocke toute les informations
        /// dont on aura besoin pour un nom (exemple : le texte qui va apparaitre dans la liste, le language 
        /// et la traduction)
        /// </summary>
        internal class ComboboxItem
        {
            private string text;
            private Language language;
            private string translation;
            public string Text { get { return text; } set { text = value; } }
            public Language Language { get { return language; } set { language = value; } }

            public string Translation { get { return translation; } set { translation = value; } }

            public ComboboxItem(string text, Language language, string translation)
            {
                this.text = text;
                this.language = language;
                this.translation = translation;
            }
        }

    }


}
