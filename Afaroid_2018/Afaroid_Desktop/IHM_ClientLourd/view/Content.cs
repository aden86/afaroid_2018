﻿using IHM_ClientLourd;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CustomForm
{
    /// <summary>
    /// Classe representant une vue pour visualiser une ressources ajoutés lors de l'ajout, la modification d'un cours.
    /// </summary>
    public partial class Content : UserControl
    {
        private Button button1;
        private TextBox textBox1;
        private Button Edit;
        private Text text;
        private Audio audio;
        private Image image;
        public TextBox TextBox1 { get { return TextBox1; } set { textBox1 = value; } }
        public int index = 0;
        private Text textadd = null;
        private LanguageIHM putTrad;
        private Audio audioadd = null;
        private Image imgadd = null;
        private Course newCourse;
        private Resource rs;

        public delegate void RemoveSiteEventHandler(Object sender, ContentArgs e);
        public event RemoveSiteEventHandler onRemoveSite;

        public Content(Course newCourse)
        {
            this.newCourse = newCourse;
            InitializeComponent();
        }

        public Content(Course newCourse, Resource rs)
        {
            this.newCourse = newCourse;
            this.rs = rs;
            InitializeComponent();
        }

        private void button_remove_Click(object sender, EventArgs e)
        {
            if (text != null)
            {
                if (text.STATEPush == StatePush.NEW)
                {
                    text.STATEPush = StatePush.NONE;
                    newCourse.ListOfResources.Remove(text);
                }

            }

            if (image != null)
            {
                if (image.STATEPush == StatePush.NEW)
                {
                    image.STATEPush = StatePush.NONE;
                    newCourse.ListOfResources.Remove(image);
                }
            }
            if (audio != null)
            {
                if (audio.STATEPush == StatePush.NEW)
                {
                    audio.STATEPush = StatePush.NONE;
                    newCourse.ListOfResources.Remove(audio);
                }
            }

            if (rs == null)
            {
            }
            else
            {
                this.rs.STATEPush = StatePush.DELETE;
            }

            onRemoveSite(this, new ContentArgs(index));
            this.Dispose(true);
        }
        public void AppendMyText(string text)
        {
            textBox1.AppendText(text);
        }
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Edit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(128, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Supprimer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(191, 20);
            this.textBox1.TabIndex = 1;
            // 
            // Edit
            // 
            this.Edit.Location = new System.Drawing.Point(38, 51);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(84, 23);
            this.Edit.TabIndex = 2;
            this.Edit.Text = "Traductions";
            this.Edit.UseVisualStyleBackColor = true;
            this.Edit.Click += new System.EventHandler(this.Edit_Click);
            // 
            // Content
            // 
            this.Controls.Add(this.Edit);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Name = "Content";
            this.Size = new System.Drawing.Size(218, 89);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            button_remove_Click(sender, e);
        }
        public void AddTxt(Text text) { this.text = text; }
        public void AddImg(Image image) { this.image = image; }
        public void AddAudio(Audio audio) { this.audio = audio; }
        private void Edit_Click(object sender, EventArgs e)
        {
            Regex r = new Regex("(.txt)");
            Match m = r.Match(textBox1.Text);
            if (m.Success || text != null)
            {

                putTrad = new LanguageIHM(text);
            }
            else
            {
                r = new Regex("(.png)|(.PNG)|(.jpg)");
                m = r.Match(textBox1.Text);
                if (m.Success || image != null)
                {

                    putTrad = new LanguageIHM(image);
                }
                else
                {
                    r = new Regex("(.mp3)");
                    m = r.Match(textBox1.Text);
                    if (m.Success || audio != null)
                    {

                        putTrad = new LanguageIHM(audio);
                    }
                }
            }
            putTrad.Show();
        }
    }

    public class ContentArgs : EventArgs
    {
        public int index;

        public ContentArgs(int value)
        {
            index = value;
        }
    }
}