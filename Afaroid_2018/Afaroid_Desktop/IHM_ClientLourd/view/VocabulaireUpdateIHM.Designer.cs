﻿namespace IHM_ClientLourd
{
    partial class VocabulaireUpdateIHM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.modifierNomButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LireAudioButton = new System.Windows.Forms.Button();
            this.StateLabel = new System.Windows.Forms.Label();
            this.ModifierImageButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ValiderButton = new System.Windows.Forms.Button();
            this.AnnulerButton = new System.Windows.Forms.Button();
            this.SuivantButton = new System.Windows.Forms.Button();
            this.PrecedentButton = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.AjtImageButton = new System.Windows.Forms.Button();
            this.AjtAudioButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // modifierNomButton
            // 
            this.modifierNomButton.Location = new System.Drawing.Point(355, 255);
            this.modifierNomButton.Name = "modifierNomButton";
            this.modifierNomButton.Size = new System.Drawing.Size(103, 23);
            this.modifierNomButton.TabIndex = 45;
            this.modifierNomButton.Text = "Modifier les noms";
            this.modifierNomButton.UseVisualStyleBackColor = true;
            this.modifierNomButton.Click += new System.EventHandler(this.modifierNomButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Green;
            this.label2.Location = new System.Drawing.Point(417, 342);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Audio ajouté !";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 342);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "Audio associé à l\'image :";
            // 
            // LireAudioButton
            // 
            this.LireAudioButton.Location = new System.Drawing.Point(328, 337);
            this.LireAudioButton.Name = "LireAudioButton";
            this.LireAudioButton.Size = new System.Drawing.Size(83, 23);
            this.LireAudioButton.TabIndex = 42;
            this.LireAudioButton.Text = "Lire l\'audio";
            this.LireAudioButton.UseVisualStyleBackColor = true;
            this.LireAudioButton.Click += new System.EventHandler(this.LireAudioButton_Click);
            // 
            // StateLabel
            // 
            this.StateLabel.AutoSize = true;
            this.StateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StateLabel.ForeColor = System.Drawing.Color.Black;
            this.StateLabel.Location = new System.Drawing.Point(522, 63);
            this.StateLabel.Name = "StateLabel";
            this.StateLabel.Size = new System.Drawing.Size(45, 16);
            this.StateLabel.TabIndex = 40;
            this.StateLabel.Text = "label2";
            // 
            // ModifierImageButton
            // 
            this.ModifierImageButton.Location = new System.Drawing.Point(258, 255);
            this.ModifierImageButton.Name = "ModifierImageButton";
            this.ModifierImageButton.Size = new System.Drawing.Size(91, 23);
            this.ModifierImageButton.TabIndex = 39;
            this.ModifierImageButton.Text = "Modifier";
            this.ModifierImageButton.UseVisualStyleBackColor = true;
            this.ModifierImageButton.Click += new System.EventHandler(this.ModifierImageButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gray;
            this.pictureBox1.Location = new System.Drawing.Point(151, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(296, 161);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.WaitOnLoad = true;
            // 
            // ValiderButton
            // 

            this.ValiderButton.Location = new System.Drawing.Point(482, 469);
            this.ValiderButton.Name = "ValiderButton";
            this.ValiderButton.Size = new System.Drawing.Size(75, 23);
            this.ValiderButton.TabIndex = 36;
            this.ValiderButton.Text = "Valider";
            this.ValiderButton.UseVisualStyleBackColor = true;
            this.ValiderButton.Click += new System.EventHandler(this.ValiderButton_Click);
            // 
            // AnnulerButton
            // 
            this.AnnulerButton.Location = new System.Drawing.Point(401, 469);
            this.AnnulerButton.Name = "AnnulerButton";
            this.AnnulerButton.Size = new System.Drawing.Size(75, 23);
            this.AnnulerButton.TabIndex = 35;
            this.AnnulerButton.Text = "Annuler";
            this.AnnulerButton.UseVisualStyleBackColor = true;
            this.AnnulerButton.Click += new System.EventHandler(this.AnnulerButton_Click);
            // 
            // SuivantButton
            // 
            this.SuivantButton.Location = new System.Drawing.Point(304, 424);
            this.SuivantButton.Name = "SuivantButton";
            this.SuivantButton.Size = new System.Drawing.Size(75, 23);
            this.SuivantButton.TabIndex = 34;
            this.SuivantButton.Text = "Suivant";
            this.SuivantButton.UseVisualStyleBackColor = true;
            this.SuivantButton.Click += new System.EventHandler(this.SuivantButton_Click);
            // 
            // PrecedentButton
            // 
            this.PrecedentButton.Enabled = false;
            this.PrecedentButton.Location = new System.Drawing.Point(223, 424);
            this.PrecedentButton.Name = "PrecedentButton";
            this.PrecedentButton.Size = new System.Drawing.Size(75, 23);
            this.PrecedentButton.TabIndex = 33;
            this.PrecedentButton.Text = "Precedent";
            this.PrecedentButton.UseVisualStyleBackColor = true;
            this.PrecedentButton.Click += new System.EventHandler(this.PrecedentButton_Click);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(208, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(182, 16);
            this.titleLabel.TabIndex = 31;
            this.titleLabel.Text = "Modification d\'un vocabulaire";
            // 
            // AjtImageButton
            // 
            this.AjtImageButton.Enabled = false;
            this.AjtImageButton.Location = new System.Drawing.Point(141, 255);
            this.AjtImageButton.Name = "AjtImageButton";
            this.AjtImageButton.Size = new System.Drawing.Size(100, 23);
            this.AjtImageButton.TabIndex = 46;
            this.AjtImageButton.Text = "Ajouter une image";
            this.AjtImageButton.UseVisualStyleBackColor = true;
            this.AjtImageButton.Click += new System.EventHandler(this.AjtImageButton_Click);
            // 
            // AjtAudioButton
            // 
            this.AjtAudioButton.Enabled = false;
            this.AjtAudioButton.Location = new System.Drawing.Point(227, 337);
            this.AjtAudioButton.Name = "AjtAudioButton";
            this.AjtAudioButton.Size = new System.Drawing.Size(93, 23);
            this.AjtAudioButton.TabIndex = 47;
            this.AjtAudioButton.Text = "Ajouter un audio";
            this.AjtAudioButton.UseVisualStyleBackColor = true;
            this.AjtAudioButton.Click += new System.EventHandler(this.AjtAudioButton_Click);
            // 
            // VocabulaireUpdateIHM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 503);
            this.Controls.Add(this.AjtAudioButton);
            this.Controls.Add(this.AjtImageButton);
            this.Controls.Add(this.modifierNomButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LireAudioButton);
            this.Controls.Add(this.StateLabel);
            this.Controls.Add(this.ModifierImageButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ValiderButton);
            this.Controls.Add(this.AnnulerButton);
            this.Controls.Add(this.SuivantButton);
            this.Controls.Add(this.PrecedentButton);
            this.Controls.Add(this.titleLabel);
            this.MaximizeBox = false;
            this.Name = "VocabulaireUpdateIHM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modification d\'un vocabulaire";
            this.Load += new System.EventHandler(this.VocabulaireUpdateIHM_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button modifierNomButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button LireAudioButton;
        private System.Windows.Forms.Label StateLabel;
        private System.Windows.Forms.Button ModifierImageButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ValiderButton;
        private System.Windows.Forms.Button AnnulerButton;
        private System.Windows.Forms.Button SuivantButton;
        private System.Windows.Forms.Button PrecedentButton;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button AjtImageButton;
        private System.Windows.Forms.Button AjtAudioButton;
    }
}