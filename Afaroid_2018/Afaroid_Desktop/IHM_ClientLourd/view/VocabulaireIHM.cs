﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;


namespace IHM_ClientLourd
{
    /// <summary>
    /// Classe represantant une IHM pour l'ajout d'un vocabulaire. Elle sera afficher quand l'utilisateur
    /// ajoute un cours.
    /// Le vocabulaire est representé dans la base de données comme un cours, et est une suite de couple Image et audio. 
    /// Pour cela on utilise comme structure de données lors de l'ajout une Map ayant comme clé la valeur courante et
    /// comme valeur le couple Image et audio. Elle sera utile pour permettre à l'utilisateur de basculer entre les couples
    /// image audio et pouvoir les visualiser ou les modifier.
    /// </summary>
    public partial class VocabulaireIHM : Form
    {

        /// <summary>
        /// La valeur dont l'utilisateur bascule dans l'IHM
        /// </summary>
        private int currentValue;
        /// <summary>
        /// La valeur maximum du couple audio image que l'utilisateur puisse ajouter
        /// </summary>
        private const int maxValue = 20;
        /// <summary>
        /// La derniere valeur dont l'utilisateur a ajouté un nouveau couple d'image audio
        /// </summary>
        private int lastValue;
        /// <summary>
        /// L'image d'un couple du vocabulaire 
        /// </summary>
        private Image img;
        /// <summary>
        /// L'audio d'un couple du vocabulaire
        /// </summary>
        private Audio audio;
        /// <summary>
        /// La Map dont on stocke les couples image et audio 
        /// </summary>
        private Dictionary<int, List<Resource>> resourceMap = new Dictionary<int, List<Resource>>();
        /// <summary>
        /// Le sous-Theme correspond à ce vocabulaire. Il sera utile lors de l'ajout dans la base de données et la
        /// synchronisation avec l'arbre
        /// </summary>
        private SubTheme subtheme;
        /// <summary>
        /// La difficulté de ce cours, de sorte que le vocabulaire est un cours dynamique
        /// </summary>
        private Difficulty difficulty;
        /// <summary>
        /// le cours qui represente le vocabulaire
        /// </summary>
        private Course course;
        /// <summary>
        /// l'objet de l'IHM principale pour pouvoir acceder et synchroniser l'arbre de l'interface après
        /// la création du vocabulaire
        /// </summary>
        private Afaroid tree;

        /// <summary>
        /// Constructeur de la classe : initialise les composantes graphiques de l'interface
        /// </summary>
        /// <param name="subtheme">Le sous-Theme correspond à ce vocabulaire</param>
        /// <param name="difficulty">La difficulté de ce cours</param>
        /// <param name="tree">l'objet de l'IHM principale pour pouvoir acceder et synchroniser l'arbre de l'interface après
        /// la création du vocabulaire</param>
        public VocabulaireIHM(SubTheme subtheme, Difficulty difficulty, Afaroid tree)
        {
            this.tree = tree;
            this.subtheme = subtheme;
            this.difficulty = difficulty;
            currentValue = 1;
            lastValue = 1;
            InitializeComponent();
        }

        /// <summary>
        /// Permet d'effectuer un traitement après l'initialisation des composantes graphiques
        /// </summary>
        private void VocabulaireIHM_Load(object sender, EventArgs e)
        {
            refreshLabel(1, maxValue);
        }

        /// <summary>
        /// Permet de rafraichir le label d'etat 
        /// </summary>
        /// <param name="value">la valeur à afficher dans le label</param>
        /// <param name="maxValue">la valeur maximum</param>
        private void refreshLabel(int value, int maxValue)
        {
            string s = value + "/" + maxValue;
            this.StateLabel.Text = s;
        }

        /// <summary>
        /// Permet de charger une image dans le pictureBox de l'interface
        /// </summary>
        /// <param name="imagePath">Le chemin vers l'image</param>
        private void loadImage(string imagePath)
        {
            try
            {
                Bitmap image1 = new Bitmap(imagePath, true);
                // Set the PictureBox to display the image.
                pictureBox1.Image = image1;
            }
            catch (Exception)
            {
                MessageBox.Show("L'image n'est pas présente sur votre PC ou son chemin a été changé !", "Afaroid",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                pictureBox1.BackColor = Color.Gray;
            }
        }

        /// <summary>
        /// permet d'ajouter une image depuis l'explorateur des dossiers et fichiers
        /// </summary>
        private void AjtImageButton_Click(object sender, EventArgs e)
        {
            img = new Image();
            TextTranslations textFR = new TextTranslations(0, 0, "", Language.FR);
            img.ListOfTextTranslations.Add(textFR);

            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(img, true);
            if (img.TextToken != null)
            {
                img.STATEPush = StatePush.NONE;
                loadImage(img.TextToken);
                LanguageIHM langIHM = new LanguageIHM(img);
                langIHM.Show();
                AjtImageButton.Enabled = false;
                ModifierImageButton.Enabled = true;
                modifierNomButton.Enabled = true;
                AjtAudioButton.Enabled = true;
            }
            else
            {
                this.pictureBox1.BackColor = Color.Gray;
            }

        }

        /// <summary>
        /// Permet de changer l'image depuis l'explorateur des dossiers et fichiers
        /// </summary>
        private void ModifierImageButton_Click(object sender, EventArgs e)
        {
            string s = img.TextToken;
            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(img, true);
            if (!img.TextToken.Equals(s))
            {
                loadImage(img.TextToken);
                img.ListOfTextTranslations.Clear();
                TextTranslations textFR = new TextTranslations(0, 0, null, Language.FR);
                img.ListOfTextTranslations.Add(textFR);
                LanguageIHM langIHM = new LanguageIHM(img);
                langIHM.Show();
            }
            else
            {
                loadImage(s);
            }
        }

        /// <summary>
        /// Permet de modifier les noms associés à l'image dans les differents languages
        /// </summary>
        private void modifierNomButton_Click(object sender, EventArgs e)
        {
            LanguageIHM langIHM = new LanguageIHM(img);
            langIHM.Show();
        }

        /// <summary>
        /// permet d'ajouter un audio depuis l'explorateur des dossiers et fichiers
        /// </summary>
        private void AjtAudioButton_Click(object sender, EventArgs e)
        {
            audio = new Audio();

            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(audio);
            if (audio.TextToken != null)
            {
                audio.STATEPush = StatePush.NONE;
                label2.ForeColor = Color.Green;
                label2.Text = "Audio ajoutée avec succès";
                AjtAudioButton.Enabled = false;
                LireAudioButton.Enabled = true;
                SuivantButton.Enabled = true;
            }
        }

        /// <summary>
        /// Permet de lire l'audio ajouté
        /// </summary>
        private void LireAudioButton_Click(object sender, EventArgs e)
        {
            try
            {
                Player player = new Player();

                player.Open(@audio.TextToken);

                player.Play(false);
            }
            catch (Exception)
            {
                MessageBox.Show("L'audio n'est pas présente sur votre PC ou son chemin a été changé !", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        /// <summary>
        /// Permet de passer au couple image audio suivant s'il existe on le visualise, sinon on initialise toute les
        /// composantes graphique pour pouvoir ajouter un nouveau couple.
        /// </summary>
        private void SuivantButton_Click(object sender, EventArgs e)
        {
            if (currentValue == maxValue)
                SuivantButton.Enabled = false;

            if (currentValue == 1)
                PrecedentButton.Enabled = true;

            if (img.STATEPush == StatePush.NONE && audio.STATEPush == StatePush.NONE)
            {
                List<Resource> pairImageAudio = new List<Resource>();
                img.STATEPush = StatePush.NEW;
                pairImageAudio.Add(img);
                audio.STATEPush = StatePush.NEW;
                pairImageAudio.Add(audio);
                resourceMap.Add(currentValue, pairImageAudio);
                currentValue += 1;
                lastValue += 1;
                initAllButton();
            }
            else
            {
                if (currentValue + 1 == lastValue)
                {
                    currentValue += 1;
                    initAllButton();
                }
                else
                {
                    currentValue += 1;
                    refreshLabel(currentValue, maxValue);
                    List<Resource> pairImageAudio = getPairImageAudio(currentValue);
                    img = (Image)pairImageAudio[0];
                    this.pictureBox1.Image = null;
                    loadImage(img.TextToken);
                    ModifierImageButton.Enabled = true;
                    modifierNomButton.Enabled = true;
                    audio = (Audio)pairImageAudio[1];
                    AjtAudioButton.Enabled = false;
                    label2.ForeColor = Color.Green;
                    label2.Text = "Audio ajoutée avec succès";
                    LireAudioButton.Enabled = true;
                    SuivantButton.Enabled = true;
                }

            }

            if (currentValue == 5)
            {
                label1.ForeColor = Color.Green;
                label1.Text = "Le nombre de vocabulaires minimal a été atteint, vous avez désormais la possibilité de valider";
                StateLabel.ForeColor = Color.Green;
                ValiderButton.Enabled = true;
            }
        }

        /// <summary>
        /// Permet d'initialiser toutes les composantes graphiques permettant d'ajouter un nouveau couple d'image audio 
        /// </summary>
        private void initAllButton()
        {
            refreshLabel(currentValue, maxValue);
            this.pictureBox1.Image = null;
            this.pictureBox1.BackColor = Color.Gray;
            AjtImageButton.Enabled = true;
            ModifierImageButton.Enabled = false;
            modifierNomButton.Enabled = false;
            AjtAudioButton.Enabled = true;
            LireAudioButton.Enabled = false;
            label2.ForeColor = Color.Red;
            label2.Text = "Audio non ajoutée !";
            PrecedentButton.Enabled = true;
            SuivantButton.Enabled = false;
            AjtAudioButton.Enabled = false;
        }

        /// <summary>
        /// Permet de recuperer un couple Image audio de la map 
        /// </summary>
        /// <param name="key">la clé vers la valeur</param>
        /// <returns></returns>
        private List<Resource> getPairImageAudio(int key)
        {
            List<Resource> value = new List<Resource>();
            if (resourceMap.TryGetValue(key, out value))
            {
                return value;
            }
            else
            {
                MessageBox.Show("Key = " + key + " is not found.");
                return value;
            }
        }
        /// <summary>
        /// Permet de visualiser le couple image audio precedent avec la possibilité de le modifier
        /// </summary>
        private void PrecedentButton_Click(object sender, EventArgs e)
        {

            currentValue = currentValue - 1;
            if (currentValue == 1)
            {
                PrecedentButton.Enabled = false;
            }
            refreshLabel(currentValue, maxValue);
            List<Resource> pairImageAudio = getPairImageAudio(currentValue);
            img = (Image)pairImageAudio[0];
            this.pictureBox1.Image = null;
            loadImage(img.TextToken);
            ModifierImageButton.Enabled = true;
            modifierNomButton.Enabled = true;
            audio = (Audio)pairImageAudio[1];
            AjtAudioButton.Enabled = false;
            label2.ForeColor = Color.Green;
            label2.Text = "Audio ajoutée avec succès";
            LireAudioButton.Enabled = true;
            SuivantButton.Enabled = true;
        }

        /// <summary>
        /// Ferme le formulaire
        /// </summary>
        private void AnnulerButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Permet de valider et ajouter le vocabulaire. Normalement on peut valider que si on a au moins 4 couples 
        /// d'images et audio.
        /// </summary>
        private void ValiderButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation de supression", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                //Action si l'utilisateur est sûr

                // Insertion dans la table Course
                course = new Course(0, difficulty, true);
                course.MyStatePush = StatePush.NEW;
                insertCourse(course);

                // Parcourir la map pour inserer le pair de resource (Image et audio) en lui affectant le meme TextID
                int maxTextIDinDB = maxIDinDB("TextTranslations", "textid");
                foreach (KeyValuePair<int, List<Resource>> kvp in resourceMap)
                {
                    maxTextIDinDB++;

                    List<Resource> PairImageAudio = kvp.Value;
                    img = (Image)PairImageAudio[0];
                    img.TextToken = img.TextToken.Replace("\\", "\\\\");
                    audio = (Audio)PairImageAudio[1];
                    audio.TextToken = audio.TextToken.Replace("\\", "\\\\");

                    // Insertion dans la table TextTranslations
                    for (int i = 0; i < img.ListOfTextTranslations.Count; ++i)
                    {
                        img.ListOfTextTranslations[i].TextID = maxTextIDinDB;
                        img.ListOfTextTranslations[i].add();
                    }

                    audio.ListOfTextTranslations = img.ListOfTextTranslations;

                    // On recupere l'id du cours qu'on vient d'ajouter
                    int maxIdCourse = maxIDinDB("Courses", "CourseID");

                    // Insertion dans la table Resources et la table de jointure
                    //// Image
                    insertResource(img, maxTextIDinDB);
                    int maxIdresource = maxIDinDB("Resources", "ResourceID");
                    insertIntoJoinTable(maxIdCourse, maxIdresource);
                    //// Audio
                    insertResource(audio, maxTextIDinDB);
                    maxIdresource = maxIDinDB("Resources", "ResourceID");
                    insertIntoJoinTable(maxIdCourse, maxIdresource);

                    img.TextToken = img.TextToken.Replace("\\\\", "\\");
                    audio.TextToken = audio.TextToken.Replace("\\\\", "\\");
                }

                MessageBox.Show("Le vocabulaire a été ajouté avec succès", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                // Mettre en place l'etat des nouveaux ressource et ajouter les ressources dans le cours 
                foreach (KeyValuePair<int, List<Resource>> kvp in resourceMap)
                {
                    foreach (Resource rc in kvp.Value)
                    {
                        rc.STATEPush = StatePush.NEW;
                        course.ListOfResources.Add(rc);
                    }

                }

                //Synchroniser l'arbre après l'ajout
                subtheme.ListOfCourses.Add(course);
                tree.refreshTree();
                this.Close();
            }
        }

        /// <summary>
        /// Permet d'inserer un cours dans la base de données
        /// </summary>
        /// <param name="c">le cours à inserer</param>
        private void insertCourse(Course c)
        {
            // Database connexion
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();
                int i = c.IsDynamic ? 1 : 0;
                MySqlCommand myCommand = new MySqlCommand("INSERT INTO `courses` (`CourseID`, `SubThemes_SubThemeID`, `Difficulties_DifficultyID`, `IsDynamic`) VALUES (NULL,'" + subtheme.IdSubTheme + "','" + (int)c.Mydifficulty + "','" + i + "')"
                                                            , MyConnection);
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
                MyConnection.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString() + "                  " + (int)c.Mydifficulty);
            }
            try
            {
                c.IdCourse = maxIDinDB("Courses", "CourseID");
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString() + "                  " + (int)c.Mydifficulty);
            }
        }

        /// <summary>
        /// Fonction privé de la classe permettant d'obtenir le maximum d'une colonne dans une table dans la base
        /// de données. Elle sera utilisé pour recuperer l'identifiant du dernier enregistrement dans une table.
        /// </summary>
        /// <param name="tableName">le nom de la table</param>
        /// <param name="colonneName">le nom de la colonne</param>
        /// <returns>retourne -1 en cas d'erreur, sinon elle retourne la max de la colonne spécifié en parametre de la
        /// fonction</returns>
        private int maxIDinDB(string tableName, string colonneName)
        {
            int max = 1;
            try
            {

                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();
                MySqlDataReader myReader = null;
                string query = "SELECT MAX(" + colonneName + ") AS MAXID FROM " + tableName;
                MySqlCommand myCommand = new MySqlCommand(query, MyConnection);
                myReader = myCommand.ExecuteReader();
                if (myReader.Read())
                {
                    if (!myReader["MAXID"].ToString().Equals(""))
                        max = (int)myReader["MAXID"];
                }
                myReader.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
            }
            return max;
        }

        /// <summary>
        /// Permet d'inserer une ressource dans la base de données
        /// </summary>
        /// <param name="rc">La ressource à inserer</param>
        /// <param name="textID">le lien entre la ressource et ses traductions dans les differents languages</param>
        private void insertResource(Resource rc, int textID)
        {
            // Database connexion
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();

                MySqlCommand myCommand = new MySqlCommand("INSERT INTO Resources(`ResourceID`, `TextToken`, `ResourceTypes_ResourceTypeID`, `TextTranslations_TextID`) VALUES (NULL,\"" + rc.TextToken + "\",\"" + (int)rc.Type + "\",\"" + textID + "\")"
                    , MyConnection);
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
                MyConnection.Close();
                System.IO.File.AppendAllText("cache.txt", rc.TextToken + "\r\n");
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString());
            }
        }

        /// <summary>
        /// Permet d'inserer dans la table de jointure entre un cours et une ressource pour les associer
        /// </summary>
        /// <param name="courseID">l'identifiant du cours</param>
        /// <param name="resourceID">l'identifiant de la ressource</param>
        private void insertIntoJoinTable(int courseID, int resourceID)
        {
            // Database connexion
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();

                MySqlCommand myCommand = new MySqlCommand("INSERT INTO CoursesHasResources(`Courses_CourseID`, `Resources_ResourceID`, `OrderRes`) VALUES (" + courseID + ",\"" + resourceID + "\", NULL)"
                    , MyConnection);
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
                MyConnection.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString());
            }
        }


    }

    /// <summary>
    /// Classe represente un mediaplayer Permettant de lire un audio de type mp3
    /// </summary>
    class Player
    {
        private string _command;
        private bool isOpen;
        [DllImport("winmm.dll")]
        private static extern long mciSendString(string strCommand, StringBuilder strReturn, int iReturnLength, IntPtr hwndCallback);

        /// <summary>
        /// Constructeur
        /// </summary>
        public Player()
        {
            Close();
        }
        /// <summary>
        /// Permet de Fermer le mediaplayer
        /// </summary>
        public void Close()
        {
            _command = "close MediaFile";
            mciSendString(_command, null, 0, IntPtr.Zero);
            isOpen = false;
        }

        /// <summary>
        /// Permet d'ouvrir le mediaPlayer
        /// </summary>
        /// <param name="sFileName">le chemin vers l'audio</param>
        public void Open(string sFileName)
        {
            try
            {
                _command = "open \"" + sFileName + "\" type mpegvideo alias MediaFile";
                mciSendString(_command, null, 0, IntPtr.Zero);
                isOpen = true;
            }
            catch
            {
                MessageBox.Show("L'audio n'est pas présente sur votre PC ou son chemin a été changé !", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        /// <summary>
        /// Permet de lire l'audio
        /// </summary>
        /// <param name="loop">Flag indiquant si la lecture soit en boucle</param>
        public void Play(bool loop)
        {
            if (isOpen)
            {
                _command = "play MediaFile";
                if (loop)
                    _command += " REPEAT";
                mciSendString(_command, null, 0, IntPtr.Zero);
            }
        }
    }

}
