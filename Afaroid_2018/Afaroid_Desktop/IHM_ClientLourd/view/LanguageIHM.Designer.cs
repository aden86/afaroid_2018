﻿namespace IHM_ClientLourd
{
    partial class LanguageIHM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LanguageIHM));
            this.SuspendLayout();
            // 
            // LanguageIHM
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LanguageIHM";
            this.Text = "Gestion des noms";
            this.ResumeLayout(false);
            this.label1 = new System.Windows.Forms.Label();
            this.SaveLanguage = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.richTextBoxLanguage = new System.Windows.Forms.RichTextBox();
            this.FR = new System.Windows.Forms.Button();
            this.EN = new System.Windows.Forms.Button();
            this.AA = new System.Windows.Forms.Button();
            this.DE = new System.Windows.Forms.Button();
            this.SV = new System.Windows.Forms.Button();
            this.AR = new System.Windows.Forms.Button();
            
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(197, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 2;
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // SaveLanguage
            // 
            this.SaveLanguage.Location = new System.Drawing.Point(133, 300);
            this.SaveLanguage.Name = "SaveLanguage";
            this.SaveLanguage.Size = new System.Drawing.Size(75, 23);
            this.SaveLanguage.TabIndex = 6;
            this.SaveLanguage.Text = "Save";
            this.SaveLanguage.UseVisualStyleBackColor = true;
            this.SaveLanguage.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            /*
            this.button4.Location = new System.Drawing.Point(328, 300);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = "Close";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);*/
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(203, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(131, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // richTextBoxLanguage
            // 
            this.richTextBoxLanguage.Location = new System.Drawing.Point(64, 145);
            this.richTextBoxLanguage.Name = "richTextBoxLanguage";
            this.richTextBoxLanguage.Size = new System.Drawing.Size(408, 127);
            this.richTextBoxLanguage.TabIndex = 11;
            this.richTextBoxLanguage.Text = "";
            // 
            // FR
            // 
            this.FR.Location = new System.Drawing.Point(64, 116);
            this.FR.Name = "FR";
            this.FR.Size = new System.Drawing.Size(63, 23);
            this.FR.TabIndex = 12;
            this.FR.Text = "FR";
            this.FR.UseVisualStyleBackColor = true;
            this.FR.Click += new System.EventHandler(this.FR_Click);
            // 
            // EN
            // 
            this.EN.Location = new System.Drawing.Point(133, 116);
            this.EN.Name = "EN";
            this.EN.Size = new System.Drawing.Size(63, 23);
            this.EN.TabIndex = 13;
            this.EN.Text = "EN";
            this.EN.UseVisualStyleBackColor = true;
            this.EN.Click += new System.EventHandler(this.EN_Click);
            // 
            // AA
            // 
            this.AA.Location = new System.Drawing.Point(202, 116);
            this.AA.Name = "AA";
            this.AA.Size = new System.Drawing.Size(63, 23);
            this.AA.TabIndex = 14;
            this.AA.Text = "AA";
            this.AA.UseVisualStyleBackColor = true;
            this.AA.Click += new System.EventHandler(this.AA_Click);
            // 
            // DE
            // 
            this.DE.Location = new System.Drawing.Point(271, 116);
            this.DE.Name = "DE";
            this.DE.Size = new System.Drawing.Size(63, 23);
            this.DE.TabIndex = 15;
            this.DE.Text = "DE";
            this.DE.UseVisualStyleBackColor = true;
            this.DE.Click += new System.EventHandler(this.DE_Click);
            // 
            // SV
            // 
            this.SV.Location = new System.Drawing.Point(340, 116);
            this.SV.Name = "SV";
            this.SV.Size = new System.Drawing.Size(63, 23);
            this.SV.TabIndex = 16;
            this.SV.Text = "SV";
            this.SV.UseVisualStyleBackColor = true;
            this.SV.Click += new System.EventHandler(this.SV_Click);
            // 
            // AR
            // 
            this.AR.Location = new System.Drawing.Point(409, 116);
            this.AR.Name = "AR";
            this.AR.Size = new System.Drawing.Size(63, 23);
            this.AR.TabIndex = 17;
            this.AR.Text = "AR";
            this.AR.UseVisualStyleBackColor = true;
            this.AR.Click += new System.EventHandler(this.AR_Click);
            // 
            // LanguageIHM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 361);
            this.Controls.Add(this.AA);
            this.Controls.Add(this.AR);
            this.Controls.Add(this.SV);
            this.Controls.Add(this.DE);
            this.Controls.Add(this.EN);
            this.Controls.Add(this.FR);
            this.Controls.Add(this.richTextBoxLanguage);
            this.Controls.Add(this.pictureBox1);
            //this.Controls.Add(this.button4);
            this.Controls.Add(this.SaveLanguage);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "LanguageIHM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LanguageIHM";
            this.Load += new System.EventHandler(this.LanguageIHM_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SaveLanguage;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox richTextBoxLanguage;
        private System.Windows.Forms.Button FR;
        private System.Windows.Forms.Button EN;
        private System.Windows.Forms.Button DE;
        private System.Windows.Forms.Button SV;
        private System.Windows.Forms.Button AR;
        private System.Windows.Forms.Button AA;
    }
}