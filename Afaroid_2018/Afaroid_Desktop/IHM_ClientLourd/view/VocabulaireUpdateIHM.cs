﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace IHM_ClientLourd
{
    /// <summary>
    /// Classe represantant une IHM pour la visualisation et modification d'un vocabulaire. 
    /// Elle Permet de visualiser les ressources du vocabulaire deux à deux (le couple d'image audio) et basculer entre eux.
    /// De meme elle permet d'ajouter de nouveaux ressources pour ce vocabulaire.
    /// Elle a le meme principe que l'interface d'ajout (VocabulaireIHM)
    /// </summary>
    public partial class VocabulaireUpdateIHM : Form
    {
        /// <summary>
        /// La valeur dont l'utilisateur bascule dans l'IHM
        /// </summary>
        private int currentValue;
        /// <summary>
        /// La valeur maximum du couple audio image que l'utilisateur puisse ajouter
        /// </summary>
        private const int maxValue = 20;
        /// <summary>
        /// La derniere valeur dont l'utilisateur a ajouté un nouveau couple d'image audio
        /// </summary>
        private int lastValue;
        /// <summary>
        /// L'image d'un couple du vocabulaire 
        /// </summary>
        private Image img;
        /// <summary>
        /// L'audio d'un couple du vocabulaire
        /// </summary>
        private Audio audio;
        /// <summary>
        /// La Map dont on stocke les couples image et audio
        /// </summary>
        private Dictionary<int, List<Resource>> resourceMap = new Dictionary<int, List<Resource>>();
        /// <summary>
        /// Le vocabulaire à visualiser ou à modifier
        /// </summary>
        private Course vocabulary;
        /// <summary>
        /// une valeur pour memoriser combien de couples image et audio associé au vocabulaire
        /// </summary>
        private int initResourcesValue;
        /// <summary>
        /// l'objet de l'IHM principale pour pouvoir acceder et synchroniser l'arbre de l'interface après
        /// la modification du vocabulaire
        /// </summary>
        private Afaroid afaroid;


        /// <summary>
        /// Constructeur de la classe : initialise les composantes graphiques de l'interface
        /// </summary>
        /// <param name="vocabulary">Le vocabulaire à visualiser ou à modifier</param>
        /// <param name="afaroid">l'objet de l'IHM principale pour pouvoir acceder et synchroniser l'arbre de l'interface après
        /// la modification du vocabulaire</param>
        public VocabulaireUpdateIHM(Course vocabulary, Afaroid afaroid)
        {
            this.vocabulary = vocabulary;
            this.afaroid = afaroid;
            InitializeComponent();
        }

        /// <summary>
        /// Permet d'effectuer un traitement après l'initialisation des composantes graphiques
        /// </summary>
        private void VocabulaireUpdateIHM_Load(object sender, EventArgs e)
        {
            currentValue = 1;
            // rendre toute la liste des ressources en état update
            foreach (Resource rc in vocabulary.ListOfResources)
            {
                rc.STATEPush = StatePush.UPDATE;
            }
            // Convertir la liste des ressources en map
            convertToMap(vocabulary.ListOfResources, 1);
            lastValue = resourceMap.Count + 1;
            initResourcesValue = resourceMap.Count;
            refreshLabel(1, maxValue);
            List<Resource> PairImageAudio = getPairImageAudio(1);

            if (PairImageAudio[0].Type == RC_TYPE.IMAGE)
                img = (Image)PairImageAudio[0];
            else
                img = (Image)PairImageAudio[1];


            if (PairImageAudio[0].Type == RC_TYPE.AUDIO)
                audio = (Audio)PairImageAudio[0];
            else
                audio = (Audio)PairImageAudio[1];


            loadImage(img.TextToken);


        }
        /// <summary>
        /// Permet de rafraichir le label d'etat
        /// </summary>
        /// <param name="value">la valeur à afficher dans le label</param>
        /// <param name="maxValue">la valeur maximum</param>
        private void refreshLabel(int value, int maxValue)
        {
            string s = value + "/" + maxValue;
            this.StateLabel.Text = s;
        }

        /// <summary>
        /// Permet de charger une image dans le pictureBox de l'interface
        /// </summary>
        /// <param name="imagePath">Le chemin vers l'image</param>
        private void loadImage(string imagePath)
        {
            try
            {
                Bitmap image1 = new Bitmap(imagePath, true);
                // Set the PictureBox to display the image.
                pictureBox1.Image = image1;
            }
            catch (Exception)
            {
                MessageBox.Show("L'image n'est pas présente sur votre PC ou son chemin a été changé !", "Afaroid",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                pictureBox1.BackColor = Color.Gray;
            }
        }

        /// <summary>
        /// Methode permettant de convertir la liste des ressources du vocabulaire en une map ayant comme clé un entier
        /// et comme valeur le couple d'image audio ayant le meme textID pour pouvoir les identifier comme un couple
        /// </summary>
        /// <param name="listOfImageAudio">La liste des ressources Image et audio</param>
        /// <param name="key">la clé de debut</param>
        private void convertToMap(List<Resource> listOfImageAudio, int key)
        {
            while (listOfImageAudio.Count != 0)
            {
                Resource firstElement = listOfImageAudio[0];
                List<Resource> PairOfImageAudio = new List<Resource>();
                PairOfImageAudio.Add(firstElement);
                listOfImageAudio.Remove(firstElement);
                foreach (Resource rc in listOfImageAudio)
                {
                    if (firstElement.ListOfTextTranslations[0].TextID == rc.ListOfTextTranslations[0].TextID)
                    {
                        PairOfImageAudio.Add(rc);
                        listOfImageAudio.Remove(rc);
                        break;
                    }
                }
                resourceMap.Add(key, PairOfImageAudio);
                key++;
            }
        }

        /// <summary>
        /// Permet de changer l'image depuis l'explorateur des dossiers et fichiers
        /// </summary>
        private void ModifierImageButton_Click(object sender, EventArgs e)
        {
            string s = img.TextToken;
            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(img, true);
            if (!img.TextToken.Equals(s))
            {
                loadImage(img.TextToken);
                img.STATEPush = StatePush.NEW;
            }
            else
            {
                loadImage(s);
            }
        }

        /// <summary>
        /// Permet de recuperer un couple Image audio de la map 
        /// </summary>
        /// <param name="key">la clé vers la valeur</param>
        /// <returns></returns>
        private List<Resource> getPairImageAudio(int key)
        {
            List<Resource> value = new List<Resource>();
            if (resourceMap.TryGetValue(key, out value))
            {
                return value;
            }
            else
            {
                MessageBox.Show("Key = " + key + " is not found.");
                return value;
            }
        }

        /// <summary>
        /// Permet de lire l'audio associé au couple Image Audio
        /// </summary>
        private void LireAudioButton_Click(object sender, EventArgs e)
        {
            try
            {
                Player player = new Player();
                player.Open(@audio.TextToken);
                player.Play(false);
            }
            catch (Exception exp)
            {
                MessageBox.Show("L'audio n'est pas présent sur votre PC ou son chemin a été changé !", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Permet de modifier les noms associés à l'image dans les differents languages
        /// </summary>
        private void modifierNomButton_Click(object sender, EventArgs e)
        {
            LanguageIHM langIHM = new LanguageIHM(img);
            langIHM.Show();
        }

        /// <summary>
        /// Permet de passer au couple image audio suivant s'il existe on le visualise, sinon on initialise toute les
        /// composantes graphique pour pouvoir ajouter un nouveau couple.
        /// </summary>
        private void SuivantButton_Click(object sender, EventArgs e)
        {
            if (currentValue == maxValue)
                SuivantButton.Enabled = false;
            if (currentValue + 1 <= initResourcesValue)
            {
                if (img.STATEPush == StatePush.NEW || audio.STATEPush == StatePush.NEW)
                {
                    List<Resource> PairOfImageAudio = new List<Resource>();
                    PairOfImageAudio.Add(img);
                    PairOfImageAudio.Add(audio);
                    resourceMap[currentValue] = PairOfImageAudio;
                }

                currentValue += 1;
                refreshLabel(currentValue, maxValue);
                List<Resource> pairImageAudio = getPairImageAudio(currentValue);
                img = (Image)pairImageAudio[0];
                this.pictureBox1.Image = null;
                loadImage(img.TextToken);
                AjtImageButton.Enabled = false;
                AjtAudioButton.Enabled = false;
                ModifierImageButton.Enabled = true;
                modifierNomButton.Enabled = true;
                audio = (Audio)pairImageAudio[1];

            }
            else
            {
                if (img.STATEPush == StatePush.NONE && audio.STATEPush == StatePush.NONE)
                {
                    List<Resource> pairImageAudio = new List<Resource>();
                    img.STATEPush = StatePush.NEW;
                    pairImageAudio.Add(img);
                    audio.STATEPush = StatePush.NEW;
                    pairImageAudio.Add(audio);
                    resourceMap.Add(currentValue, pairImageAudio);
                    currentValue += 1;
                    lastValue += 1;
                    initAllButton();
                }
                else
                {
                    if (currentValue + 1 == lastValue)
                    {
                        currentValue += 1;
                        initAllButton();
                    }
                    else
                    {
                        currentValue += 1;
                        refreshLabel(currentValue, maxValue);
                        List<Resource> pairImageAudio = getPairImageAudio(currentValue);
                        img = (Image)pairImageAudio[0];
                        this.pictureBox1.Image = null;
                        loadImage(img.TextToken);
                        ModifierImageButton.Enabled = true;
                        modifierNomButton.Enabled = true;
                        audio = (Audio)pairImageAudio[1];
                        AjtAudioButton.Enabled = false;
                        LireAudioButton.Enabled = true;
                        SuivantButton.Enabled = true;
                    }

                }
            }
            PrecedentButton.Enabled = true;

        }

        /// <summary>
        /// Permet d'initialiser toutes les composantes graphique permettant d'ajouter un nouveau couple d'image audio 
        /// </summary>
        private void initAllButton()
        {
            refreshLabel(currentValue, maxValue);
            this.pictureBox1.Image = null;
            this.pictureBox1.BackColor = Color.Gray;
            AjtImageButton.Enabled = true;
            ModifierImageButton.Enabled = false;
            modifierNomButton.Enabled = false;
            AjtAudioButton.Enabled = true;
            LireAudioButton.Enabled = false;
            label2.ForeColor = Color.Red;
            label2.Text = "Audio non ajouté !";
            PrecedentButton.Enabled = true;
            SuivantButton.Enabled = false;
            AjtAudioButton.Enabled = false;
        }

        /// <summary>
        /// permet de passer au couple image audio precedent pour pouvoir le visualiser et le modifier  
        /// </summary>
        private void PrecedentButton_Click(object sender, EventArgs e)
        {
            currentValue = currentValue - 1;
            if (currentValue == 1)
            {
                PrecedentButton.Enabled = false;
            }
            refreshLabel(currentValue, maxValue);
            List<Resource> pairImageAudio = getPairImageAudio(currentValue);
            img = (Image)pairImageAudio[0];
            this.pictureBox1.Image = null;
            loadImage(img.TextToken);
            audio = (Audio)pairImageAudio[1];
            AjtImageButton.Enabled = false;
            modifierNomButton.Enabled = true;
            ModifierImageButton.Enabled = true;
            AjtAudioButton.Enabled = false;
            LireAudioButton.Enabled = true;
            SuivantButton.Enabled = true;
            label2.ForeColor = Color.Green;
            label2.Text = "Audio ajouté !";
        }

        /// <summary>
        /// permet d'ajouter un audio depuis l'explorateur des dossiers et fichiers
        /// </summary>
        private void AjtAudioButton_Click(object sender, EventArgs e)
        {
            audio = new Audio();

            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(audio);
            if (audio.TextToken != null)
            {
                audio.STATEPush = StatePush.NONE;
                label2.ForeColor = Color.Green;
                label2.Text = "Audio ajouté avec succés";
                AjtAudioButton.Enabled = false;
                LireAudioButton.Enabled = true;
                SuivantButton.Enabled = true;
            }
        }

        /// <summary>
        /// permet d'ajouter une image depuis l'explorateur des dossiers et fichiers
        /// </summary>
        private void AjtImageButton_Click(object sender, EventArgs e)
        {
            img = new Image();
            TextTranslations textFR = new TextTranslations(0, 0, "", Language.FR);
            img.ListOfTextTranslations.Add(textFR);

            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(img, true);
            if (img.TextToken != null)
            {
                img.STATEPush = StatePush.NONE;
                loadImage(img.TextToken);
                LanguageIHM langIHM = new LanguageIHM(img);
                langIHM.Show();
                AjtImageButton.Enabled = false;
                ModifierImageButton.Enabled = true;
                modifierNomButton.Enabled = true;
                AjtAudioButton.Enabled = true;
            }
            else
            {
                this.pictureBox1.BackColor = Color.Gray;
            }
        }

        /// <summary>
        /// Permet d'annuler la modification, Ferme le formulaire
        /// </summary>
        private void AnnulerButton_Click(object sender, EventArgs e)
        {
            resourceMap.Clear();
            this.Close();
        }

        /// <summary>
        /// Permet de modifer le vocabulaire
        /// </summary>
        private void ValiderButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation de supression", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                //Action si l'utilisateur est sûr

                int maxTextIDinDB = maxIDinDB("TextTranslations", "textid");
                int j = 0;
                foreach (KeyValuePair<int, List<Resource>> kvp in resourceMap)
                {
                    if (kvp.Key <= initResourcesValue)
                    {
                        //Acces à la liste de texttranslation de l'image et modifier la table TextTranslation 
                        kvp.Value[0].updateTextTranslations();
                        // Acces à l'image et modifier la table resources
                        img = (Image)kvp.Value[0];
                        img.TextToken = img.TextToken.Replace("\\", "\\\\");
                        img.update();
                        // Acces à l'audio et modifier la table resources
                        audio = (Audio)kvp.Value[1];
                        audio.TextToken = audio.TextToken.Replace("\\", "\\\\");
                        audio.update();
                        img.TextToken = img.TextToken.Replace("\\\\", "\\");
                        audio.TextToken = audio.TextToken.Replace("\\\\", "\\");

                    }
                    else
                    {
                        maxTextIDinDB = maxTextIDinDB + j + 1;
                        List<Resource> PairImageAudio = kvp.Value;
                        img = (Image)PairImageAudio[0];
                        img.TextToken = img.TextToken.Replace("\\", "\\\\");
                        audio = (Audio)PairImageAudio[1];
                        audio.TextToken = audio.TextToken.Replace("\\", "\\\\");

                        // Insertion dans la table TextTranslations
                        for (int i = 0; i < img.ListOfTextTranslations.Count; ++i)
                        {
                            img.ListOfTextTranslations[i].TextID = maxTextIDinDB;
                            img.ListOfTextTranslations[i].add();
                        }
                        audio.ListOfTextTranslations = img.ListOfTextTranslations;

                        // Insertion dans la table Resources et la table de jointure
                        //// Image
                        insertResource(img, maxTextIDinDB);
                        int maxIdresource = maxIDinDB("Resources", "ResourceID");
                        insertIntoJoinTable(vocabulary.IdCourse, maxIdresource);
                        //// Audio
                        insertResource(audio, maxTextIDinDB);
                        maxIdresource = maxIDinDB("Resources", "ResourceID");
                        insertIntoJoinTable(vocabulary.IdCourse, maxIdresource);

                        img.TextToken = img.TextToken.Replace("\\\\", "\\");
                        audio.TextToken = audio.TextToken.Replace("\\\\", "\\");
                        j++;
                    }
                }

                MessageBox.Show("Le vocabulaire a été modifié avecc succes", "Afaroid",                                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                convertToList(resourceMap);
                vocabulary.MyStatePush = StatePush.UPDATE;
                this.afaroid.refreshTree();
                resourceMap.Clear();
                this.Close();
            }
        }


        /// <summary>
        /// Fonction privé de la classe permettant d'obtenir le maximum d'une colonne dans une table dans la base
        /// de données. Elle sera utilisé pour recuperer l'identifiant du dernier enregistrement dans une table.
        /// </summary>
        /// <param name="tableName">le nom de la table</param>
        /// <param name="colonneName">le nom de la colonne</param>
        /// <returns>retourne -1 en cas d'erreur, sinon elle retourne la max de la colonne spécifié en parametre de la
        /// fonction</returns>
        private int maxIDinDB(string tableName, string colonneName)
        {
            int max = 1;
            try
            {

                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();
                MySqlDataReader myReader = null;
                string query = "SELECT MAX(" + colonneName + ") AS MAXID FROM " + tableName;
                MySqlCommand myCommand = new MySqlCommand(query, MyConnection);
                myReader = myCommand.ExecuteReader();
                if (myReader.Read())
                {
                    if (!myReader["MAXID"].ToString().Equals(""))
                        max = (int)myReader["MAXID"];
                }
                myReader.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
            }
            return max;
        }


        /// <summary>
        /// Permet d'inserer une ressource dans la base de données
        /// </summary>
        /// <param name="rc">La ressource à inserer</param>
        /// <param name="textID">le lien entre la ressource et ses traductions dans les differents languages</param>
        private void insertResource(Resource rc, int textID)
        {
            // Database connexion
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();

                MySqlCommand myCommand = new MySqlCommand("INSERT INTO Resources(`ResourceID`, `TextToken`, `ResourceTypes_ResourceTypeID`, `TextTranslations_TextID`) VALUES (NULL,\"" + rc.TextToken + "\",\"" + (int)rc.Type + "\",\"" + textID + "\")"
                    , MyConnection);
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
                MyConnection.Close();
                System.IO.File.AppendAllText("cache.txt", rc.TextToken + "\r\n");
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString());
            }
        }

        /// <summary>
        /// Permet d'inserer dans la table de jointure entre un cours et une ressource pour les associer
        /// </summary>
        /// <param name="courseID">l'identifiant du cours</param>
        /// <param name="resourceID">l'identifiant de la ressource</param>
        private void insertIntoJoinTable(int courseID, int resourceID)
        {
            // Database connexion
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();

                MySqlCommand myCommand = new MySqlCommand("INSERT INTO CoursesHasResources(`Courses_CourseID`, `Resources_ResourceID`, `OrderRes`) VALUES (" + courseID + ",\"" + resourceID + "\", NULL)"
                    , MyConnection);
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
                MyConnection.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show("fail to insert ! :" + exp.ToString());
            }
        }

        /// <summary>
        /// Methode permettant de convertir la map de couple Image audio en une liste de ressources image audio.
        /// </summary>
        /// <param name="resourceMap">La map à convertir en liste</param>
        private void convertToList(Dictionary<int, List<Resource>> resourceMap)
        {
            foreach (KeyValuePair<int, List<Resource>> kvp in resourceMap)
            {
                foreach (Resource rc in kvp.Value)
                {
                    if (rc.STATEPush == StatePush.UPDATE)
                        rc.STATEPush = StatePush.NONE;
                    if (rc.STATEPush == StatePush.NEW)
                        rc.STATEPush = StatePush.UPDATE;

                    vocabulary.ListOfResources.Add(rc);
                }
            }
        }

    }
}
