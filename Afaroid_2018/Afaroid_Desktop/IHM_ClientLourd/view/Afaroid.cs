﻿using CustomForm;
using IHM_ClientLourd.model;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace IHM_ClientLourd
{
    /// <summary>
    /// conteneur principal de l'ihm de l'application
    /// </summary>
    public partial class Afaroid : Form
    {
        private TreeAfaroid MyTree = new TreeAfaroid();
        private List<Resource> listesOfResOfCourse;
        private List<Exercise> listesOfExercise;
        public List<Resource> ListesOfResOfCourse { get { return listesOfResOfCourse; } set { listesOfResOfCourse = value; } }
        public List<Exercise> ListesOfExercise { get { return listesOfExercise; } set { listesOfExercise = value; } }
        public int SPACER { get; private set; }

        /// <summary>
        /// adresse du server distant
        /// </summary>
        private string adresse;
        /// <summary>
        /// login du server distant
        /// </summary>
        private string login;
        /// <summary>
        /// password du server distant
        /// </summary>
        private string password;
        private Theme NewTheme;
        private SubTheme LeSubTheme;
        private bool IsNewThemeAndSub;
        private bool isNewSubTheme;
        private Course newCourse;
        private bool courseUpdate;
        private User userFTP;

        public Afaroid(User userFTP)
        {
            this.userFTP = userFTP;
            checkConnDb();
            InitializeComponent();
            textBox3.PasswordChar = '*';
            MyTree = new TreeAfaroid();
            listesOfResOfCourse = new List<Resource>();
            listesOfExercise = new List<Exercise>();
            newCourse = new Course();
            ChooseDifficulty.SelectedValue = Difficulty.BEGINNER;
            NewTheme = null;
            LeSubTheme = null;
            panel3.Visible = true;
            panel1.Visible = false;
            panel2.Visible = false;
            panel4.Visible = false;
        }
        /// <summary>
        /// Permet de verifier la connexion à la base de données. Si la connexion est Ok l'application se lance
        /// correctement avec les données dans l'arbre, sinon on informe l'utilisateur que la connexion avec la base
        /// de données a échoué et qu'il ne peut pas visualiser les données ni faire aucun traitement
        /// </summary>
        private void checkConnDb()
        {
            bool ConnOK = ConnectWithDB();
            if (!ConnOK)
            {
                MessageBox.Show("Impossible de se connecter à la base de données ! Vous ne pouvez pas visualiser les données", "Afaroid",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning);
                closeAPP();
            }
        }

        public void closeAPP()
        {
            if (!this.Created)
            {
                Timer oTimer = new Timer();
                oTimer.Tick += delegate { this.closeAPP(); };
                oTimer.Interval = 500;
                oTimer.Start();
                return;
            }
            this.Dispose();
            this.Close();
            Application.ExitThread();
            Application.Exit();
            
        }

        /// <summary>
        /// Permet de se connecter à la base de données avec la configuration du DBsingleton
        /// </summary>
        /// <returns>retourne true si la connexion est effectué avec succés , sinon elle retourne false</returns>
        private bool ConnectWithDB()
        {
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;

                return true;
            }

            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                return false;
            }
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            NewTheme = null;
            LeSubTheme = null;
            comboBoxTheme.Text = "";
            comboBoxSubTheme.Text = "";
            List<Control> listControls = new List<Control>();
            foreach (Control control in flowLayoutPanel1.Controls)
            {
                listControls.Add(control);
            }
            foreach (Control control in listControls)
            {
                flowLayoutPanel1.Controls.Remove(control);
                control.Dispose();
            }
            courseUpdate = false;
            newCourse = new Course();
            panel3.Visible = false;
            panel2.Visible = false;
            panel1.Visible = true;
            panel4.Visible = true;
            panel4.BringToFront();
            panel4.Visible = true;
            this.comboBoxTheme.Items.Clear();

            foreach (Theme theme in this.MyTree.ListOfThemes)
            {
                ComboboxItem comboTheme = new ComboboxItem(theme.IdTheme, theme.Name);
                this.comboBoxTheme.Items.Add(comboTheme);
                if (comboBoxTheme.SelectedItem != null)
                {
                    if (theme.Name == comboBoxTheme.SelectedItem.ToString())
                    {
                        foreach (SubTheme subtheme in theme.ListOfSubThemes)
                        {
                            ComboboxItem comboSubTheme = new ComboboxItem(subtheme.IdSubTheme, subtheme.Name);
                            this.comboBoxTheme.Items.Add(comboSubTheme);
                        }
                    }
                }
            }
            this.comboBoxTheme.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxSubTheme.DropDownStyle = ComboBoxStyle.DropDownList;

            loadComboDifficulty();
        }

        private void load_course()
        {
            panel4.Visible = false;
            panel3.Visible = false;
            panel2.Visible = false;
            panel1.BringToFront();
            panel1.Visible = true;
        }
        /// <summary>
        /// envoie de la base de donnée ( clone dans le fichier backup.sql puis envoie au serveur)
        /// envoie des ressources au serveur distant via ftp dans le dossier www/afaroid
        /// </summary>
        private void button5_Click(object sender, EventArgs e)
        {
            string file = "backup.sql";

            using (MySqlConnection conn = DBSingleton.Connection)
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (MySqlBackup mb = new MySqlBackup(cmd))
                    {
                        cmd.Connection = conn;
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        mb.ExportToFile(file);
                        conn.Close();
                    }
                }
            }
            string ftpServerIP = "ftp.cluster023.hosting.ovh.net";
            string ftpUserName = this.userFTP.FTPUsername;
            string ftpPassword = this.userFTP.FTPPassword;
            string localFilePath = "backup.sql";
            try
            {
                using (SftpClient client = new SftpClient(ftpServerIP, 22, ftpUserName, ftpPassword))
                {
                    client.Connect();
                    client.ChangeDirectory("www/afaroid");
                    using (FileStream fs = new FileStream(localFilePath, FileMode.Open))
                    {
                        client.BufferSize = 4 * 1024;
                        client.UploadFile(fs, Path.GetFileName(localFilePath));
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.ToString());
            }
            HttpWebRequest request = WebRequest.Create("http://afaroid.com/afaroid/src/updateFromClient.php") as HttpWebRequest;
            //optional
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            Stream stream = response.GetResponseStream();
            // Read the file and display it line by line.  
            System.IO.StreamReader fileTexttoken = new System.IO.StreamReader(@"cache.txt");
            while ((localFilePath = fileTexttoken.ReadLine()) != null)
            {
                try
                {
                    using (SftpClient client = new SftpClient(ftpServerIP, 22, ftpUserName, ftpPassword))
                    {
                        client.Connect();
                        client.ChangeDirectory("www/afaroid/resources");
                        using (FileStream fs = new FileStream(localFilePath, FileMode.Open))
                        {
                            client.BufferSize = 4 * 1024;
                            client.UploadFile(fs, Path.GetFileName(localFilePath));
                        }
                    }
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.ToString());
                }
            }
            fileTexttoken.Close();
            StreamWriter sw = new StreamWriter(@"cache.txt");
            sw.Close();
            MessageBox.Show("La synchronisation avec le serveur a été faite avec succès", "Afaroid",

                           MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }


        /// <summary>
        /// Fermer l'application
        /// </summary>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
        private void button3_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialogExampleForm dialog = new FolderBrowserDialogExampleForm(flowLayoutPanel1, newCourse);
            listesOfResOfCourse = dialog.ListesOfResOfCourse;
        }
        private void parametresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel3.Visible = false;
            panel1.Visible = true;
            panel2.Visible = true;
            panel2.BringToFront();
        }
        private void button7_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.BringToFront();
            panel3.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            adresse = textBox1.Text;
            login = textBox2.Text;
            password = textBox3.Text;
            DBSingleton.setConn(adresse, "afaroiddb", login, password);
            try
            {
                MySqlConnection MyConnection = DBSingleton.Connection;
                MessageBox.Show(" succes connection ? ");
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
                MessageBox.Show(" fail connection ? " + exp.ToString());
            }
            Afaroid_Load(sender, e);
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.BringToFront();
            panel3.Visible = true;
        }
        /// <summary>
        /// Permet de  modifier ou ajouter un cours lors du clic sur le bouton valider. 
        /// Elle synchronise egalement l'arbre de l'application et lance la fenêtre d'ajout d'un vocabulaire
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Etes-vous sûr ?", "Afaroid", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                // Si le cas d'une modification
                if (courseUpdate)
                {
                    try
                    {
                        if (ChooseDifficulty.SelectedItem != null)
                            newCourse.Mydifficulty = (Difficulty)ChooseDifficulty.SelectedItem;
                    }
                    catch (ArgumentException ex)
                    {
                        throw new ArgumentException("Error-" + ex);
                    }

                    LeSubTheme.UpdateCourse(newCourse);

                    comboBoxTheme.Text = "";

                    comboBoxSubTheme.Text = "";

                    List<Control> listControls = new List<Control>();
                    foreach (Control control in flowLayoutPanel1.Controls)
                    {
                        listControls.Add(control);
                    }

                    foreach (Control control in listControls)
                    {
                        flowLayoutPanel1.Controls.Remove(control);
                        control.Dispose();
                    }
                    refreshTree();
                }
                // Si le cas d'une insertion
                else
                {
                    Difficulty difficulty = Difficulty.BEGINNER;
                    try
                    {
                        if (ChooseDifficulty.SelectedItem != null)
                            difficulty = (Difficulty)ChooseDifficulty.SelectedItem;
                    }
                    catch (ArgumentException ex)
                    {
                        throw new ArgumentException("Error-" + ex);
                    }
                    List<Resource> courseResources = new List<Resource>();
                    foreach (Resource rc in listesOfResOfCourse)
                    {
                        Resource rcNew = rc.clone();
                        courseResources.Add(rcNew);
                    }

                    newCourse = new Course(0, difficulty, false, courseResources);

                    newCourse.MyStatePush = StatePush.NEW;

                    // Si le theme et sous theme sont nouveaux

                    if (IsNewThemeAndSub)
                    {
                        NewTheme.Image.add();

                        NewTheme.Image.IDresource = maxIDinDB("Resources", "ResourceID");

                        MyTree.PushTheme(NewTheme);


                        NewTheme.IdTheme = maxIDinDB("Themes", "ThemeID");

                        LeSubTheme.Image.add();

                        LeSubTheme.Image.IDresource = maxIDinDB("Resources", "ResourceID");

                        NewTheme.PushSubTheme(LeSubTheme);

                        LeSubTheme.IdSubTheme = maxIDinDB("SubThemes", "SubThemeID");

                        int maxId = maxIDinDB("Resources", "ResourceID");

                        LeSubTheme.PushCourse(newCourse);

                        int maxIdAfterPush = maxIDinDB("Resources", "ResourceID");

                        MySqlConnection MyConnection = DBSingleton.Connection;

                        if (MyConnection.State == System.Data.ConnectionState.Closed)
                            MyConnection.Open();
                        for (int i = maxId + 1; i <= maxIdAfterPush; i++)
                        {
                            if (MyConnection.State == System.Data.ConnectionState.Closed)
                                MyConnection.Open();
                            MySqlCommand myCommand = new MySqlCommand("INSERT INTO courseshasresources  (`Courses_CourseID`, `Resources_ResourceID`) VALUES (" + newCourse.IdCourse + "," + i + ")", MyConnection);
                            myCommand.ExecuteNonQuery();
                            myCommand.Connection.Close();
                        }
                        // synchronisation avec l'arbre
                        NewTheme.MyStatePush = StatePush.NEW;
                        NewTheme.Image.STATEPush = StatePush.NEW;
                        MyTree.ListOfThemes.Add(NewTheme);
                        LeSubTheme.MyStatePush = StatePush.NEW;
                        LeSubTheme.Image.STATEPush = StatePush.NEW;
                        NewTheme.ListOfSubThemes.Add(LeSubTheme);
                        LeSubTheme.ListOfCourses.Add(newCourse);
                    }
                    else if (isNewSubTheme)
                    {
                        LeSubTheme.Image.add();
                        LeSubTheme.Image.IDresource = maxIDinDB("Resources", "ResourceID");
                        NewTheme.PushSubTheme(LeSubTheme);
                        LeSubTheme.IdSubTheme = maxIDinDB("SubThemes", "SubThemeID");
                        int maxId = maxIDinDB("Resources", "ResourceID");
                        LeSubTheme.PushCourse(newCourse);
                        int maxIdAfterPush = maxIDinDB("Resources", "ResourceID");
                        MySqlConnection MyConnection = DBSingleton.Connection;
                        if (MyConnection.State == System.Data.ConnectionState.Closed)
                            MyConnection.Open();
                        for (int i = maxId + 1; i <= maxIdAfterPush; i++)
                        {
                            if (MyConnection.State == System.Data.ConnectionState.Closed)
                                MyConnection.Open();
                            MySqlCommand myCommand = new MySqlCommand("INSERT INTO courseshasresources  (`Courses_CourseID`, `Resources_ResourceID`) VALUES (" + newCourse.IdCourse + "," + i + ")", MyConnection);
                            myCommand.ExecuteNonQuery();
                            myCommand.Connection.Close();
                        }
                        // synchronisation avec l'arbre
                        LeSubTheme.MyStatePush = StatePush.NEW;
                        LeSubTheme.Image.STATEPush = StatePush.NEW;
                        NewTheme.ListOfSubThemes.Add(LeSubTheme);
                        LeSubTheme.ListOfCourses.Add(newCourse);
                    }
                    //sinon
                    else
                    {
                        ChooseDifficulty.SelectedItem = null;
                        int maxId = maxIDinDB("Resources", "ResourceID");
                        foreach (Theme theme in this.MyTree.ListOfThemes)
                        {
                            ComboboxItem comboTheme = (ComboboxItem)comboBoxTheme.SelectedItem;
                            if (theme.IdTheme == comboTheme.ID)
                            {
                                foreach (SubTheme subtheme in theme.ListOfSubThemes)
                                {
                                    ComboboxItem comboSubTheme = (ComboboxItem)comboBoxSubTheme.SelectedItem;
                                    if (subtheme.IdSubTheme == comboSubTheme.ID)
                                    {
                                        subtheme.AddCourse(newCourse);
                                        subtheme.PushCourse(newCourse);
                                        break;

                                    }
                                }
                            }
                        }
                        int maxIdAfterPush = maxIDinDB("Resources", "ResourceID");
                        MySqlConnection MyConnection = DBSingleton.Connection;
                        if (MyConnection.State == System.Data.ConnectionState.Closed)
                            MyConnection.Open();
                        for (int i = maxId + 1; i <= maxIdAfterPush; i++)
                        {
                            if (MyConnection.State == System.Data.ConnectionState.Closed)
                                MyConnection.Open();
                            MySqlCommand myCommand = new MySqlCommand("INSERT INTO courseshasresources  (`Courses_CourseID`, `Resources_ResourceID`) VALUES (" + newCourse.IdCourse + "," + i + ")", MyConnection);
                            myCommand.ExecuteNonQuery();
                            myCommand.Connection.Close();
                        }
                    }
                    // mis à jour de l'arbre
                    refreshTree();
                    listesOfResOfCourse.Clear();
                    listesOfResOfCourse = new List<Resource>();
                    comboBoxTheme.Text = "";
                    comboBoxSubTheme.Text = "";
                    List<Control> listControls = new List<Control>();
                    foreach (Control control in flowLayoutPanel1.Controls)
                    {
                        listControls.Add(control);
                    }
                    foreach (Control control in listControls)
                    {
                        flowLayoutPanel1.Controls.Remove(control);
                        control.Dispose();
                    }
                    // Lancer la fenetre d'ajout du vocabulaire
                    if (IsNewThemeAndSub || isNewSubTheme)
                    {
                        VocabulaireIHM vcbIHM = new VocabulaireIHM(LeSubTheme, newCourse.Mydifficulty, this);
                        vcbIHM.Show();
                    }
                    else
                    {
                        foreach (Theme theme in this.MyTree.ListOfThemes)
                        {
                            ComboboxItem comboTheme = (ComboboxItem)comboBoxTheme.SelectedItem;
                            if (theme.IdTheme == comboTheme.ID)
                            {
                                foreach (SubTheme subtheme in theme.ListOfSubThemes)
                                {
                                    ComboboxItem comboSubTheme = (ComboboxItem)comboBoxSubTheme.SelectedItem;
                                    if (subtheme.IdSubTheme == comboSubTheme.ID)
                                    {
                                        VocabulaireIHM vcbIHM = new VocabulaireIHM(subtheme, newCourse.Mydifficulty, this);
                                        vcbIHM.Show();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    NewTheme = null;
                    LeSubTheme = null;
                }
                panel1.Visible = false;
                panel3.Visible = true;
                panel3.BringToFront();
            }
        }

        private void showVocabularyView()
        {
            // Si le theme et sous theme sont nouveaux
            if (IsNewThemeAndSub)
            {
                NewTheme.Image.add();

                NewTheme.Image.IDresource = maxIDinDB("Resources", "ResourceID");

                MyTree.PushTheme(NewTheme);

                NewTheme.IdTheme = maxIDinDB("Themes", "ThemeID");

                LeSubTheme.Image.add();

                LeSubTheme.Image.IDresource = maxIDinDB("Resources", "ResourceID");

                NewTheme.PushSubTheme(LeSubTheme);

                LeSubTheme.IdSubTheme = maxIDinDB("SubThemes", "SubThemeID");

                // synchronisation avec l'arbre
                NewTheme.MyStatePush = StatePush.NEW;
                NewTheme.Image.STATEPush = StatePush.NEW;
                MyTree.ListOfThemes.Add(NewTheme);
                LeSubTheme.MyStatePush = StatePush.NEW;
                LeSubTheme.Image.STATEPush = StatePush.NEW;
                NewTheme.ListOfSubThemes.Add(LeSubTheme);
            }

            // si uniquement le sous-theme est nouveaux
            if (isNewSubTheme)
            {
                LeSubTheme.Image.add();
                LeSubTheme.Image.IDresource = maxIDinDB("Resources", "ResourceID");
                NewTheme.PushSubTheme(LeSubTheme);
                LeSubTheme.IdSubTheme = maxIDinDB("SubThemes", "SubThemeID");

                // synchronisation avec l'arbre
                LeSubTheme.MyStatePush = StatePush.NEW;
                LeSubTheme.Image.STATEPush = StatePush.NEW;
                NewTheme.ListOfSubThemes.Add(LeSubTheme);
            }

            // mis à jour de l'arbre
            refreshTree();
            comboBoxTheme.Text = "";
            comboBoxSubTheme.Text = "";


            Difficulty difficulty = (Difficulty)DifficultycomboBox.SelectedItem;

            // Lancer la fenetre d'ajout du vocabulaire
            if (IsNewThemeAndSub || isNewSubTheme)
            {

                VocabulaireIHM vcbIHM = new VocabulaireIHM(LeSubTheme, difficulty, this);
                vcbIHM.Show();
            }
            else
            {
                foreach (Theme theme in this.MyTree.ListOfThemes)
                {
                    ComboboxItem comboTheme = (ComboboxItem)comboBoxTheme.SelectedItem;
                    if (theme.IdTheme == comboTheme.ID)
                    {
                        foreach (SubTheme subtheme in theme.ListOfSubThemes)
                        {
                            ComboboxItem comboSubTheme = (ComboboxItem)comboBoxSubTheme.SelectedItem;
                            if (subtheme.IdSubTheme == comboSubTheme.ID)
                            {
                                VocabulaireIHM vcbIHM = new VocabulaireIHM(subtheme, difficulty, this);
                                vcbIHM.Show();
                                break;
                            }
                        }
                    }
                }
            }
            NewTheme = null;
            LeSubTheme = null;

        }


        private void button2_Click(object sender, EventArgs e)
        {
            comboBoxSubTheme.Items.Clear();
            comboBoxTheme.SelectedItem = null;
            listesOfResOfCourse.Clear();
            listesOfExercise.Clear();
            comboBoxTheme.Text = "";
            comboBoxSubTheme.Text = "";
            List<Control> listControls = new List<Control>();
            foreach (Control control in flowLayoutPanel1.Controls)
            {
                listControls.Add(control);
            }
            foreach (Control control in listControls)
            {
                flowLayoutPanel1.Controls.Remove(control);
                control.Dispose();
            }

            panel1.Visible = false;

            panel3.Visible = true;

            panel3.BringToFront();
        }
        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            flowLayoutPanel1.Location = new Point(flowLayoutPanel1.Left, -(vScrollBar1.Value * flowLayoutPanel1.Height) / 100);
        }
        /// <summary>
        /// Permet de gerer si l'utilisateur a bien choisi une combinaison de theme et sous-theme, ou il a creé un nouveau
        /// theme et sous-theme pour pouvoir passer à l'etape suivante d'ajout du ressources du cours
        /// </summary>
        private void SaveChoiceTheme_Click_1(object sender, EventArgs e)
        {
            if (comboBoxSubTheme.SelectedItem == null && LeSubTheme != null && NewTheme == null)
            {
                if (LeSubTheme == null)
                {
                    MessageBox.Show("Veuillez selectionner ou ajouter un sous-thème", "Afaroid",

                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                else
                {
                    foreach (Theme theme in this.MyTree.ListOfThemes)
                    {
                        ComboboxItem comboTheme = (ComboboxItem)comboBoxTheme.SelectedItem;
                        if (theme.Name == comboTheme.Name)
                        {
                            NewTheme = theme;
                            break;
                        }
                    }
                    isNewSubTheme = true;
                    //load_course();
                    showVocabularyView();

                }
            }

            else
            {
                if (comboBoxTheme.SelectedItem == null || comboBoxSubTheme.SelectedItem == null)
                {
                    if (NewTheme == null || LeSubTheme == null)
                    {
                        MessageBox.Show("Veuillez selectionner ou ajouter une combinaison de theme et sous-theme", "Afaroid",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        IsNewThemeAndSub = true;
                        showVocabularyView();
                    }
                }
                else
                {
                    bool checkCourseExist = true;
                    ComboboxItem combo = (ComboboxItem)comboBoxSubTheme.SelectedItem;
                    foreach (Theme theme in this.MyTree.ListOfThemes)
                    {
                        ComboboxItem comboTheme = (ComboboxItem)comboBoxTheme.SelectedItem;
                        if (theme.IdTheme == comboTheme.ID)
                        {
                            foreach (SubTheme st in theme.ListOfSubThemes)
                            {

                                if (st.IdSubTheme == combo.ID)
                                {
                                    if (st.ListOfCourses.Count == 0)
                                    {
                                        checkCourseExist = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!checkCourseExist)
                    {
                        foreach (Theme theme in this.MyTree.ListOfThemes)
                        {
                            ComboboxItem comboTheme = (ComboboxItem)comboBoxTheme.SelectedItem;
                            if (theme.Name == comboTheme.Name)
                            {
                                NewTheme = theme;
                                foreach (SubTheme subtheme in theme.ListOfSubThemes)
                                {
                                    LeSubTheme = subtheme;
                                }
                            }
                        }
                        IsNewThemeAndSub = false;
                        showVocabularyView();
                    }
                    else
                    {
                        MessageBox.Show("Operation Interdite ! Cette combinaison de theme et sous-theme contient dèja un cours", "Afaroid",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void CancelChoiceTheme_Click(object sender, EventArgs e)
        {
            comboBoxSubTheme.Items.Clear();
            comboBoxTheme.SelectedItem = null;
            panel1.Visible = false;
            panel4.Visible = false;
            panel3.Visible = true;
            panel3.BringToFront();
        }
        /// <summary>
        /// Permet de d'afficher la fenêtre d'ajout d'un nouveau theme lors du clik sur le boutton
        /// Ajouter un Theme
        /// </summary>
        private void ImageTheme_Click(object sender, EventArgs e)
        {
            NewTheme = new Theme();
            AjouterThemeIHM ajtThemeIHM = new AjouterThemeIHM(NewTheme);
            ajtThemeIHM.Show();
        }
        /// <summary>
        /// Permet de d'afficher la fenêtre d'ajout d'un nouveau sous-theme lors du clik sur le boutton
        /// Ajouter un sous-Theme
        /// </summary>
        private void ImageSubTheme_Click(object sender, EventArgs e)
        {
            LeSubTheme = new SubTheme();
            AjouterThemeIHM ajtThemeIHM = new AjouterThemeIHM(LeSubTheme);
            ajtThemeIHM.Show();
        }

        /// <summary>
        /// permet d'effectuer un traitement après l'initialisation des composantes graphiques. Elle rempli la liste 
        /// deroulante du theme et sous-theme
        /// </summary>
        private void Afaroid_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            this.comboBoxTheme.DisplayMember = "name";
            this.comboBoxSubTheme.DisplayMember = "name";
            this.populateTreeview();
            treeView1.Nodes[0].Expand();
            foreach (Theme theme in this.MyTree.ListOfThemes)
            {
                ComboboxItem comboTheme = new ComboboxItem(theme.IdTheme, theme.Name);
                this.comboBoxTheme.Items.Add(comboTheme);
                if (comboBoxTheme.SelectedItem != null)
                {
                    if (theme.Name == comboBoxTheme.SelectedItem.ToString())
                    {
                        foreach (SubTheme subtheme in theme.ListOfSubThemes)
                        {
                            ComboboxItem comboSubTheme = new ComboboxItem(subtheme.IdSubTheme, subtheme.Name);
                            this.comboBoxTheme.Items.Add(comboSubTheme);
                        }
                    }
                }
            }
            this.comboBoxTheme.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxSubTheme.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        /// <summary>
        /// Permet d'afficher l'architecture de l'application dans un arbre, Elle charge les données depuis la base de données
        /// et les classe par theme, sous-theme, cours, vocabulaire et ressource. 
        /// Elle est utilisé également lors du synchronisation quand on a un ajout, une modification ou une suppression.
        /// Elle permet aussi de visualiser l'etat des données si ils sont ajoutées modifiées ou supprimées à partir du jeu 
        /// de couleur des noeuds de l'arbre . 
        /// </summary>
        public void populateTreeview()
        {
            TreeNode rac = new TreeNode();
            rac.Text = "Afaroid";
            treeView1.Nodes.Add(rac);
            foreach (Theme theme in MyTree.ListOfThemes)
            {
                TreeNode themeNode = new TreeNode();

                themeNode.Name = "Theme";

                themeNode.Text = "Thème : " + theme.Name;

                themeNode.Tag = theme;

                if (theme.MyStatePush == StatePush.NONE)
                {
                    themeNode.ForeColor = Color.Green;
                }
                if (theme.MyStatePush == StatePush.UPDATE || theme.MyStatePush == StatePush.NEW)
                {
                    themeNode.BackColor = Color.Yellow;
                }
                themeNode.ContextMenuStrip = nodeMenu;
                treeView1.TopNode.Nodes.Add(themeNode);
                TreeNode ImageThemeNode = new TreeNode();
                ImageThemeNode.Name = "Resource";
                ImageThemeNode.Text = "Ressource " + theme.Image.Type + " : " + theme.Image.TextToken;
                ImageThemeNode.Tag = theme.Image;
                if (theme.Image.STATEPush == StatePush.NONE)
                {
                    ImageThemeNode.ForeColor = Color.Green;
                }
                if (theme.Image.STATEPush == StatePush.UPDATE || theme.Image.STATEPush == StatePush.NEW)
                {
                    ImageThemeNode.BackColor = Color.Yellow;
                }
                themeNode.Nodes.Add(ImageThemeNode);
                foreach (SubTheme st in theme.ListOfSubThemes)
                {
                    TreeNode subThemeNode = new TreeNode();
                    subThemeNode.Name = "SubTheme";
                    subThemeNode.Text = "Sous-thème : " + st.Name;
                    subThemeNode.Tag = st;
                    if (st.MyStatePush == StatePush.NONE)
                    {
                        subThemeNode.ForeColor = Color.Green;
                    }
                    if (st.MyStatePush == StatePush.UPDATE || st.Image.STATEPush == StatePush.NEW)
                    {
                        subThemeNode.BackColor = Color.Yellow;
                    }
                    subThemeNode.ContextMenuStrip = nodeMenu;
                    themeNode.Nodes.Add(subThemeNode);
                    TreeNode ImageSubThemeNode = new TreeNode();
                    ImageSubThemeNode.Name = "Resource";
                    ImageSubThemeNode.Text = "Ressource " + st.Image.Type + " : " + st.Image.TextToken;
                    ImageSubThemeNode.Tag = st.Image;
                    if (st.Image.STATEPush == StatePush.NONE)
                    {
                        ImageSubThemeNode.ForeColor = Color.Green;
                    }
                    if (st.Image.STATEPush == StatePush.UPDATE || st.Image.STATEPush == StatePush.NEW)
                    {
                        ImageSubThemeNode.BackColor = Color.Yellow;
                    }
                    subThemeNode.Nodes.Add(ImageSubThemeNode);
                    int i = 1;
                    foreach (Course course in st.ListOfCourses)
                    {
                        TreeNode courseNode = new TreeNode();
                        if (!course.IsDynamic)
                        {
                            courseNode.Name = "Course";
                            courseNode.Text = "Cours : " + i + " Difficulté = " + course.Mydifficulty;
                            courseNode.Tag = course;
                            if (course.MyStatePush == StatePush.NONE)
                            {
                                courseNode.ForeColor = Color.Green;
                            }
                            if (course.MyStatePush == StatePush.UPDATE || course.MyStatePush == StatePush.NEW)
                            {
                                courseNode.BackColor = Color.Yellow;
                            }
                            courseNode.ContextMenuStrip = nodeMenu;
                            subThemeNode.Nodes.Add(courseNode);
                            i++;
                        }
                        else
                        {
                            courseNode.Name = "Vocabulary";
                            courseNode.Text = "Vocabulaire : Difficulté = " + course.Mydifficulty;
                            courseNode.Tag = course;
                            if (course.MyStatePush == StatePush.NONE)
                            {
                                courseNode.ForeColor = Color.Green;
                            }
                            if (course.MyStatePush == StatePush.UPDATE || course.MyStatePush == StatePush.NEW)
                            {
                                courseNode.BackColor = Color.Yellow;
                            }
                            courseNode.ContextMenuStrip = nodeMenu;
                            subThemeNode.Nodes.Add(courseNode);
                            i++;
                        }
                        foreach (Resource rc in course.ListOfResources)
                        {
                            TreeNode resourceNode = new TreeNode();
                            resourceNode.Name = "Resource";
                            resourceNode.Text = "Ressource " + rc.Type + " : " + rc.TextToken;
                            resourceNode.Tag = rc;
                            if (rc.STATEPush == StatePush.NONE)
                            {
                                resourceNode.ForeColor = Color.Green;
                            }
                            if (rc.STATEPush == StatePush.UPDATE || rc.STATEPush == StatePush.NEW)

                            {
                                resourceNode.BackColor = Color.Yellow;
                            }
                            courseNode.Nodes.Add(resourceNode);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Permet de supprimer un Theme ou sous-theme ou un cours ou un vocabulaire de la base données à partir d'un noeud 
        /// recupéré de l'arbre lors d'un click droit sur le noeud et cliquer sur supprimer
        /// </summary>
        /// <param name="hitTest">le noeud récuperé de l'arbre</param>
        private void deleteFromTree(TreeViewHitTestInfo hitTest)
        {
            if (hitTest.Node != null)

            {
                switch (hitTest.Node.Name)
                {
                    case "Theme":
                        Theme theme = (Theme)(hitTest.Node.Tag);
                        foreach (Theme themeDelete in MyTree.ListOfThemes)
                        {
                            if (themeDelete.IdTheme == theme.IdTheme)
                            {
                                MyTree.DeleteTheme(themeDelete);
                                hitTest.Node.Remove();
                                MyTree.ListOfThemes.Remove(themeDelete);
                                MessageBox.Show("Le theme a été supprimé avec succès", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                break;
                            }
                        }
                        break;
                    case "SubTheme":
                        Theme themeOfsubTheme = (Theme)(hitTest.Node.Parent.Tag);
                        SubTheme subtheme = (SubTheme)(hitTest.Node.Tag);
                        foreach (SubTheme subthemeDelete in themeOfsubTheme.ListOfSubThemes)
                        {
                            if (subthemeDelete.IdSubTheme == subtheme.IdSubTheme)
                            {
                                themeOfsubTheme.DeleteSubTheme(subthemeDelete);
                                hitTest.Node.Remove();
                                themeOfsubTheme.ListOfSubThemes.Remove(subthemeDelete);
                                MessageBox.Show("Le sous-theme a été supprimé avec succès", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                break;
                            }
                        }
                        break;
                    case "Course":
                        SubTheme subthemeOfCourse = (SubTheme)(hitTest.Node.Parent.Tag);
                        Course course = (Course)(hitTest.Node.Tag);
                        foreach (Course courseDelete in subthemeOfCourse.ListOfCourses)
                        {
                            if (courseDelete.IdCourse == course.IdCourse)
                            {
                                subthemeOfCourse.DeleteCourse(courseDelete);
                                hitTest.Node.Remove();
                                subthemeOfCourse.ListOfCourses.Remove(courseDelete);
                                MessageBox.Show("Le cours a été supprimé avec succès", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                break;
                            }
                        }
                        break;
                    case "Vocabulary":
                        SubTheme subthemeOfVocabulary = (SubTheme)(hitTest.Node.Parent.Tag);
                        Course vocabulary = (Course)(hitTest.Node.Tag);
                        foreach (Course courseDelete in subthemeOfVocabulary.ListOfCourses)
                        {
                            if (courseDelete.IdCourse == vocabulary.IdCourse)
                            {
                                subthemeOfVocabulary.DeleteCourse(courseDelete);
                                hitTest.Node.Remove();
                                subthemeOfVocabulary.ListOfCourses.Remove(courseDelete);
                                MessageBox.Show("Le vocabulaire a été supprimé avec succès", "Afaroid",
                                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                break;
                            }
                        }
                        break;
                }
            }
        }
        /// <summary>
        /// Action effectué lors du clic d'un item du menu associé à un noeud.
        /// Elle peut être soit une suppression soit une modification
        /// </summary>
        private void nodeMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Name.Equals("deleteLabel"))
            {
                if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation de supression", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Action si l'utilisateur est sûr                
                    var hitTest = treeView1.HitTest(treeView1.PointToClient
                    (new Point(e.ClickedItem.GetCurrentParent().Left, e.ClickedItem.GetCurrentParent().Top)));
                    deleteFromTree(hitTest);
                }
            }
            if (e.ClickedItem.Name.Equals("renameLabel"))
            {
                var hitTest = treeView1.HitTest(treeView1.PointToClient
                (new Point(e.ClickedItem.GetCurrentParent().Left, e.ClickedItem.GetCurrentParent().Top)));
                switch (hitTest.Node.Name)
                {
                    case "Theme":
                        Theme theme = (Theme)hitTest.Node.Tag;
                        ThemeIHM themeIHM = new ThemeIHM(theme, this);
                        themeIHM.Show();
                        break;
                    case "SubTheme":

                        SubTheme subtheme = (SubTheme)hitTest.Node.Tag;
                        ThemeIHM subthemeIHM = new ThemeIHM(subtheme, this);
                        subthemeIHM.Show();
                        break;
                    case "Vocabulary":
                        Course vocabulary = (Course)hitTest.Node.Tag;
                        VocabulaireUpdateIHM vcbUpdateIHM = new VocabulaireUpdateIHM(vocabulary, this);
                        vcbUpdateIHM.Show();
                        break;
                }
            }

        }

        /// <summary>
        /// Permet d'actualiser la treeview qui affiche l'arbre. Elle est utile pour que l'utilisateur visualise les 
        /// modification qu'il a effectué sur les themes, sous-themes, cours et vocabulaire. 
        /// </summary>
        public void refreshTree()
        {
            treeView1.Nodes.Clear();
            populateTreeview();
            treeView1.Nodes[0].Expand();
        }
        /// <summary>
        /// Action effectué lors d'un double clic sur un noeud de l'arbre
        /// </summary>
        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Name.Equals("Theme"))
            {
                Theme theme = (Theme)e.Node.Tag;
                ThemeIHM themeIHM = new ThemeIHM(theme, this);
                themeIHM.Show();
            }
            if (e.Node.Name.Equals("SubTheme"))
            {
                SubTheme subtheme = (SubTheme)e.Node.Tag;
                ThemeIHM themeIHM = new ThemeIHM(subtheme, this);
                themeIHM.Show();
            }
            if (e.Node.Name.Equals("Course"))
            {
                Course course = (Course)e.Node.Tag;
                LeSubTheme = (SubTheme)e.Node.Parent.Tag;
                comboBoxTheme.Text = "";
                comboBoxSubTheme.Text = "";
                List<Control> listControls = new List<Control>();
                foreach (Control control in flowLayoutPanel1.Controls)
                {
                    listControls.Add(control);
                }
                foreach (Control control in listControls)
                {
                    flowLayoutPanel1.Controls.Remove(control);
                    control.Dispose();
                }
                newCourse = course;
                foreach (Resource rs in course.ListOfResources)
                {
                    Content content = new Content(course, rs);
                    Content previous;
                    flowLayoutPanel1.Controls.Add(content);
                    if (flowLayoutPanel1.Controls.Count < 2)
                    {
                        content.Location = new Point(0, 0);
                    }
                    else
                    {
                        previous = (Content)flowLayoutPanel1.Controls[flowLayoutPanel1.Controls.Count - 2];
                        if (flowLayoutPanel1.Controls.Count > 2)
                        {
                            flowLayoutPanel1.Height += content.Size.Height + SPACER;
                        }
                        content.Location = new Point(0, previous.Location.Y + previous.Height + SPACER);
                    }
                    content.onRemoveSite += new Content.RemoveSiteEventHandler(RemoveSite_Click);
                    content.index = flowLayoutPanel1.Controls.Count - 1;
                    content.Width = flowLayoutPanel1.Width;
                    content.Anchor = ((System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top
                        | System.Windows.Forms.AnchorStyles.Left
                        | System.Windows.Forms.AnchorStyles.Right));
                    content.AppendMyText(rs.TextToken);
                    switch (rs.Type)
                    {
                        case RC_TYPE.AUDIO: content.AddAudio((Audio)rs); break;
                        case RC_TYPE.IMAGE: content.AddImg((Image)rs); break;
                        case RC_TYPE.TEXT: content.AddTxt((Text)rs); break;
                        default: break;
                    }
                }
                courseUpdate = true;
                panel4.Visible = false;
                panel3.Visible = false;
                panel2.Visible = false;
                panel1.BringToFront();
                panel1.Visible = true;

            }
            if (e.Node.Name.Equals("Vocabulary"))
            {
                Course vocabulary = (Course)e.Node.Tag;
                VocabulaireUpdateIHM vcbUpdateIHM = new VocabulaireUpdateIHM(vocabulary, this);
                vcbUpdateIHM.Show();

            }
        }
        private void RemoveSite_Click(Object sender, ContentArgs e)
        {
            Content content = (Content)sender;
            Content updateList;
            for (int i = e.index; i < flowLayoutPanel1.Controls.Count; i++)
            {
                updateList = (Content)flowLayoutPanel1.Controls[i];
                updateList.Location = new Point(0, updateList.Location.Y - updateList.Height - SPACER);
                updateList.index = i - 1;
            }
            flowLayoutPanel1.Controls.RemoveAt(e.index);
            flowLayoutPanel1.Height -= content.Height;
        }
        private void comboBoxTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxSubTheme.Items.Clear();
            foreach (Theme theme in this.MyTree.ListOfThemes)
            {
                if (comboBoxTheme.SelectedItem == null)
                {
                    break;
                }
                else
                {
                    ComboboxItem comboTheme = (ComboboxItem)comboBoxTheme.SelectedItem;
                    if (theme.Name == comboTheme.Name)
                    {
                        foreach (SubTheme subtheme in theme.ListOfSubThemes)
                        {
                            ComboboxItem comboSubTheme = new ComboboxItem(subtheme.IdSubTheme, subtheme.Name);

                            this.comboBoxSubTheme.Items.Add(comboSubTheme);
                        }
                    }
                }
            }

        }



        /// <summary>
        /// Fonction privé de la classe permettant d'obtenir le maximum d'une colonne dans une table dans la base
        /// de données. Elle sera utilisé pour recuperer l'identifiant du dernier enregistrement dans une table.
        /// </summary>
        /// <param name="tableName">le nom de la table</param>
        /// <param name="colonneName">le nom de la colonne</param>
        /// <returns>retourne -1 en cas d'erreur, sinon elle retourne la max de la colonne spécifié en parametre de la
        /// fonction</returns>
        private int maxIDinDB(string tableName, string colonneName)
        {
            int max = 1;
            try
            {

                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();
                MySqlDataReader myReader = null;
                string query = "SELECT MAX(" + colonneName + ") AS MAXID FROM " + tableName;
                MySqlCommand myCommand = new MySqlCommand(query, MyConnection);
                myReader = myCommand.ExecuteReader();
                if (myReader.Read())
                {
                    if (!myReader["MAXID"].ToString().Equals(""))
                        max = (int)myReader["MAXID"];
                }
                myReader.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
            }
            return max;
        }
        /// <summary>
        /// Classe interne representant un Item de la liste deroulante des themes ou sous-themes.
        /// On stocke toute les informations dont on aura besoin pour un theme ou sous-theme
        /// (exemple : le texte qui va apparaitre dans la liste, l'id du theme )
        /// </summary>

        internal class ComboboxItem
        {
            private int id;
            private string name;
            public int ID { get { return id; } set { id = value; } }
            public string Name { get { return name; } set { name = value; } }
            public ComboboxItem(int id, string name)
            {
                this.id = id;
                this.name = name;
            }
        }


        public void loadComboDifficulty()
        {
            DifficultycomboBox.Items.Add(Difficulty.BEGINNER);
            DifficultycomboBox.Items.Add(Difficulty.INTERMEDIATE);
            DifficultycomboBox.Items.Add(Difficulty.ADVANCED);
            DifficultycomboBox.SelectedItem = DifficultycomboBox.Items[0];
            DifficultycomboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }/* */





}