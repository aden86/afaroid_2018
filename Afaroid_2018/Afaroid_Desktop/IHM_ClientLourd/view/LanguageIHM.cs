﻿using MySql.Data.MySqlClient;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace IHM_ClientLourd
{

    /// <summary>
    /// permet de definir les traduction des ressources
    /// </summary>
    public partial class LanguageIHM : Form
    {
        /// <summary>
        /// pert de definir quelle etait la langue de la derniere modification
        /// </summary>
        Language lastmodif = Language.FR;
        /// <summary>
        /// permet de gerer une resource de type image
        /// </summary>
        Image image;
        /// <summary>
        /// recupere les traduction de text jusq'a la sauvegarde
        /// </summary>
        Image imagetmp;
        /// <summary>
        /// id du text temporaire avant insertion dans la bdd
        /// </summary>
        private int idLanguetmp = 0;

        /// <summary>
        /// check si il y a les 3 langues en aa fr au minimum
        /// </summary>
        int istranlated;
        /// <summary>
        /// recupere les traduction de text jusq'a la sauvegarde
        /// </summary>
        Text texttmp;
        /// <summary>
        /// permet de gerer une resource de type text
        /// </summary>
        Text text;
        /// <summary>
        /// permet de gerer une resource de type audio
        /// </summary>
        Audio audio;
        private int textImageAudio;
        private Audio audiotmp;
        private bool existedeja = false;
        /*
         * proprieter
         */
        /// <summary>
        /// permet d'obtenir la ressource audio
        /// permet aussi de la modfier
        /// </summary>
        public Audio Audio { get { return audio; } set { audio = value; } }
        /// <summary>
        /// permet d'obtenir la ressource text
        /// permet aussi de la modfier
        /// </summary>
        public Text Texte { get { return text; } set { text = value; } }
        /// <summary>
        /// permet d'obtenir la ressource image 
        /// permet aussi de la modfier
        /// </summary>
        public Image Image { get { return image; } set { image = value; } }

        public LanguageIHM(Image image)
        {
            textImageAudio = 2;
            this.image = image;

            foreach (TextTranslations trs in image.ListOfTextTranslations)
            {
                if (trs.Language.ToString().Equals(Language.FR.ToString()))
                {
                    idLanguetmp = trs.TextTranslationID;
                }
                if (trs.Language.ToString().Equals(Language.EN.ToString()))
                {
                    existedeja = true;
                }
            }
            if (!existedeja)
            {

                this.image.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 1, image.IDresource, "", Language.AA));
                this.image.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 2, image.IDresource, "", Language.EN));
                this.image.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 3, image.IDresource, "", Language.DE));
                this.image.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 4, image.IDresource, "", Language.AR));
                this.image.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 5, image.IDresource, "", Language.SV));
            }
            this.imagetmp = this.image;
            InitializeComponent();
            SaveLanguage.Enabled = false;
        }
        public LanguageIHM(Text text)
        {
            textImageAudio = 3;
            this.text = text;

            foreach (TextTranslations trs in text.ListOfTextTranslations)
            {
                if (trs.Language.ToString().Equals(Language.FR.ToString()))
                {
                    idLanguetmp = trs.TextTranslationID;
                }
                if (trs.Language.ToString().Equals(Language.EN.ToString()))
                {
                    existedeja = true;
                }
            }
            if (!existedeja)
            {
                this.text.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 1, text.IDresource, "", Language.AA));
                this.text.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 2, text.IDresource, "", Language.EN));
                this.text.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 3, text.IDresource, "", Language.DE));
                this.text.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 4, text.IDresource, "", Language.AR));
                this.text.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 5, text.IDresource, "", Language.SV));
            }
            this.texttmp = this.text;
            InitializeComponent();

            SaveLanguage.Enabled = false;
        }
        public LanguageIHM(Audio audio)
        {

            textImageAudio = 1;
            this.audio = audio;
            foreach (TextTranslations trs in audio.ListOfTextTranslations)
            {
                if (trs.Language.ToString().Equals(Language.FR.ToString()))
                {
                    idLanguetmp = trs.TextTranslationID;
                }
                if (trs.Language.ToString().Equals(Language.EN.ToString()))
                {
                    existedeja = true;
                }
            }
            if (!existedeja)
            {
                this.audio.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 1, audio.IDresource, "", Language.AA));
                this.audio.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 2, audio.IDresource, "", Language.EN));
                this.audio.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 3, audio.IDresource, "", Language.DE));
                this.audio.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 4, audio.IDresource, "", Language.AR));
                this.audio.ListOfTextTranslations.Add(new TextTranslations(idLanguetmp + 5, audio.IDresource, "", Language.SV));
            }
            this.audiotmp = this.audio;
            InitializeComponent();
            SaveLanguage.Enabled = false;
        }

        private void LanguageIHM_Load(object sender, EventArgs e)
        {
            if (this.image != null)
            {
                loadImage(image.TextToken);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// affiche l'image si la ressource est de type image
        /// </summary>
        /// <param name="imagePath">le chemin vers l'image</param>
        private void loadImage(string imagePath)
        {
            try
            {
                Bitmap image1 = new Bitmap(imagePath, true);
                // Set the PictureBox to display the image.
                pictureBox1.Image = image1;
            }
            catch (Exception)
            {
                MessageBox.Show("L'image n'est pas présente sur votre PC ou son chemin a été changé !", "Afaroid",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                pictureBox1.BackColor = Color.Gray;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (richTextBoxLanguage.Modified == true && richTextBoxLanguage.TextLength != 0)
            {
                if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Action si l'utilisateur est sûr
                    richTextBoxLanguage.Text = "" + richTextBoxLanguage.Text;
                }

            }

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void loadComboBox()
        {
            MySqlConnection MyConnection = DBSingleton.Connection;
            MySqlDataReader myReader = null;
            MySqlCommand myCommand = new MySqlCommand("select * from TextTranslations where TextID = " + text.IDresource, MyConnection);
            myReader = myCommand.ExecuteReader();
        }
        /// <summary>
        /// permet de modifier la traduction en fr
        /// </summary>
        private void FR_Click(object sender, EventArgs e)
        {
            istranlated = 0;
            if (textImageAudio == 3)
            {
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }

                foreach (TextTranslations trs in texttmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.FR.ToString())) richTextBoxLanguage.Text = trs.Translation;
            }
            else if (textImageAudio == 2)
            {
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }
                    if (trs.Language == Language.EN || trs.Language == Language.AA || trs.Language == Language.FR)
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.FR.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 1)
            {
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.FR.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            lastmodif = Language.FR;
            if (istranlated > 2) { SaveLanguage.Enabled = true; } else { SaveLanguage.Enabled = false; }
        }
        /// <summary>
        /// sauvegarde les modification faite après confirmation de l'utilisateur 
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            if (richTextBoxLanguage.TextLength != 0)
            {
                if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    switch (textImageAudio)
                    {
                        case 3:
                            foreach (TextTranslations trs in texttmp.ListOfTextTranslations)
                                if (trs.Language.ToString().Equals(lastmodif.ToString()))
                                {
                                    if (richTextBoxLanguage.TextLength != 0)
                                    {
                                        trs.Translation = richTextBoxLanguage.Text;
                                    }
                                    richTextBoxLanguage.Clear();
                                }
                            break;
                        case 2:
                            foreach (TextTranslations trs in imagetmp.ListOfTextTranslations)
                                if (trs.Language.ToString().Equals(lastmodif.ToString()))
                                {
                                    if (richTextBoxLanguage.TextLength != 0)
                                    {
                                        trs.Translation = richTextBoxLanguage.Text;
                                    }
                                    richTextBoxLanguage.Clear();
                                }
                            break;
                        case 1:
                            foreach (TextTranslations trs in audio.ListOfTextTranslations)
                                if (trs.Language.ToString().Equals(lastmodif.ToString()))
                                {
                                    if (richTextBoxLanguage.TextLength != 0)
                                    {
                                        trs.Translation = richTextBoxLanguage.Text;
                                    }
                                    richTextBoxLanguage.Clear();
                                }
                            break;
                        default:
                            break;
                    }
                    text = texttmp;
                    image = imagetmp;
                    audio = audiotmp;
                    this.Close();
                }
            }
        }
        /// <summary>
        /// permet de modifier la traduction en en
        /// </summary>
        private void EN_Click(object sender, EventArgs e)
        {
            istranlated = 0;
            if (textImageAudio == 3)
            {
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.EN.ToString())) richTextBoxLanguage.Text = trs.Translation;
            }
            else if (textImageAudio == 2)
            {
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations)
                {
                    if (trs.Language.Equals(lastmodif))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.EN.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 1)
            {
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.EN.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            lastmodif = Language.EN;
            if (istranlated > 2) { SaveLanguage.Enabled = true; } else { SaveLanguage.Enabled = false; }
        }
        /// <summary>
        /// permet de modifier la traduction en de
        /// </summary>
        private void DE_Click(object sender, EventArgs e)
        {
            istranlated = 0;
            if (textImageAudio == 3)
            {
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.DE.ToString())) richTextBoxLanguage.Text = trs.Translation;
            }
            else if (textImageAudio == 2)
            {
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations)
                {
                    if (trs.Language.Equals(lastmodif))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.DE.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 1)
            {
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.DE.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            lastmodif = Language.DE;
            if (istranlated > 2) { SaveLanguage.Enabled = true; } else { SaveLanguage.Enabled = false; }
        }
        /// <summary>
        /// permet de modifier la traduction en sv
        /// </summary>
        private void SV_Click(object sender, EventArgs e)
        {
            istranlated = 0;
            if (textImageAudio == 3)
            {
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.SV.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 2)
            {
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.SV.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 1)
            {
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.SV.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            lastmodif = Language.SV;
            if (istranlated > 2) { SaveLanguage.Enabled = true; } else { SaveLanguage.Enabled = false; }
        }
        /// <summary>
        /// permet de modifier la traduction en nl
        /// </summary>
        private void AR_Click(object sender, EventArgs e)
        {
            istranlated = 0;
            if (textImageAudio == 3)
            {
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.AR.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 2)
            {
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.AR.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 1)
            {
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.AR.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            lastmodif = Language.AR;
            if (istranlated > 2) { SaveLanguage.Enabled = true; } else { SaveLanguage.Enabled = false; }
        }
        /// <summary>
        /// permet de modifier la traduction en aa
        /// </summary>
        private void AA_Click(object sender, EventArgs e)
        {
            istranlated = 0;
            if (textImageAudio == 3)
            {
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in texttmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.AA.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 2)
            {
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations)
                {
                    if (trs.Language.Equals(lastmodif))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in imagetmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.AA.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            else if (textImageAudio == 1)
            {
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations)
                {
                    if (trs.Language.ToString().Equals(lastmodif.ToString()))
                    {
                        if (richTextBoxLanguage.TextLength != 0)
                        {
                            trs.Translation = richTextBoxLanguage.Text;
                        }
                        richTextBoxLanguage.Clear();
                    }

                    if (trs.Language.ToString().Equals(Language.EN.ToString()) || trs.Language.ToString().Equals(Language.AA.ToString()) || trs.Language.ToString().Equals(Language.FR.ToString()))
                    {
                        if (trs.Translation.Length > 1)
                        {
                            istranlated++;
                        }
                    }
                }
                foreach (TextTranslations trs in audiotmp.ListOfTextTranslations) if (trs.Language.ToString().Equals(Language.AA.ToString())) richTextBoxLanguage.Text = trs.Translation;

            }
            lastmodif = Language.AA;
            if (istranlated > 2) { SaveLanguage.Enabled = true; } else { SaveLanguage.Enabled = false; }
        }

    }
}
