﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;


namespace IHM_ClientLourd
{
    /// <summary>
    /// Classe representant une IHM pour l'ajout d'un Theme ou sous-theme
    /// L'IHM est la meme pour un theme que pour un sous-theme vu qu'ils representent la même chose
    /// </summary>
    public partial class AjouterThemeIHM : Form
    {
        /// <summary>
        /// Le theme à ajouter
        /// </summary>
        private Theme theme;

        /// <summary>
        /// Le sous-Theme à ajouter
        /// </summary>
        private SubTheme subtheme;

        /// <summary>
        /// Flag indiquant si l'iHM va s'afficher pour la création d'un theme ou d'un sous-theme
        /// </summary>
        private bool isTheme;

        /// <summary>
        /// L'image associé au Theme (ou sous-Theme) 
        /// </summary>
        private Image img = new Image();


        /// <summary>
        /// Constructeur de la classe. Il permet d'initialiser toutes les composantes graphique de l'IHM pour un theme
        /// </summary>
        /// <param name="theme">le theme à rajouter, Il sera rempli apres la validation de la création</param>
        public AjouterThemeIHM(Theme theme)
        {
            this.theme = theme;
            this.isTheme = true;
            InitializeComponent();
        }

        /// <summary>
        /// Constructeur de la classe. Il permet d'initialiser toutes les composantes graphique de l'IHM pour un sous-theme
        /// </summary>
        /// <param name="subtheme">le sous-theme à rajouter , Il sera rempli apres la validation de la création</param>
        public AjouterThemeIHM(SubTheme subtheme)
        {
            this.subtheme = subtheme;
            this.isTheme = false;
            InitializeComponent();
        }

        /// <summary>
        /// Methode pour le chargement des composantes graphique du formulaire
        /// Elle sera appelé dans chaque lancement du formulaire
        /// </summary>
        private void AjouterThemeIHM_Load(object sender, EventArgs e)
        {
            this.comboBox1.DisplayMember = "text";
            this.comboBox2.ValueMember = "language";
            showLabelName();
        }

        /// <summary>
        /// Permet de montrer le nom du theme ou sous-theme dans le label correspond au titre
        /// </summary>
        private void showLabelName()
        {
            if (isTheme)
                this.titleLabel.Text = "Ajouter un Theme";
            else
            {
                this.titleLabel.Text = "Ajouter un sous-Theme";
                this.Text = "Ajouter un sous-Theme";
            }

        }

        /// <summary>
        /// Action effectuée si on selectionne un item de la liste deroulante qui correspond au noms du theme
        /// (ou sous-theme) dans les differentes langues
        /// </summary>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboboxItem item = (ComboboxItem)this.comboBox1.SelectedItem;
            string s = item.Text;
            string res = null;
            for (int i = 0; i < s.Length; ++i)
            {
                if (s[i] == ':')
                {
                    res = s.Substring(i + 1, s.Length - i - 1);
                    break;
                }
            }
            nameTextBox.Text = res;
        }

        /// <summary>
        /// Permet d'afficher le panel d'ajout d'un nom dans un language après le click sur le bouton Ajouter langue
        /// </summary>
        private void buttonAjtLangue_Click(object sender, EventArgs e)
        {
            this.comboBox2.Items.Clear();
            this.textBox1.Text = null;
            this.textBox1.Enabled = false;
            loadCombobox2();
            this.panel1.Visible = false;
            this.panel2.Visible = true;
            this.panel2.BringToFront();
        }

        /// <summary>
        /// Permet de charger la liste déroulante contenant les langues non ajoutés
        /// </summary>
        private void loadCombobox2()
        {
            this.comboBox2.DisplayMember = "text";
            this.comboBox2.ValueMember = "language";

            foreach (Language lang in Enum.GetValues(typeof(Language)))
            {
                bool exist = false;
                foreach (ComboboxItem item in this.comboBox1.Items)
                {
                    if ((int)lang == (int)item.Language)
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist && (!lang.ToString().Equals("AA")))
                {
                    string text = lang.ToString();
                    ComboboxItem combobox = new ComboboxItem(text, lang, null);
                    this.comboBox2.Items.Add(combobox);
                }
            }
            this.comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// Action effectuée si on selectionne un item de la liste deroulante des langues non ajoutés
        /// </summary>
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.textBox1.Enabled = true;
        }

        /// <summary>
        /// Permet d'ajouter le nom dans la nouvelle langue dans la premiere liste deroulante qui correspond au noms du theme
        /// (ou sous-theme) dans les differentes langues
        /// </summary>
        private void buttonAjouter_Click(object sender, EventArgs e)
        {
            if (textBox1.Modified == true && textBox1.TextLength != 0)
            {
                if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Action si l'utilisateur est sûr
                    ComboboxItem combo = (ComboboxItem)this.comboBox2.SelectedItem;
                    combo.Text = combo.Language + " : " + this.textBox1.Text;
                    combo.Translation = this.textBox1.Text;
                    this.comboBox1.Items.Add(combo);
                    this.comboBox1.SelectedItem = combo;
                    this.buttonModifierNom.Enabled = true;
                    this.nameTextBox.Enabled = true;
                    this.panel2.Visible = false;
                    this.panel1.Visible = true;
                    this.panel1.BringToFront();
                }
            }
        }
        /// <summary>
        /// Permet d'apparaitre le panel qui contient les informations du theme à rajouter
        /// </summary>
        private void buttonFermer_Click(object sender, EventArgs e)
        {
            this.panel1.Visible = true;
            this.panel2.Visible = false;
            this.panel1.BringToFront();
        }

        /// <summary>
        /// Permet de modifier le nom du theme (ou sous-theme) qui correspond à l'item selectionné dans la liste déroulante 
        /// </summary>
        private void buttonModifierNom_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Modified == true && nameTextBox.TextLength != 0)
            {
                if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Action si l'utilisateur est sûr
                    ComboboxItem item = (ComboboxItem)this.comboBox1.SelectedItem;
                    item.Text = item.Text.Substring(0, 5);
                    item.Text += nameTextBox.Text;
                    item.Translation = nameTextBox.Text;
                    this.comboBox1.Items[this.comboBox1.SelectedIndex] = item;
                }
            }
        }

        /// <summary>
        /// Permet de charger une image dans le pictureBox de l'interface
        /// </summary>
        private void loadImage(string imagePath)
        {
            Bitmap image1 = new Bitmap(imagePath, true);
            // Set the PictureBox to display the image.
            pictureBox1.Image = image1;
        }

        /// <summary>
        /// Permet d'ajouter une image depuis le folder browser de l'ordinateur
        /// </summary>
        private void buttonAjtImage_Click(object sender, EventArgs e)
        {
            img = new Image();
            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(img, true);
            if (img.TextToken != null)
            {
                loadImage(img.TextToken);
                buttonAjtImage.Enabled = false;
                buttonModifier.Enabled = true;
            }
            else
            {
                this.pictureBox1.BackColor = Color.Gray;
            }
        }

        /// <summary>
        /// Permet de changer l'image du theme(ou sous-theme)
        /// </summary>
        private void buttonModifier_Click(object sender, EventArgs e)
        {
            FolderBrowserDialogExampleForm browserImage = new FolderBrowserDialogExampleForm(img, true);
            if (img.TextToken != null)
            {
                loadImage(img.TextToken);
            }
            else
            {
                loadImage(img.TextToken); ;
            }

        }

        /// <summary>
        /// Permet de fermer le formulaire 
        /// </summary>
        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Permet de valider et ajouter le theme(ou sous theme)
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            if (this.comboBox1.Items.Count != 0 && img.TextToken != null)
            {
                bool isEn = false, isFr = false;
                foreach (ComboboxItem item in this.comboBox1.Items)
                {
                    if (item.Language == Language.FR)
                        isFr = true;
                    if (item.Language == Language.EN)
                        isEn = true;
                }
                bool OK = isFr && isEn;
                if (OK)
                {
                    if (MessageBox.Show(this, "Etes-vous sûr ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        //Action si l'utilisateur est sûr
                        if (isTheme)
                        {
                            saveTheme();
                            MessageBox.Show("Le theme est ajouté avecc succes", "Afaroid",
                                            MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }
                        else
                        {
                            saveTheme();
                            MessageBox.Show("Le sous-Theme est ajouté avecc succes", "Afaroid",
                                           MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }

                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez remplir le deux langues obligatoires : Français et Anglais", "Afaroid",
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        /// <summary>
        /// Permer de sauvegarder les informations du theme(ou sous-theme) dans l'objet theme(ou sous-theme) passé en 
        /// parametres du constructeur de la classe
        /// </summary>
        private void saveTheme()
        {

            List<TextTranslations> ListOfTextTranslations = new List<TextTranslations>();
            string name = null;
            int maxTextIDinDB = maxIDinDB("texttranslations", "textid");
            string imageTextToken = null;
            // Création des translations du theme ou soustheme et les ajouter dans la liste
            foreach (Language lang in Enum.GetValues(typeof(Language)))
            {
                bool exist = false;
                foreach (ComboboxItem item in this.comboBox1.Items)
                {
                    if ((int)item.Language == (int)lang)
                    {
                        if (lang == Language.FR)
                            name = item.Translation;

                        TextTranslations trans = new TextTranslations(0, maxTextIDinDB + 1, item.Translation, lang);
                        ListOfTextTranslations.Add(trans);
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                    ListOfTextTranslations.Add(new TextTranslations(0, maxTextIDinDB + 1, "", lang));
            }
            // Création de l'objet Image comme resource du theme ou sous-theme 
            if (img.TextToken != null)
            {
                imageTextToken = img.TextToken;
            }
            Image ImageResource = new Image(0, RC_TYPE.IMAGE, imageTextToken);
            ImageResource.ListOfTextTranslations = ListOfTextTranslations;
            if (isTheme)
            {
                theme.Name = name;
                theme.Image = ImageResource;
            }
            else
            {
                subtheme.Name = name;
                subtheme.Image = ImageResource;
            }

        }


        /// <summary>
        /// Fonction privé de la classe permettant d'obtenir le maximum d'une colonne dans une table dans la base
        /// de données. Elle sera utilisé pour recuperer l'identifiant du dernier enregistrement dans une table.
        /// </summary>
        /// <param name="tableName">le nom de la table</param>
        /// <param name="colonneName">le nom de la colonne</param>
        /// <returns>retourne -1 en cas d'erreur, sinon elle retourne la max de la colonne spécifié en parametre de la
        /// fonction</returns>
        private int maxIDinDB(string tableName, string colonneName)
        {
            int max = 1;
            try
            {

                MySqlConnection MyConnection = DBSingleton.Connection;
                if (MyConnection.State == System.Data.ConnectionState.Closed)
                    MyConnection.Open();
                MySqlDataReader myReader = null;
                string query = "SELECT MAX(" + colonneName + ") AS MAXID FROM " + tableName;
                MySqlCommand myCommand = new MySqlCommand(query, MyConnection);
                myReader = myCommand.ExecuteReader();
                if (myReader.Read())
                {
                    if (!myReader["MAXID"].ToString().Equals(""))
                        max = (int)myReader["MAXID"];
                }
                myReader.Close();
            }
            catch (MySqlException exp)
            {
                System.Console.WriteLine(exp);
            }
            return max;
        }

        /// <summary>
        /// Classe interne representant un Item de la liste deroulante des noms. On stocke toute les informations
        /// dont on aura besoin pour un nom (exemple : le texte qui va apparaitre dans la liste, le language 
        /// et la traduction)
        /// </summary>
        internal class ComboboxItem
        {
            private string text;
            private Language language;
            private string translation;
            public string Text { get { return text; } set { text = value; } }
            public Language Language { get { return language; } set { language = value; } }

            public string Translation { get { return translation; } set { translation = value; } }

            public ComboboxItem(string text, Language language, string translation)
            {
                this.text = text;
                this.language = language;
                this.translation = translation;
            }
        }
    }
}
