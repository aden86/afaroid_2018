﻿

using System.Windows.Forms;





namespace IHM_ClientLourd


{


    partial class Afaroid


    {


        /// <summary>


        /// Variable nécessaire au concepteur.


        /// </summary>


        private System.ComponentModel.IContainer components = null;





        /// <summary>


        /// Nettoyage des ressources utilisées.


        /// </summary>


        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>


        protected override void Dispose(bool disposing)


        {


            if (disposing && (components != null))


            {


                components.Dispose();


            }


            base.Dispose(disposing);


        }





        #region Code généré par le Concepteur Windows Form





        /// <summary>


        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas


        /// le contenu de cette méthode avec l'éditeur de code.


        /// </summary>


        private void InitializeComponent()


        {            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Afaroid));
            this.panel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelRessources = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.panelSaveRessources = new System.Windows.Forms.TableLayoutPanel();
            this.CancelCourse = new System.Windows.Forms.Button();
            this.SaveCourse = new System.Windows.Forms.Button();
            this.panelButton = new System.Windows.Forms.TableLayoutPanel();
            this.ChooseDifficulty = new System.Windows.Forms.ComboBox();
            this.AddResources = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panelTheme = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxTheme = new System.Windows.Forms.ComboBox();
            this.panelSubTheme = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxSubTheme = new System.Windows.Forms.ComboBox();
            this.ImageTheme = new System.Windows.Forms.Button();
            this.ImageSubTheme = new System.Windows.Forms.Button();
            this.SaveChoiceTheme = new System.Windows.Forms.Button();
            this.CancelChoiceTheme = new System.Windows.Forms.Button();
            this.DifficultycomboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTheme = new System.Windows.Forms.TextBox();
            this.textBoxSubTheme = new System.Windows.Forms.TextBox();
            this.panelSaveCon = new System.Windows.Forms.TableLayoutPanel();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newCourseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.parametresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.button5 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.nodeMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteLabel = new System.Windows.Forms.ToolStripMenuItem();
            this.renameLabel = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panelRessources.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelSaveRessources.SuspendLayout();
            this.panelButton.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelTheme.SuspendLayout();
            this.panelSubTheme.SuspendLayout();
            this.panelSaveCon.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.nodeMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSlateGray;
            this.panel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.panel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.panel1.Controls.Add(this.panelRessources, 1, 0);
            this.panel1.Controls.Add(this.panelButton, 0, 0);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 430F));
            this.panel1.Size = new System.Drawing.Size(680, 430);
            this.panel1.TabIndex = 1;
            // 
            // panelRessources
            // 
            this.panelRessources.ColumnCount = 2;
            this.panelRessources.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.panelRessources.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelRessources.Controls.Add(this.groupBox1, 0, 1);
            this.panelRessources.Controls.Add(this.vScrollBar1, 1, 1);
            this.panelRessources.Controls.Add(this.panelSaveRessources, 0, 3);
            this.panelRessources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRessources.Location = new System.Drawing.Point(139, 3);
            this.panelRessources.Name = "panelRessources";
            this.panelRessources.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.20755F));
            this.panelRessources.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.60378F));
            this.panelRessources.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.panelRessources.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelRessources.Size = new System.Drawing.Size(538, 424);
            this.panelRessources.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(0, 56);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(484, 240);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 20);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(475, 218);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.vScrollBar1.Location = new System.Drawing.Point(484, 62);
            this.vScrollBar1.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(23, 234);
            this.vScrollBar1.TabIndex = 3;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            // 
            // panelSaveRessources
            // 
            this.panelSaveRessources.ColumnCount = 2;
            this.panelSaveRessources.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panelSaveRessources.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panelSaveRessources.Controls.Add(this.CancelCourse, 1, 0);
            this.panelSaveRessources.Controls.Add(this.SaveCourse, 0, 0);
            this.panelSaveRessources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSaveRessources.Location = new System.Drawing.Point(0, 380);
            this.panelSaveRessources.Margin = new System.Windows.Forms.Padding(0);
            this.panelSaveRessources.Name = "panelSaveRessources";
            this.panelSaveRessources.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelSaveRessources.Size = new System.Drawing.Size(484, 44);
            this.panelSaveRessources.TabIndex = 0;
            // 
            // CancelCourse
            // 
            this.CancelCourse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelCourse.BackColor = System.Drawing.Color.LightSalmon;
            this.CancelCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelCourse.Location = new System.Drawing.Point(245, 3);
            this.CancelCourse.Name = "CancelCourse";
            this.CancelCourse.Size = new System.Drawing.Size(236, 38);
            this.CancelCourse.TabIndex = 4;
            this.CancelCourse.Text = "Annuler";
            this.CancelCourse.UseVisualStyleBackColor = false;
            this.CancelCourse.Click += new System.EventHandler(this.button2_Click);
            // 
            // SaveCourse
            // 
            this.SaveCourse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveCourse.BackColor = System.Drawing.Color.LightSteelBlue;
            this.SaveCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveCourse.Location = new System.Drawing.Point(3, 3);
            this.SaveCourse.Name = "SaveCourse";
            this.SaveCourse.Size = new System.Drawing.Size(236, 38);
            this.SaveCourse.TabIndex = 1;
            this.SaveCourse.Text = "Valider";
            this.SaveCourse.UseVisualStyleBackColor = false;
            this.SaveCourse.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelButton
            // 
            this.panelButton.ColumnCount = 2;
            this.panelButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.15385F));
            this.panelButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.84615F));
            this.panelButton.Controls.Add(this.ChooseDifficulty, 1, 4);
            this.panelButton.Controls.Add(this.AddResources, 1, 1);
            this.panelButton.Controls.Add(this.label6, 1, 3);
            this.panelButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelButton.Location = new System.Drawing.Point(3, 3);
            this.panelButton.Name = "panelButton";
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.panelButton.Size = new System.Drawing.Size(130, 424);
            this.panelButton.TabIndex = 0;
            // 
            // ChooseDifficulty
            // 
            this.ChooseDifficulty.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChooseDifficulty.DisplayMember = "int";
            this.ChooseDifficulty.FormattingEnabled = true;
            this.ChooseDifficulty.Location = new System.Drawing.Point(50, 339);
            this.ChooseDifficulty.Name = "ChooseDifficulty";
            this.ChooseDifficulty.Size = new System.Drawing.Size(77, 21);
            this.ChooseDifficulty.TabIndex = 27;
            // 
            // AddResources
            // 
            this.AddResources.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddResources.BackColor = System.Drawing.Color.LightSteelBlue;
            this.AddResources.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddResources.Location = new System.Drawing.Point(50, 87);
            this.AddResources.Name = "AddResources";
            this.AddResources.Size = new System.Drawing.Size(77, 78);
            this.AddResources.TabIndex = 5;
            this.AddResources.Text = "Ajouter une ressource";
            this.AddResources.UseVisualStyleBackColor = false;
            this.AddResources.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(50, 300);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 36);
            this.label6.TabIndex = 28;
            this.label6.Text = "Choisir la difficulté :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel4
            // 
            this.panel4.ColumnCount = 4;
            this.panel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.73529F));
            this.panel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.11765F));
            this.panel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.panel4.Controls.Add(this.panelTheme, 1, 1);
            this.panel4.Controls.Add(this.panelSubTheme, 2, 1);
            this.panel4.Controls.Add(this.ImageTheme, 1, 3);
            this.panel4.Controls.Add(this.ImageSubTheme, 2, 3);
            this.panel4.Controls.Add(this.SaveChoiceTheme, 1, 5);
            this.panel4.Controls.Add(this.CancelChoiceTheme, 2, 5);
            this.panel4.Controls.Add(this.DifficultycomboBox, 2, 4);
            this.panel4.Controls.Add(this.label7, 1, 4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.67442F));
            this.panel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.88372F));
            this.panel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.32653F));
            this.panel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.32653F));
            this.panel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.32653F));
            this.panel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.36735F));
            this.panel4.Size = new System.Drawing.Size(680, 430);
            this.panel4.TabIndex = 6;
            // 
            // panelTheme
            // 
            this.panelTheme.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelTheme.Controls.Add(this.label2);
            this.panelTheme.Controls.Add(this.comboBoxTheme);
            this.panelTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTheme.Location = new System.Drawing.Point(178, 79);
            this.panelTheme.Name = "panelTheme";
            this.panelTheme.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelTheme.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelTheme.Size = new System.Drawing.Size(158, 58);
            this.panelTheme.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 29);
            this.label2.TabIndex = 12;
            this.label2.Text = "Choisir un theme";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // comboBoxTheme
            // 
            this.comboBoxTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxTheme.FormattingEnabled = true;
            this.comboBoxTheme.Location = new System.Drawing.Point(3, 32);
            this.comboBoxTheme.Name = "comboBoxTheme";
            this.comboBoxTheme.Size = new System.Drawing.Size(152, 21);
            this.comboBoxTheme.TabIndex = 0;
            this.comboBoxTheme.SelectedIndexChanged += new System.EventHandler(this.comboBoxTheme_SelectedIndexChanged);
            // 
            // panelSubTheme
            // 
            this.panelSubTheme.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelSubTheme.Controls.Add(this.label3, 0, 0);
            this.panelSubTheme.Controls.Add(this.comboBoxSubTheme, 0, 1);
            this.panelSubTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSubTheme.Location = new System.Drawing.Point(339, 76);
            this.panelSubTheme.Margin = new System.Windows.Forms.Padding(0);
            this.panelSubTheme.Name = "panelSubTheme";
            this.panelSubTheme.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panelSubTheme.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panelSubTheme.Size = new System.Drawing.Size(170, 64);
            this.panelSubTheme.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 32);
            this.label3.TabIndex = 13;
            this.label3.Text = "Choisir un sous-theme";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxSubTheme
            // 
            this.comboBoxSubTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxSubTheme.FormattingEnabled = true;
            this.comboBoxSubTheme.Location = new System.Drawing.Point(3, 35);
            this.comboBoxSubTheme.Name = "comboBoxSubTheme";
            this.comboBoxSubTheme.Size = new System.Drawing.Size(164, 21);
            this.comboBoxSubTheme.TabIndex = 1;
            // 
            // ImageTheme
            // 
            this.ImageTheme.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ImageTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageTheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ImageTheme.Location = new System.Drawing.Point(195, 220);
            this.ImageTheme.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
            this.ImageTheme.Name = "ImageTheme";
            this.ImageTheme.Size = new System.Drawing.Size(124, 50);
            this.ImageTheme.TabIndex = 10;
            this.ImageTheme.Text = "Ajouter un theme";
            this.ImageTheme.UseVisualStyleBackColor = false;
            this.ImageTheme.Click += new System.EventHandler(this.ImageTheme_Click);
            // 
            // ImageSubTheme
            // 
            this.ImageSubTheme.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ImageSubTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageSubTheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ImageSubTheme.Location = new System.Drawing.Point(359, 220);
            this.ImageSubTheme.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
            this.ImageSubTheme.Name = "ImageSubTheme";
            this.ImageSubTheme.Size = new System.Drawing.Size(130, 50);
            this.ImageSubTheme.TabIndex = 11;
            this.ImageSubTheme.Text = "Ajouter un sous-theme";
            this.ImageSubTheme.UseVisualStyleBackColor = false;
            this.ImageSubTheme.Click += new System.EventHandler(this.ImageSubTheme_Click);
            // 
            // SaveChoiceTheme
            // 
            this.SaveChoiceTheme.BackColor = System.Drawing.Color.PaleGreen;
            this.SaveChoiceTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveChoiceTheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveChoiceTheme.Location = new System.Drawing.Point(195, 360);
            this.SaveChoiceTheme.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
            this.SaveChoiceTheme.Name = "SaveChoiceTheme";
            this.SaveChoiceTheme.Size = new System.Drawing.Size(124, 60);
            this.SaveChoiceTheme.TabIndex = 16;
            this.SaveChoiceTheme.Text = "Valider";
            this.SaveChoiceTheme.UseVisualStyleBackColor = false;
            this.SaveChoiceTheme.Click += new System.EventHandler(this.SaveChoiceTheme_Click_1);
            // 
            // CancelChoiceTheme
            // 
            this.CancelChoiceTheme.BackColor = System.Drawing.Color.LightSalmon;
            this.CancelChoiceTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelChoiceTheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelChoiceTheme.Location = new System.Drawing.Point(359, 360);
            this.CancelChoiceTheme.Margin = new System.Windows.Forms.Padding(20, 10, 20, 10);
            this.CancelChoiceTheme.Name = "CancelChoiceTheme";
            this.CancelChoiceTheme.Size = new System.Drawing.Size(130, 60);
            this.CancelChoiceTheme.TabIndex = 9;
            this.CancelChoiceTheme.Text = "Annuler";
            this.CancelChoiceTheme.UseVisualStyleBackColor = false;
            this.CancelChoiceTheme.Click += new System.EventHandler(this.CancelChoiceTheme_Click);
            // 
            // DifficultycomboBox
            // 
            this.DifficultycomboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DifficultycomboBox.FormattingEnabled = true;
            this.DifficultycomboBox.Location = new System.Drawing.Point(363, 326);
            this.DifficultycomboBox.Name = "DifficultycomboBox";
            this.DifficultycomboBox.Size = new System.Drawing.Size(121, 21);
            this.DifficultycomboBox.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(178, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(158, 40);
            this.label7.TabIndex = 18;
            this.label7.Text = "Choisir la difficulté du cours :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(173, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 70);
            this.label4.TabIndex = 14;
            this.label4.Text = "Ajouter un Theme";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(343, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 70);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ajouter un sous Theme";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxTheme
            // 
            this.textBoxTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTheme.Location = new System.Drawing.Point(173, 213);
            this.textBoxTheme.Name = "textBoxTheme";
            this.textBoxTheme.Size = new System.Drawing.Size(164, 20);
            this.textBoxTheme.TabIndex = 6;
            // 
            // textBoxSubTheme
            // 
            this.textBoxSubTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSubTheme.Location = new System.Drawing.Point(343, 213);
            this.textBoxSubTheme.Name = "textBoxSubTheme";
            this.textBoxSubTheme.Size = new System.Drawing.Size(164, 20);
            this.textBoxSubTheme.TabIndex = 7;
            // 
            // panelSaveCon
            // 
            this.panelSaveCon.ColumnCount = 2;
            this.panelSaveCon.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panelSaveCon.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panelSaveCon.Controls.Add(this.button6, 0, 0);
            this.panelSaveCon.Controls.Add(this.button7, 1, 0);
            this.panelSaveCon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSaveCon.Location = new System.Drawing.Point(226, 351);
            this.panelSaveCon.Margin = new System.Windows.Forms.Padding(0);
            this.panelSaveCon.Name = "panelSaveCon";
            this.panelSaveCon.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelSaveCon.Size = new System.Drawing.Size(226, 79);
            this.panelSaveCon.TabIndex = 0;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(3, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(107, 73);
            this.button6.TabIndex = 2;
            this.button6.Text = "Valider";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.LightSalmon;
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(116, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(107, 73);
            this.button7.TabIndex = 9;
            this.button7.Text = "Annuler";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel2
            // 
            this.panel2.ColumnCount = 3;
            this.panel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
            this.panel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
            this.panel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3F));
            this.panel2.Controls.Add(this.textBox3, 1, 4);
            this.panel2.Controls.Add(this.panelSaveCon, 1, 5);
            this.panel2.Controls.Add(this.textBox2, 1, 3);
            this.panel2.Controls.Add(this.textBox1, 1, 2);
            this.panel2.Controls.Add(this.label1, 1, 1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.3F));
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.6F));
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.6F));
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.6F));
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.6F));
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.6F));
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.3F));
            this.panel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.3F));
            this.panel2.Size = new System.Drawing.Size(680, 430);
            this.panel2.TabIndex = 9;
            this.panel2.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Location = new System.Drawing.Point(229, 276);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(220, 20);
            this.textBox3.TabIndex = 10;
            this.textBox3.Text = "Mot de passe";
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Location = new System.Drawing.Point(229, 198);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(220, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "Identification";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(229, 120);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(220, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Adresse du serveur";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(229, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 78);
            this.label1.TabIndex = 11;
            this.label1.Text = "Parametre de connexion";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Gray;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(76, 29);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCourseToolStripMenuItem,
            this.toolStripSeparator1,
            this.parametresToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.GhostWhite;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(68, 25);
            this.toolStripMenuItem1.Text = "Fichier";
            // 
            // newCourseToolStripMenuItem
            // 
            this.newCourseToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.newCourseToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.newCourseToolStripMenuItem.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.newCourseToolStripMenuItem.Name = "newCourseToolStripMenuItem";
            this.newCourseToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.newCourseToolStripMenuItem.Text = "Ajouter un cours";
            this.newCourseToolStripMenuItem.Click += new System.EventHandler(this.Form2_Load);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(192, 6);
            // 
            // parametresToolStripMenuItem
            // 
            this.parametresToolStripMenuItem.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.parametresToolStripMenuItem.Name = "parametresToolStripMenuItem";
            this.parametresToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.parametresToolStripMenuItem.Text = "Parametres";
            this.parametresToolStripMenuItem.Click += new System.EventHandler(this.parametresToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(192, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.exitToolStripMenuItem.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(195, 26);
            this.exitToolStripMenuItem.Text = "Quitter";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button5.AutoSize = true;
            this.button5.BackColor = System.Drawing.Color.Gray;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button5.ForeColor = System.Drawing.Color.GhostWhite;
            this.button5.Location = new System.Drawing.Point(0, 393);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(204, 29);
            this.button5.TabIndex = 5;
            this.button5.Text = "Synchroniser avec le serveur";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.Location = new System.Drawing.Point(3, 63);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(0, 0);
            this.panel3.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(898, 430);
            this.panel5.TabIndex = 10;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Controls.Add(this.panel1);
            this.panel7.Controls.Add(this.panel2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(218, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(680, 430);
            this.panel7.TabIndex = 10;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.treeView1);
            this.panel6.Controls.Add(this.button5);
            this.panel6.Controls.Add(this.menuStrip1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(218, 430);
            this.panel6.TabIndex = 10;
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView1.Location = new System.Drawing.Point(-8, 29);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(212, 361);
            this.treeView1.TabIndex = 6;
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
            // 
            // nodeMenu
            // 
            this.nodeMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteLabel,
            this.renameLabel});
            this.nodeMenu.Name = "nodeMenu";
            this.nodeMenu.Size = new System.Drawing.Size(130, 48);
            this.nodeMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.nodeMenu_ItemClicked);
            // 
            // deleteLabel
            // 
            this.deleteLabel.Name = "deleteLabel";
            this.deleteLabel.Size = new System.Drawing.Size(129, 22);
            this.deleteLabel.Text = "Supprimer";
            // 
            // renameLabel
            // 
            this.renameLabel.Name = "renameLabel";
            this.renameLabel.Size = new System.Drawing.Size(129, 22);
            this.renameLabel.Text = "Modifier";
            // 
            // Afaroid
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(898, 430);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Afaroid";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Afaroid";
            this.Load += new System.EventHandler(this.Afaroid_Load);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            this.panel1.ResumeLayout(false);
            this.panelRessources.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panelSaveRessources.ResumeLayout(false);
            this.panelButton.ResumeLayout(false);
            this.panelButton.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panelTheme.ResumeLayout(false);
            this.panelTheme.PerformLayout();
            this.panelSubTheme.ResumeLayout(false);
            this.panelSaveCon.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.nodeMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }





        #endregion


        private System.Windows.Forms.MenuStrip menuStrip1;


        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;


        private System.Windows.Forms.Button SaveCourse;


        private System.Windows.Forms.Button CancelCourse;


        private System.Diagnostics.EventLog eventLog1;


        private System.Windows.Forms.Button button5;


        private System.Windows.Forms.ToolStripMenuItem newCourseToolStripMenuItem;


        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;


        private Button AddResources;


        private ToolStripSeparator toolStripSeparator1;


        private ToolStripMenuItem parametresToolStripMenuItem;


        private ToolTip toolTip1;


        private Panel panel3;


        private TableLayoutPanel panel2;


        private Label label1;


        private TextBox textBox3;


        private Button button7;


        private Button button6;


        private TextBox textBox2;


        private TextBox textBox1;


        private Panel panel7;


        private ComboBox comboBoxSubTheme;


        private ComboBox comboBoxTheme;


        private Button CancelChoiceTheme;


        private ToolStripSeparator toolStripSeparator2;


        private Button ImageSubTheme;


        private Button ImageTheme;


        private Label label3;


        private Label label2;


        private GroupBox groupBox1;


        private FlowLayoutPanel flowLayoutPanel1;


        private VScrollBar vScrollBar1;


        private Panel panel5;


        private Panel panel6;


        private TableLayoutPanel panel4;


        private Button SaveChoiceTheme;


        private ComboBox ChooseDifficulty;


        private TableLayoutPanel panelTheme;


        private TableLayoutPanel panelSubTheme;


        private TableLayoutPanel panelSaveCon;


        private TableLayoutPanel panel1;


        private TableLayoutPanel panelButton;


        private TableLayoutPanel panelRessources;


        private TableLayoutPanel panelSaveRessources;


        private TreeView treeView1;


        private ContextMenuStrip nodeMenu;


        private ToolStripMenuItem deleteLabel;


        private ToolStripMenuItem renameLabel;

        private Label label4;

        private Label label5;

        private TextBox textBoxTheme;

        private TextBox textBoxSubTheme;

        private Label label6;
        private ComboBox DifficultycomboBox;
        private Label label7;

        public FlowLayoutPanel FlowLayoutPanel1 { get { return flowLayoutPanel1; } set { flowLayoutPanel1 = value; } }


    }


}





