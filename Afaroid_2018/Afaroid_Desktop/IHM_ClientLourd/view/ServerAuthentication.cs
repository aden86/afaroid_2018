﻿using IHM_ClientLourd.model;
using System;
using System.Windows.Forms;

namespace IHM_ClientLourd.view
{
    public partial class ServerAuthentication : Form
    {
        public ServerAuthentication()
        {
            InitializeComponent();
        }

        private void ServerAuthentication_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (usernameTextbox.Text != null && passwordTextBox.Text != null)
            {
                string FTPusername = usernameTextbox.Text;
                string FTPpassword = passwordTextBox.Text;

                User userFTP = new User(FTPusername, FTPpassword);
                bool isConnOk = userFTP.connect();
                if (isConnOk)
                {
                    MessageBox.Show("Connexion au serveur distant acceptée ", "Afaroid",

                           MessageBoxButtons.OK, MessageBoxIcon.Asterisk);


                    Afaroid afar = new Afaroid(userFTP);
                    this.Hide();
                    afar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Impossible de se connecter au serveur distant ! L'application va se fermer", "Afaroid",

                               MessageBoxButtons.OK, MessageBoxIcon.Error);

                    this.Close();
                }

            }
            else
            {
                MessageBox.Show("Veuilez remplir tous les champs", "Afaroid",

                           MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
