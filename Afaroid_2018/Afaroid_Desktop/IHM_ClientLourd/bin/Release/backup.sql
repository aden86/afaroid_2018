-- MySqlBackup.NET 2.0.9.2
-- Dump Time: 2017-08-25 17:13:48
-- --------------------------------------
-- Server version 5.7.14 MySQL Community Server (GPL)


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 
-- Definition of courses
-- 

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `CourseID` int(11) NOT NULL AUTO_INCREMENT,
  `SubThemes_SubThemeID` int(11) NOT NULL,
  `Difficulties_DifficultyID` int(11) NOT NULL,
  `IsDynamic` tinyint(4) NOT NULL,
  PRIMARY KEY (`CourseID`),
  KEY `fk_COURS_SOUS_THEME1_idx` (`SubThemes_SubThemeID`),
  KEY `fk_COURS_DIFFICULTE1_idx` (`Difficulties_DifficultyID`),
  CONSTRAINT `fk_COURS_DIFFICULTE1` FOREIGN KEY (`Difficulties_DifficultyID`) REFERENCES `difficulties` (`DifficultyID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COURS_SOUS_THEME1` FOREIGN KEY (`SubThemes_SubThemeID`) REFERENCES `subthemes` (`SubThemeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table courses
-- 

/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses`(`CourseID`,`SubThemes_SubThemeID`,`Difficulties_DifficultyID`,`IsDynamic`) VALUES
(1,1,1,1),
(2,2,1,1),
(3,3,1,1),
(4,4,1,1),
(5,5,1,1),
(6,6,1,1),
(7,7,1,1),
(8,8,1,1),
(9,9,1,1),
(10,10,1,1),
(11,11,1,1),
(12,12,1,1);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- 
-- Definition of courseshasresources
-- 

DROP TABLE IF EXISTS `courseshasresources`;
CREATE TABLE IF NOT EXISTS `courseshasresources` (
  `Courses_CourseID` int(11) NOT NULL,
  `Resources_ResourceID` int(11) NOT NULL,
  `OrderRes` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Courses_CourseID`,`Resources_ResourceID`),
  UNIQUE KEY `CoursesHasResourcescol_UNIQUE` (`OrderRes`),
  KEY `fk_COURS_has_RESSOURCE_RESSOURCE1_idx` (`Resources_ResourceID`),
  KEY `fk_COURS_has_RESSOURCE_COURS1_idx` (`Courses_CourseID`),
  CONSTRAINT `fk_COURS_has_RESSOURCE_COURS1` FOREIGN KEY (`Courses_CourseID`) REFERENCES `courses` (`CourseID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_COURS_has_RESSOURCE_RESSOURCE1` FOREIGN KEY (`Resources_ResourceID`) REFERENCES `resources` (`ResourceID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table courseshasresources
-- 

/*!40000 ALTER TABLE `courseshasresources` DISABLE KEYS */;
INSERT INTO `courseshasresources`(`Courses_CourseID`,`Resources_ResourceID`,`OrderRes`) VALUES
(1,3,1),
(1,4,2),
(1,5,3),
(1,6,4),
(1,7,5),
(1,8,6),
(1,9,7),
(1,10,8),
(1,11,9),
(1,12,10),
(1,13,11),
(1,14,12),
(1,15,13),
(1,16,14),
(2,18,15),
(2,19,16),
(2,20,17),
(2,21,18),
(2,22,19),
(2,23,20),
(2,24,21),
(2,25,22),
(2,26,23),
(2,27,24),
(2,28,25),
(2,29,26),
(2,30,27),
(2,31,28),
(2,32,29),
(2,33,30),
(2,34,31),
(2,35,32),
(3,37,33),
(3,38,34),
(3,39,35),
(3,40,36),
(3,41,37),
(3,42,38),
(3,43,39),
(3,44,40),
(3,45,41),
(3,46,42),
(3,47,43),
(3,48,44),
(3,49,45),
(3,50,46),
(3,51,47),
(3,52,48),
(3,53,49),
(3,54,50),
(3,55,51),
(3,56,52),
(3,57,53),
(3,58,54),
(3,59,55),
(3,60,56),
(3,61,57),
(3,62,58),
(3,63,59),
(3,64,60),
(4,66,61),
(4,67,62),
(4,68,63),
(4,69,64),
(4,70,65),
(4,71,66),
(4,72,67),
(4,73,68),
(4,74,69),
(4,75,70),
(4,76,71),
(4,77,72),
(4,78,73),
(4,79,74),
(5,81,75),
(5,82,76),
(5,83,77),
(5,84,78),
(5,85,79),
(5,86,80),
(5,87,81),
(5,88,82),
(5,89,83),
(5,90,84),
(5,91,85),
(5,92,86),
(5,93,87),
(5,94,88),
(6,96,89),
(6,97,90),
(6,98,91),
(6,99,92),
(6,100,93),
(6,101,94),
(6,102,95),
(6,103,96),
(6,104,97),
(6,105,98),
(6,106,99),
(6,107,100),
(6,108,101),
(6,109,102),
(6,110,103),
(6,111,104),
(6,112,105),
(6,113,106),
(7,115,143),
(7,116,144),
(7,117,145),
(7,118,146),
(7,119,147),
(7,120,148),
(7,121,149),
(7,122,150),
(7,123,151),
(7,124,152),
(7,125,153),
(7,126,154),
(7,127,155),
(7,128,156),
(7,129,157),
(7,130,158),
(7,131,159),
(7,132,160),
(7,133,161),
(7,134,162),
(8,136,163),
(8,137,164),
(8,138,165),
(8,139,166),
(8,140,167),
(8,141,168),
(8,142,169),
(8,143,170),
(8,144,171),
(8,145,172),
(9,147,173),
(9,148,174),
(9,149,175),
(9,150,176),
(9,151,177),
(9,152,178),
(9,153,179),
(9,154,180),
(10,156,181),
(10,157,182),
(10,158,183),
(10,159,184),
(10,160,185),
(10,161,186),
(10,162,187),
(10,163,188),
(10,164,189),
(10,165,190),
(10,166,191),
(10,167,192),
(10,168,193),
(10,169,194),
(10,170,195),
(10,171,196),
(10,172,197),
(10,173,198),
(10,174,199),
(10,175,200),
(10,176,201),
(10,177,202),
(11,180,203),
(11,181,204),
(11,182,205),
(11,183,206),
(11,184,207),
(11,185,208),
(11,186,209),
(11,187,210),
(11,188,211),
(11,189,212),
(12,191,213),
(12,192,214),
(12,193,215),
(12,194,216),
(12,195,217),
(12,196,218),
(12,197,219),
(12,198,220);
/*!40000 ALTER TABLE `courseshasresources` ENABLE KEYS */;

-- 
-- Definition of difficulties
-- 

DROP TABLE IF EXISTS `difficulties`;
CREATE TABLE IF NOT EXISTS `difficulties` (
  `DifficultyID` int(11) NOT NULL AUTO_INCREMENT,
  `Difficulty` varchar(45) NOT NULL,
  PRIMARY KEY (`DifficultyID`),
  UNIQUE KEY `Difficulty_UNIQUE` (`Difficulty`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table difficulties
-- 

/*!40000 ALTER TABLE `difficulties` DISABLE KEYS */;
INSERT INTO `difficulties`(`DifficultyID`,`Difficulty`) VALUES
(3,'ADVANCED'),
(1,'BEGINNER'),
(2,'INTERMEDIATE');
/*!40000 ALTER TABLE `difficulties` ENABLE KEYS */;

-- 
-- Definition of exercises
-- 

DROP TABLE IF EXISTS `exercises`;
CREATE TABLE IF NOT EXISTS `exercises` (
  `ExerciseID` int(11) NOT NULL AUTO_INCREMENT,
  `Courses_CourseId` int(11) NOT NULL,
  `ExerciseTypes_ExerciseTypeID` int(11) NOT NULL,
  PRIMARY KEY (`ExerciseID`),
  KEY `fk_EXERCICE_COURS1_idx` (`Courses_CourseId`),
  KEY `fk_Exercises_ExerciseType1_idx` (`ExerciseTypes_ExerciseTypeID`),
  CONSTRAINT `fk_EXERCICE_COURS1` FOREIGN KEY (`Courses_CourseId`) REFERENCES `courses` (`CourseID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Exercises_ExerciseType1` FOREIGN KEY (`ExerciseTypes_ExerciseTypeID`) REFERENCES `exercisetypes` (`ExerciseTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table exercises
-- 

/*!40000 ALTER TABLE `exercises` DISABLE KEYS */;

/*!40000 ALTER TABLE `exercises` ENABLE KEYS */;

-- 
-- Definition of exercisetypes
-- 

DROP TABLE IF EXISTS `exercisetypes`;
CREATE TABLE IF NOT EXISTS `exercisetypes` (
  `ExerciseTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(45) NOT NULL,
  PRIMARY KEY (`ExerciseTypeID`),
  UNIQUE KEY `Type_UNIQUE` (`Type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table exercisetypes
-- 

/*!40000 ALTER TABLE `exercisetypes` DISABLE KEYS */;
INSERT INTO `exercisetypes`(`ExerciseTypeID`,`Type`) VALUES
(2,'Find a Picture'),
(4,'Listen and Choose'),
(5,'Listen and Write'),
(1,'Word Choice'),
(3,'Word Matching');
/*!40000 ALTER TABLE `exercisetypes` ENABLE KEYS */;

-- 
-- Definition of languages
-- 

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `LanguageID` int(11) NOT NULL AUTO_INCREMENT,
  `Language` varchar(45) NOT NULL,
  `LanguageISO` varchar(3) NOT NULL,
  PRIMARY KEY (`LanguageID`),
  UNIQUE KEY `Language_UNIQUE` (`Language`),
  UNIQUE KEY `LanguageISO_UNIQUE` (`LanguageISO`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table languages
-- 

/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages`(`LanguageID`,`Language`,`LanguageISO`) VALUES
(1,'Afar','AA'),
(2,'Français','FR'),
(3,'English','EN'),
(4,'Svenska','SV'),
(5,'Deutsch','DE'),
(6,'Nederlands','NL'),
(7,'Arabic','AR');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

-- 
-- Definition of languageskills
-- 

DROP TABLE IF EXISTS `languageskills`;
CREATE TABLE IF NOT EXISTS `languageskills` (
  `LanguageSkillID` int(11) NOT NULL AUTO_INCREMENT,
  `Skill` varchar(45) NOT NULL,
  PRIMARY KEY (`LanguageSkillID`),
  UNIQUE KEY `Skill_UNIQUE` (`Skill`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table languageskills
-- 

/*!40000 ALTER TABLE `languageskills` DISABLE KEYS */;
INSERT INTO `languageskills`(`LanguageSkillID`,`Skill`) VALUES
(2,'Listening'),
(1,'Reading'),
(4,'Translation'),
(3,'Writing');
/*!40000 ALTER TABLE `languageskills` ENABLE KEYS */;

-- 
-- Definition of languageskillshasexercisetypes
-- 

DROP TABLE IF EXISTS `languageskillshasexercisetypes`;
CREATE TABLE IF NOT EXISTS `languageskillshasexercisetypes` (
  `LanguageSkills_LanguageSkillID` int(11) NOT NULL,
  `ExerciseTypes_ExerciseTypeID` int(11) NOT NULL,
  PRIMARY KEY (`LanguageSkills_LanguageSkillID`,`ExerciseTypes_ExerciseTypeID`),
  KEY `fk_LanguageSkill_has_ExerciseType_ExerciseType1_idx` (`ExerciseTypes_ExerciseTypeID`),
  KEY `fk_LanguageSkill_has_ExerciseType_LanguageSkill1_idx` (`LanguageSkills_LanguageSkillID`),
  CONSTRAINT `fk_LanguageSkill_has_ExerciseType_ExerciseType1` FOREIGN KEY (`ExerciseTypes_ExerciseTypeID`) REFERENCES `exercisetypes` (`ExerciseTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_LanguageSkill_has_ExerciseType_LanguageSkill1` FOREIGN KEY (`LanguageSkills_LanguageSkillID`) REFERENCES `languageskills` (`LanguageSkillID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table languageskillshasexercisetypes
-- 

/*!40000 ALTER TABLE `languageskillshasexercisetypes` DISABLE KEYS */;
INSERT INTO `languageskillshasexercisetypes`(`LanguageSkills_LanguageSkillID`,`ExerciseTypes_ExerciseTypeID`) VALUES
(1,1),
(4,1),
(1,2),
(2,4),
(2,5),
(3,5),
(4,5);
/*!40000 ALTER TABLE `languageskillshasexercisetypes` ENABLE KEYS */;

-- 
-- Definition of marks
-- 

DROP TABLE IF EXISTS `marks`;
CREATE TABLE IF NOT EXISTS `marks` (
  `MarkID` int(11) NOT NULL AUTO_INCREMENT,
  `Mark` int(11) NOT NULL,
  `Times_TimeID` int(11) NOT NULL,
  `Users_UserID` int(11) NOT NULL,
  `Exercises_ExerciseID` int(11) NOT NULL,
  PRIMARY KEY (`MarkID`),
  KEY `fk_NOTE_TEMPS1_idx` (`Times_TimeID`),
  KEY `fk_NOTE_UTILISATEUR1_idx` (`Users_UserID`),
  KEY `fk_Marks_Exercises1_idx` (`Exercises_ExerciseID`),
  CONSTRAINT `fk_Marks_Exercises1` FOREIGN KEY (`Exercises_ExerciseID`) REFERENCES `exercises` (`ExerciseID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NOTE_TEMPS1` FOREIGN KEY (`Times_TimeID`) REFERENCES `times` (`TimeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NOTE_UTILISATEUR1` FOREIGN KEY (`Users_UserID`) REFERENCES `users` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table marks
-- 

/*!40000 ALTER TABLE `marks` DISABLE KEYS */;

/*!40000 ALTER TABLE `marks` ENABLE KEYS */;

-- 
-- Definition of periodicgoals
-- 

DROP TABLE IF EXISTS `periodicgoals`;
CREATE TABLE IF NOT EXISTS `periodicgoals` (
  `PeriodicGoalID` int(11) NOT NULL AUTO_INCREMENT,
  `Period` varchar(45) NOT NULL,
  PRIMARY KEY (`PeriodicGoalID`),
  UNIQUE KEY `Period_UNIQUE` (`Period`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table periodicgoals
-- 

/*!40000 ALTER TABLE `periodicgoals` DISABLE KEYS */;
INSERT INTO `periodicgoals`(`PeriodicGoalID`,`Period`) VALUES
(2,'EACH2DAY'),
(3,'EACH3DAY'),
(1,'EACHDAY');
/*!40000 ALTER TABLE `periodicgoals` ENABLE KEYS */;

-- 
-- Definition of resources
-- 

DROP TABLE IF EXISTS `resources`;
CREATE TABLE IF NOT EXISTS `resources` (
  `ResourceID` int(11) NOT NULL AUTO_INCREMENT,
  `TextToken` varchar(255) DEFAULT NULL,
  `ResourceTypes_ResourceTypeID` int(11) NOT NULL,
  `TextTranslations_TextID` int(11) NOT NULL,
  PRIMARY KEY (`ResourceID`),
  KEY `fk_RESSOURCE_RESSOURCE_TYPE1_idx` (`ResourceTypes_ResourceTypeID`),
  KEY `fk_Resource_TextTranslations1_idx` (`TextTranslations_TextID`),
  CONSTRAINT `fk_RESSOURCE_RESSOURCE_TYPE1` FOREIGN KEY (`ResourceTypes_ResourceTypeID`) REFERENCES `resourcetypes` (`ResourceTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Resource_TextTranslations1` FOREIGN KEY (`TextTranslations_TextID`) REFERENCES `texttranslations` (`TextID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table resources
-- 

/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources`(`ResourceID`,`TextToken`,`ResourceTypes_ResourceTypeID`,`TextTranslations_TextID`) VALUES
(1,'C:\\Users\\aden\\Desktop\\Projet_GL2_Afar\\afaroidgl2\\app\\src\\main\\res\\drawable\\information.PNG',2,2),
(2,'C:\\Users\\aden\\Desktop\\Projet_GL2_Afar\\afaroidgl2\\app\\src\\main\\res\\drawable\\heure.PNG',2,3),
(3,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Heure\\horloge.PNG',2,4),
(4,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\L''heure\\horloge.mp3',1,4),
(5,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Heure\\la_minute.PNG',2,5),
(6,'C:\\Users\\aden\\Desktop\\Audio\\L''information\\L''heure\\la_minute.mp3',1,5),
(7,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Heure\\la_seconde.PNG',2,6),
(8,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\L''heure\\la_seconde.mp3',1,6),
(9,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Heure\\maintenant.PNG',2,7),
(10,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\L''heure\\maintenant.mp3',1,7),
(11,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Heure\\le_soir.PNG',2,8),
(12,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\L''heure\\le_soir.mp3',1,8),
(13,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Heure\\le_matin.PNG',2,9),
(14,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\L''heure\\le_matin.mp3',1,9),
(15,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Heure\\heure_voc.PNG',2,10),
(16,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\L''heure\\heure.mp3',1,10),
(17,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\le_calendrier.PNG',2,11),
(18,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\demain.PNG',2,12),
(19,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\demain.mp3',1,12),
(20,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\le_mois.PNG',2,13),
(21,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\le_mois.mp3',1,13),
(22,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\la_date.PNG',2,14),
(23,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\la_date.mp3',1,14),
(24,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\hier.PNG',2,15),
(25,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\hier.mp3',1,15),
(26,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\la_semaine.PNG',2,16),
(27,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\la_semaine.mp3',1,16),
(28,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\le_jour.PNG',2,17),
(29,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\le_jour.mp3',1,17),
(30,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\le_week_end.PNG',2,18),
(31,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\week_end.mp3',1,18),
(32,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\an.PNG',2,19),
(33,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\annee.mp3',1,19),
(34,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\le calendrier\\aujourd_hui.PNG',2,20),
(35,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le calendrier\\aujourd_hui.mp3',1,20),
(36,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\le_nombre.PNG',2,21),
(37,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\deux.PNG',2,22),
(38,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\deux.mp3',1,22),
(39,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\zero.PNG',2,23),
(40,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\zero.mp3',1,23),
(41,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\huit.PNG',2,24),
(42,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\huit.mp3',1,24),
(43,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\six.PNG',2,25),
(44,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\six.mp3',1,25),
(45,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\cent.PNG',2,26),
(46,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\cent.mp3',1,26),
(47,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\un_million.PNG',2,27),
(48,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\un_million.mp3',1,27),
(49,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\dix.PNG',2,28),
(50,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\dix.mp3',1,28),
(51,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\konoyu.PNG',2,29),
(52,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\cinq.mp3',1,29),
(53,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\un.PNG',2,31),
(54,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\un.mp3',1,31),
(55,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\sept.PNG',2,34),
(56,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\sept.mp3',1,34),
(57,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\quatre.PNG',2,38),
(58,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\quatre.mp3',1,38),
(59,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\neuf.PNG',2,43),
(60,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\neuf.mp3',1,43),
(61,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\trois.PNG',2,49),
(62,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\trois.mp3',1,49),
(63,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\les nombres\\mille.PNG',2,56),
(64,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Le nombre\\mille.mp3',1,56),
(65,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\les_poids_et_mesures.PNG',2,57),
(66,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\la_tonne.PNG',2,58),
(67,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Les poids et mesures\\la_tonne.mp3',1,58),
(68,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\le_kilometre.PNG',2,59),
(69,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Les poids et mesures\\un_kilometre.mp3',1,59),
(70,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\le_metre.PNG',2,60),
(71,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Les poids et mesures\\metre.mp3',1,60),
(72,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\le_kilogramme.PNG',2,61),
(73,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Les poids et mesures\\kilogramme.mp3',1,61),
(74,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\mesurer.PNG',2,62),
(75,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Les poids et mesures\\mesurer.mp3',1,62),
(76,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\le_mile.PNG',2,63),
(77,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Les poids et mesures\\mille.mp3',1,63),
(78,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Les poids et mesures\\le_gramme.PNG',2,64),
(79,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\Les poids et mesures\\le_gramme.mp3',1,64),
(80,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\la_carte_du_monde.PNG',2,65),
(81,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\le_nord.PNG',2,66),
(82,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\La carte du mode\\le_nord.mp3',1,66),
(83,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\le_sud.PNG',2,67),
(84,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\La carte du mode\\le_sud.mp3',1,67),
(85,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\ouest.PNG',2,68),
(86,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\La carte du mode\\ouest.mp3',1,68),
(87,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\le_pays.PNG',2,69),
(88,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\La carte du mode\\le_pays.mp3',1,69),
(89,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\est.PNG',2,70),
(90,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\La carte du mode\\est.mp3',1,70),
(91,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\la_nation.PNG',2,71),
(92,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\La carte du mode\\la_nation.mp3',1,71),
(93,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\La carte du monde\\la_capitale.PNG',2,72),
(94,'C:\\Users\\aden\\Desktop\\RC_to_add\\Audio\\L''information\\La carte du mode\\la_capitale.mp3',1,72),
(95,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\asie_et_oceanie.png',2,73),
(96,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\australie.PNG',2,74),
(97,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\australie.mp3',1,74),
(98,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\inde.PNG',2,75),
(99,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\inde.mp3',1,75),
(100,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\la_chine.PNG',2,76),
(101,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\chine.mp3',1,76),
(102,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\la_coree_du_sud.PNG',2,77),
(103,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\coree_du_sud.mp3',1,77),
(104,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\la_thailande.PNG',2,78),
(105,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\thailande.mp3',1,78),
(106,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\le_japon.PNG',2,79),
(107,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\japon.mp3',1,79),
(108,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\le_vietnam.PNG',2,80),
(109,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\vietnam.mp3',1,80),
(110,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\les_maldives.PNG',2,81),
(111,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\maldiiv.mp3',1,81),
(112,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\singapour.PNG',2,82),
(113,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\Asie et Oceanie\\singapour.mp3',1,82),
(114,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\afrique.PNG',2,84),
(115,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\afrique_du_sud.PNG',2,85),
(116,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\afrique_du_sud.mp3',1,85),
(117,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\algerie.PNG',2,86),
(118,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\algerie.mp3',1,86),
(119,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\egypte.PNG',2,87),
(120,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\masri.mp3',1,87),
(121,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\ethiopie.PNG',2,88),
(122,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\ethiopie.mp3',1,88),
(123,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\la_somalie.PNG',2,89),
(124,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\somalie.mp3',1,89),
(125,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\la_tunisie.PNG',2,90),
(126,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\tunisie.mp3',1,90),
(127,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\le_congo.PNG',2,91),
(128,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\congo.mp3',1,91),
(129,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\le_maroc.PNG',2,92),
(130,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\maroc.mp3',1,92),
(131,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\le_niger.PNG',2,93),
(132,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\niger.mp3',1,93),
(133,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\madagascar.PNG',2,94),
(134,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Afrique\\madagascar.mp3',1,94),
(135,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\amerique_du_nord_et_centrale.png',2,95),
(136,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\hawaii.PNG',2,96),
(137,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\hawai.mp3',1,96),
(138,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\le_canada.PNG',2,97),
(139,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\canada.mp3',1,97),
(140,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\le_groenland.PNG',2,98),
(141,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\groenland.mp3',1,98),
(142,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\le_mexique.PNG',2,99),
(143,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\mexique.mp3',1,99),
(144,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\les_etats_unis_amerique.PNG',2,100),
(145,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\L''Amerique du Nord et Centrale\\etats_unis_amerique.mp3',1,100),
(146,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\amerique_du_sud.PNG',2,101),
(147,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\argentine.PNG',2,102),
(148,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\argentine.mp3',1,102),
(149,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\le_bresil.PNG',2,103),
(150,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\bresil.mp3',1,103),
(151,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\le_perou.PNG',2,104),
(152,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\perou.mp3',1,104),
(153,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\le_venezuela.PNG',2,105),
(154,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Amerique du Sud\\venezuela.mp3',1,105),
(155,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\europe.PNG',2,106),
(156,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\italie.PNG',2,107),
(157,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\italie.mp3',1,107),
(158,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_moldavie.PNG',2,108),
(159,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_moldavie.mp3',1,108),
(160,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_roumanie.PNG',2,109),
(161,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_roumanie.mp3',1,109),
(162,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_turquie.PNG',2,110),
(163,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_turquie.mp3',1,110),
(164,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_federation_de_russie.PNG',2,111),
(165,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_federation_de_russie.mp3',1,111),
(166,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_france.PNG',2,112),
(167,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\la_france.mp3',1,112),
(168,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\allemagne.PNG',2,113),
(169,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\allemagne.mp3',1,113),
(170,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\autriche.PNG',2,114),
(171,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\autriche.mp3',1,114),
(172,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\monaco.PNG',2,115),
(173,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\monaco.mp3',1,115),
(174,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\le_royaume_uni.PNG',2,116),
(175,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\le_royaume_uni.mp3',1,116),
(176,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\espagne.PNG',2,117),
(177,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\Information\\l''Europe\\espagne.mp3',1,117),
(178,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\les_temps_libres.PNG',2,118),
(179,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\la_plage_topic.PNG',2,119),
(180,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\prendre_un_bain_de_soleil.PNG',2,120),
(181,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\prendre_un_bain_de_soleil.mp3',1,120),
(182,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\le_parasol.PNG',2,121),
(183,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\le_parasol.mp3',1,121),
(184,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\la_mer.PNG',2,122),
(185,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\la_mer.mp3',1,122),
(186,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\le_sable.PNG',2,123),
(187,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\le_sable.mp3',1,123),
(188,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\la_vague.PNG',2,124),
(189,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\la_plage\\la_vague.mp3',1,124),
(190,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\le_concert.PNG',2,125),
(191,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\la_chanson.PNG',2,126),
(192,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\la_chanson.mp3',1,126),
(193,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\la_dance.PNG',2,127),
(194,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\la_dance.mp3',1,127),
(195,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\le_chanteur.PNG',2,128),
(196,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\le_chanteur.mp3',1,128),
(197,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\les_paroles.PNG',2,129),
(198,'C:\\Users\\aden\\Desktop\\RC_to_add\\Execics\\LE TEMPS LIBRE              1\\le_concert\\les_paroles.mp3',1,129);
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;

-- 
-- Definition of resourcetypes
-- 

DROP TABLE IF EXISTS `resourcetypes`;
CREATE TABLE IF NOT EXISTS `resourcetypes` (
  `ResourceTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  PRIMARY KEY (`ResourceTypeID`),
  UNIQUE KEY `Name_UNIQUE` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table resourcetypes
-- 

/*!40000 ALTER TABLE `resourcetypes` DISABLE KEYS */;
INSERT INTO `resourcetypes`(`ResourceTypeID`,`Name`) VALUES
(1,'AUDIO'),
(2,'IMAGE'),
(3,'TEXT');
/*!40000 ALTER TABLE `resourcetypes` ENABLE KEYS */;

-- 
-- Definition of subthemes
-- 

DROP TABLE IF EXISTS `subthemes`;
CREATE TABLE IF NOT EXISTS `subthemes` (
  `SubThemeID` int(11) NOT NULL AUTO_INCREMENT,
  `Themes_ThemeID` int(11) NOT NULL,
  `Resources_ResourceID` int(11) NOT NULL,
  PRIMARY KEY (`SubThemeID`),
  KEY `fk_SOUS_THEME_THEME1_idx` (`Themes_ThemeID`),
  KEY `fk_SubThemes_Resource1_idx` (`Resources_ResourceID`),
  CONSTRAINT `fk_SOUS_THEME_THEME1` FOREIGN KEY (`Themes_ThemeID`) REFERENCES `themes` (`ThemeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_SubThemes_Resource1` FOREIGN KEY (`Resources_ResourceID`) REFERENCES `resources` (`ResourceID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table subthemes
-- 

/*!40000 ALTER TABLE `subthemes` DISABLE KEYS */;
INSERT INTO `subthemes`(`SubThemeID`,`Themes_ThemeID`,`Resources_ResourceID`) VALUES
(1,1,2),
(2,1,17),
(3,1,36),
(4,1,65),
(5,1,80),
(6,1,95),
(7,1,114),
(8,1,135),
(9,1,146),
(10,1,155),
(11,2,179),
(12,2,190);
/*!40000 ALTER TABLE `subthemes` ENABLE KEYS */;

-- 
-- Definition of texttranslations
-- 

DROP TABLE IF EXISTS `texttranslations`;
CREATE TABLE IF NOT EXISTS `texttranslations` (
  `TextTranslationID` int(11) NOT NULL AUTO_INCREMENT,
  `TextID` int(11) NOT NULL,
  `Languages_LanguageID` int(11) NOT NULL,
  `Translation` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`TextTranslationID`),
  UNIQUE KEY `uc_TextTranslations` (`TextID`,`Languages_LanguageID`),
  KEY `fk_TextTranslations_Languages1_idx` (`Languages_LanguageID`),
  CONSTRAINT `fk_TextTranslations_Languages1` FOREIGN KEY (`Languages_LanguageID`) REFERENCES `languages` (`LanguageID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=658 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table texttranslations
-- 

/*!40000 ALTER TABLE `texttranslations` DISABLE KEYS */;
INSERT INTO `texttranslations`(`TextTranslationID`,`TextID`,`Languages_LanguageID`,`Translation`) VALUES
(1,2,1,''),
(2,2,2,'L''information'),
(3,2,3,'Information'),
(4,2,4,''),
(5,2,5,''),
(6,2,6,''),
(7,2,7,''),
(8,3,1,''),
(9,3,2,'L''heure'),
(10,3,3,'Time'),
(11,3,4,''),
(12,3,5,''),
(13,3,6,''),
(14,3,7,''),
(15,4,2,'L''horloge'),
(16,4,1,'Uddurtá'),
(17,4,3,'Clock'),
(18,4,5,'Wanduhr'),
(19,4,7,'ساعة حائط'),
(20,4,4,'Klocka'),
(21,5,2,'La minute'),
(22,5,1,'Miniti'),
(23,5,3,'Minute'),
(24,5,5,'Minute'),
(25,5,7,' دقيقة'),
(26,5,4,'Minut'),
(27,6,2,'La seconde'),
(28,6,1,'Segondi'),
(29,6,3,'Second'),
(30,6,5,'Sekunde'),
(31,6,7,'ثانية'),
(32,6,4,'Sekund'),
(33,7,2,'Maintenant'),
(34,7,1,'Away'),
(35,7,3,'Now'),
(36,7,5,'Jetzt'),
(37,7,7,'الآن'),
(38,7,4,'Nu'),
(39,8,2,'Le soir '),
(40,8,1,'Carri Bara'),
(41,8,3,'Evening'),
(42,8,5,'Abend'),
(43,8,7,'مساء'),
(44,8,4,'Kväll'),
(45,9,2,'Le matin'),
(46,9,1,'Saaku'),
(47,9,3,'Morning'),
(48,9,5,'Morgen'),
(49,9,7,'صباح'),
(50,9,4,'Morgon'),
(51,10,2,'L''heure'),
(52,10,1,'Waqlà'),
(53,10,3,'Hour'),
(54,10,5,'Stunde'),
(55,10,7,'ساعة'),
(56,10,4,'Timme'),
(57,11,1,''),
(58,11,2,'Le calendrier'),
(59,11,3,'Calendar'),
(60,11,4,''),
(61,11,5,''),
(62,11,6,''),
(63,11,7,''),
(64,12,2,'Demain'),
(65,12,1,'Beera'),
(66,12,3,'Tomorrow'),
(67,12,5,'Morgen'),
(68,12,7,'غدا'),
(69,12,4,'Imorgon'),
(70,13,2,'Le mois'),
(71,13,1,'Alsa'),
(72,13,3,'Month'),
(73,13,5,'Monat'),
(74,13,7,'شهر'),
(75,13,4,'Månad'),
(76,14,2,'La date'),
(77,14,1,'Alsi gidé'),
(78,14,3,'Date'),
(79,14,5,'Datum'),
(80,14,7,'تاريخ'),
(81,14,4,'Datum'),
(82,15,2,'Hier'),
(83,15,1,'Kimal'),
(84,15,3,'Yesterday'),
(85,15,5,'Gestern'),
(86,15,7,'أمس'),
(87,15,4,'Igår'),
(88,16,2,'La semaine'),
(89,16,1,'Bacuuró'),
(90,16,3,'Week'),
(91,16,5,'wochenende'),
(92,16,7,'أسبوع'),
(93,16,4,'Vecka'),
(94,17,2,'Le jour'),
(95,17,1,'Laqo'),
(96,17,3,'Day'),
(97,17,5,'Tag'),
(98,17,7,'يوم'),
(99,17,4,'Dag'),
(100,18,2,'Le week-end '),
(101,18,1,'Bacuuroh ellacbo'),
(102,18,3,'Weekend'),
(103,18,5,'Wochenende'),
(104,18,7,'نهاية الأسبوع'),
(105,18,4,'Helg'),
(106,19,2,'L''an'),
(107,19,1,'Liggida'),
(108,19,3,'Year'),
(109,19,5,'Jahr'),
(110,19,7,'عام'),
(111,19,4,'År'),
(112,20,2,'Aujourd''hui'),
(113,20,1,'A saaku'),
(114,20,3,'Today'),
(115,20,5,'Heute'),
(116,20,7,'اليوم'),
(117,20,4,'Idag'),
(118,21,1,''),
(119,21,2,'Le nombre'),
(120,21,3,'Number'),
(121,21,4,''),
(122,21,5,''),
(123,21,6,''),
(124,21,7,''),
(125,22,2,'Deux'),
(126,22,1,'Lammaya'),
(127,22,3,'Two'),
(128,22,5,'Zwei'),
(129,22,7,'اثنان'),
(130,22,4,'Två'),
(131,23,2,'Zero'),
(132,23,1,'Foyyá'),
(133,23,3,'Zero'),
(134,23,5,'Null'),
(135,23,7,'صفر'),
(136,23,4,'Noll'),
(137,24,2,'Huit'),
(138,24,1,'Bacaara'),
(139,24,3,'Eight'),
(140,24,5,'Acht'),
(141,24,7,'ثمانية'),
(142,24,4,'Åtta'),
(143,25,2,'Six'),
(144,25,1,'Laceyi'),
(145,25,3,'Six'),
(146,25,5,'Sechs'),
(147,25,7,'ستة'),
(148,25,4,'Sex'),
(149,26,2,'Cent'),
(150,26,1,'Bool'),
(151,26,3,'Hundred'),
(152,26,5,'hundert'),
(153,26,7,'مائة'),
(154,26,4,'Hundra'),
(155,27,2,'Million'),
(156,27,1,'Malyuun'),
(157,27,3,'Million'),
(158,27,5,'Milonen'),
(159,27,7,'مليون'),
(160,27,4,'Miljon'),
(161,28,2,'Dix'),
(162,28,1,'Tabana'),
(163,28,3,'Ten'),
(164,28,5,'Zehn'),
(165,28,7,'عَشَرَةٌ'),
(166,28,4,'Tio'),
(167,29,2,'Cinq'),
(168,29,1,'Konoyu'),
(169,29,3,'Five'),
(170,29,5,'Fünf'),
(171,29,7,'خمسة'),
(172,29,4,'Fem'),
(173,31,2,'Un'),
(174,31,1,'Eneki'),
(175,31,3,'One'),
(176,31,5,'Eins'),
(177,31,7,'واحد'),
(178,31,4,'En'),
(179,34,2,'Sept'),
(180,34,1,'Malciini'),
(181,34,3,'Seven'),
(182,34,5,'Sieben'),
(183,34,7,'سبعة'),
(184,34,4,'Sju'),
(185,38,2,'Quatre'),
(186,38,1,'Fareyi'),
(187,38,3,'Four'),
(188,38,5,'Vier'),
(189,38,7,'أربعة'),
(190,38,4,'Fyra'),
(191,43,2,'Neuf'),
(192,43,1,'Sagaala'),
(193,43,3,'Nine'),
(194,43,5,'Neun'),
(195,43,7,'تسعة'),
(196,43,4,'Nio'),
(197,49,2,'Trois'),
(198,49,1,'Sidocu'),
(199,49,3,'Three'),
(200,49,5,'Drei'),
(201,49,7,'ثلاثة'),
(202,49,4,'Tre'),
(203,56,2,'Mille'),
(204,56,1,'Alfi'),
(205,56,3,'Thousand'),
(206,56,5,'Tausend'),
(207,56,7,'ألف'),
(208,56,4,'Tusen'),
(209,57,1,''),
(210,57,2,'Les poids et mesures'),
(211,57,3,'The weights and measures'),
(212,57,4,''),
(213,57,5,''),
(214,57,6,''),
(215,57,7,''),
(216,58,2,'La tonne'),
(217,58,1,'Tonni'),
(218,58,3,'Ton'),
(219,58,5,'Ton'),
(220,58,7,'طن'),
(221,58,4,'Ett ton'),
(222,59,2,'Un kilomètre'),
(223,59,1,'Kiilomitri'),
(224,59,3,'One kilometer'),
(225,59,5,'Ein kilometer'),
(226,59,7,'كيلو متر'),
(227,59,4,'En kilometer'),
(228,60,2,'Un mètre'),
(229,60,1,'Mitri'),
(230,60,3,'One meter'),
(231,60,5,'Ein meter'),
(232,60,7,'متر'),
(233,60,4,'En meter'),
(234,61,2,'Le kilogramme'),
(235,61,1,'Kiilogram'),
(236,61,3,'Kilogram'),
(237,61,5,'Kilogram'),
(238,61,7,'كيلو جرام'),
(239,61,4,'Ett kilogram'),
(240,62,2,'Mesurer'),
(241,62,1,'Acussuliyya'),
(242,62,3,'Measure'),
(243,62,5,'Messen'),
(244,62,7,'يقيس'),
(245,62,4,'Mått'),
(246,63,2,'Le Mile'),
(247,63,1,'Alfi'),
(248,63,3,'Mile'),
(249,63,5,'Meile'),
(250,63,7,'ميل'),
(251,63,4,'En mil'),
(252,64,2,'Le gramme'),
(253,64,1,'Gram'),
(254,64,3,'Gram'),
(255,64,5,'Gramm'),
(256,64,7,'جرام'),
(257,64,4,'Gram'),
(258,65,1,''),
(259,65,2,'La carte du monde'),
(260,65,3,'World map'),
(261,65,4,''),
(262,65,5,''),
(263,65,6,''),
(264,65,7,''),
(265,66,2,'Le nord'),
(266,66,1,'kilbata'),
(267,66,3,'North'),
(268,66,5,'Norden'),
(269,66,7,'شمال'),
(270,66,4,'Nord'),
(271,67,2,'Le sud '),
(272,67,1,'Gabbi'),
(273,67,3,'South'),
(274,67,5,'sÜden'),
(275,67,7,'جنوب'),
(276,67,4,'Syd'),
(277,68,2,'L''ouest'),
(278,68,1,'Mikrifi'),
(279,68,3,'West'),
(280,68,5,'Westen'),
(281,68,7,'غرب'),
(282,68,4,'Väst'),
(283,69,2,'Le pays'),
(284,69,1,'Baaxó'),
(285,69,3,'Country'),
(286,69,5,'Land'),
(287,69,7,'بلد'),
(288,69,4,'Land'),
(289,70,2,'L''est'),
(290,70,1,'Mikki'),
(291,70,3,'East'),
(292,70,5,'Osten'),
(293,70,7,'شرق'),
(294,70,4,'öst'),
(295,71,2,'La nation'),
(296,71,1,'Agata'),
(297,71,3,'Nation'),
(298,71,5,'Nation'),
(299,71,7,'أمة'),
(300,71,4,'Nation'),
(301,72,2,'La capitale'),
(302,72,1,'Qaasimá'),
(303,72,3,'Capital  '),
(304,72,5,'Hauptstadt'),
(305,72,7,'عاصمة'),
(306,72,4,'huvudstad'),
(307,73,1,''),
(308,73,2,'L''asie et l''océanie'),
(309,73,3,'Asia and oceania'),
(310,73,4,''),
(311,73,5,''),
(312,73,6,''),
(313,73,7,''),
(314,74,2,'Australie'),
(315,74,1,'Ostraliya'),
(316,74,3,'Australia'),
(317,74,5,'Australien'),
(318,74,7,'أستراليا'),
(319,74,4,'Australia'),
(320,75,2,'L''Inde'),
(321,75,1,'Hindiya'),
(322,75,3,'India'),
(323,75,5,'Indien'),
(324,75,7,'الهند'),
(325,75,4,'Indien'),
(326,76,2,'Chine'),
(327,76,1,'Shiina'),
(328,76,3,'China'),
(329,76,5,'China'),
(330,76,7,'الصين'),
(331,76,4,'Kina'),
(332,77,2,'La corée du sud'),
(333,77,1,'Gabbi koore'),
(334,77,3,'South Korea'),
(335,77,5,'Süd korea'),
(336,77,7,'كوريا الجنوبية'),
(337,77,4,'Sydkorea'),
(338,78,2,'La thaïlande'),
(339,78,1,'Taylanda'),
(340,78,3,'Thailand'),
(341,78,5,'Thailand'),
(342,78,7,'تيلاند'),
(343,78,4,'Thailand'),
(344,79,2,'Le japon'),
(345,79,1,'Yabbaan'),
(346,79,3,'Japan'),
(347,79,5,'Japan'),
(348,79,7,'اليابان'),
(349,79,4,'Japan'),
(350,80,2,'Le vietnam'),
(351,80,1,'Vietnaam'),
(352,80,3,'Vietnam'),
(353,80,5,'Vieatnam'),
(354,80,7,'فيتنام'),
(355,80,4,'Vietnamn'),
(356,81,2,'Les maldives'),
(357,81,1,'Maldiiv'),
(358,81,3,'Maldives'),
(359,81,5,'Maledieven'),
(360,81,7,'الملديف'),
(361,81,4,'Maldiverna'),
(362,82,2,'Le singapour'),
(363,82,1,'Singapuur'),
(364,82,3,'Singapore'),
(365,82,5,'Singapur'),
(366,82,7,'سنغافورة'),
(367,82,4,'singapore'),
(368,83,1,''),
(369,83,2,'L''afrique'),
(370,83,3,'Africa'),
(371,83,4,''),
(372,83,5,''),
(373,83,6,''),
(374,83,7,''),
(375,84,1,''),
(376,84,2,'L''afrique'),
(377,84,3,'Africa'),
(378,84,4,''),
(379,84,5,''),
(380,84,6,''),
(381,84,7,''),
(382,85,2,'L''afrique du sud'),
(383,85,1,'Gabbi afriika'),
(384,85,3,'South africa'),
(385,85,5,'Süd afrika'),
(386,85,7,'جنوب أفريقيا'),
(387,85,4,'Sydafrika'),
(388,86,2,'L''algérie'),
(389,86,1,'Aljeer'),
(390,86,3,'Algeria'),
(391,86,5,'Algerien'),
(392,86,7,'الجزائر'),
(393,86,4,'Algeriet'),
(394,87,2,'L''egypte'),
(395,87,1,'Masri'),
(396,87,3,'Egypt'),
(397,87,5,'Ägypten'),
(398,87,7,'مصر'),
(399,87,4,'Egypten'),
(400,88,2,'L''Ethiopie'),
(401,88,1,'Etiopiya'),
(402,88,3,'Ethiopia'),
(403,88,5,'Äthiopien'),
(404,88,7,'إثيوبيا'),
(405,88,4,'Etiopien'),
(406,89,2,'La somalie'),
(407,89,1,'Soomaliya'),
(408,89,3,'Somalia'),
(409,89,5,'Somalien'),
(410,89,7,'الصومال'),
(411,89,4,'Somalia'),
(412,90,2,'La tunisie'),
(413,90,1,'Tuunis'),
(414,90,3,'Tunisia'),
(415,90,5,'Tunesien'),
(416,90,7,'تونس'),
(417,90,4,'Tunisien'),
(418,91,2,'Le congo'),
(419,91,1,'Kongo'),
(420,91,3,'Congo'),
(421,91,5,'Congo'),
(422,91,7,'الكونغو'),
(423,91,4,'Kongo'),
(424,92,2,'Le maroc'),
(425,92,1,'Marok'),
(426,92,3,'Morocco'),
(427,92,5,'Marokko'),
(428,92,7,'المغرب'),
(429,92,4,'Marocko'),
(430,93,2,'Le niger'),
(431,93,1,'Nijer'),
(432,93,3,'Niger'),
(433,93,5,'Nieger'),
(434,93,7,'النيجر'),
(435,93,4,'Niger'),
(436,94,2,'Le madagascar'),
(437,94,1,'Madagaskaar'),
(438,94,3,'Madagascar'),
(439,94,5,'Madagaskar'),
(440,94,7,'مد غسشقر'),
(441,94,4,'Madagaskar'),
(442,95,1,''),
(443,95,2,'L''amérique du nord et centrale'),
(444,95,3,'North and central america'),
(445,95,4,''),
(446,95,5,''),
(447,95,6,''),
(448,95,7,''),
(449,96,2,'Hawaï'),
(450,96,1,'Hawaay'),
(451,96,3,'Hawaii'),
(452,96,5,'Hawai'),
(453,96,7,'?????'),
(454,96,4,'Hawai'),
(455,97,2,'Le canada'),
(456,97,1,'Kanada'),
(457,97,3,'Canada'),
(458,97,5,'Kanada'),
(459,97,7,'????'),
(460,97,4,'Kanada'),
(461,98,2,'Le groenland'),
(462,98,1,'Groenland'),
(463,98,3,'Greenland'),
(464,98,5,'Grönland'),
(465,98,7,'????????'),
(466,98,4,'Grönland'),
(467,99,2,'Le mexique'),
(468,99,1,'Meksiko'),
(469,99,3,'Mexico'),
(470,99,5,'Mexico'),
(471,99,7,'??????'),
(472,99,4,'Mexiko'),
(473,100,2,'Etats-unis d’amerique'),
(474,100,1,'Tengele ameerika'),
(475,100,3,'United States of Amerika'),
(476,100,5,'vereinigte Starten'),
(477,100,7,'??? ????? ??????? ?????????'),
(478,100,4,'Amerikas Förenta Stater\n'),
(479,101,1,''),
(480,101,2,'L''amérique du sud'),
(481,101,3,'South America'),
(482,101,4,''),
(483,101,5,''),
(484,101,6,''),
(485,101,7,''),
(486,102,2,'L''argentine'),
(487,102,1,'Arjentiin'),
(488,102,3,'Argentina'),
(489,102,5,'Argentienien'),
(490,102,7,'?????????'),
(491,102,4,'Argentina'),
(492,103,2,'Le brésil'),
(493,103,1,'Braziil'),
(494,103,3,'Brazil'),
(495,103,5,'Brasilien'),
(496,103,7,'??????'),
(497,103,4,'Brasilien'),
(498,104,2,'Le pérou'),
(499,104,1,'Beeru'),
(500,104,3,'Peru'),
(501,104,5,'Peru'),
(502,104,7,'????'),
(503,104,4,'Peru'),
(504,105,2,'Le venezuela'),
(505,105,1,'Venezuella'),
(506,105,3,'Venezuella'),
(507,105,5,'Venezuela'),
(508,105,7,'???????'),
(509,105,4,'Venezuella'),
(510,106,1,''),
(511,106,2,'L''europe'),
(512,106,3,'Europe'),
(513,106,4,''),
(514,106,5,''),
(515,106,6,''),
(516,106,7,''),
(517,107,2,'L''italie'),
(518,107,1,'Itaaliya'),
(519,107,3,'Italy'),
(520,107,5,'Italien'),
(521,107,7,'???????'),
(522,107,4,'Italia'),
(523,108,2,'La moldavie'),
(524,108,1,'Moldaaviya'),
(525,108,3,'Moldova'),
(526,108,5,'Maledivien'),
(527,108,7,'???????'),
(528,108,4,'Maldova'),
(529,109,2,'La roumanie'),
(530,109,1,'Rumaaniya'),
(531,109,3,'Romania'),
(532,109,5,'Romenien'),
(533,109,7,'???????'),
(534,109,4,'Rumänien'),
(535,110,2,'La turquie'),
(536,110,1,'Turkiya'),
(537,110,3,'Turkey'),
(538,110,5,'Türkei'),
(539,110,7,'?????'),
(540,110,4,'Turkiet'),
(541,111,2,'La fédération de Russie'),
(542,111,1,'Tengele ruusiya'),
(543,111,3,'Russian Federation'),
(544,111,5,'Russland'),
(545,111,7,'??????? ?????? ?????????'),
(546,111,4,'Ryssland'),
(547,112,2,'La france'),
(548,112,1,'Faransa'),
(549,112,3,'France'),
(550,112,5,'Frankreich'),
(551,112,7,'??????'),
(552,112,4,'Frankrike'),
(553,113,2,'L''allemagne'),
(554,113,1,'Alemaaniya'),
(555,113,3,'Germany'),
(556,113,5,'Deutschland'),
(557,113,7,'???????'),
(558,113,4,'Tyskland'),
(559,114,2,'L''autriche'),
(560,114,1,'Otrish'),
(561,114,3,'Austria'),
(562,114,5,'Österreich'),
(563,114,7,'??????'),
(564,114,4,'Österriket'),
(565,115,2,'Monaco'),
(566,115,1,'Monakko'),
(567,115,3,'Monaco'),
(568,115,5,'Monako'),
(569,115,7,'??????'),
(570,115,4,'Monaco'),
(571,116,2,'Le royaume-unie'),
(572,116,1,'Tengele Ingiliiz mamlakata'),
(573,116,3,'United Kingdom'),
(574,116,5,'Grossbritanien'),
(575,116,7,'??????? ???????'),
(576,116,4,'Storbrittannien'),
(577,117,2,'L''espagne'),
(578,117,1,'Espaaniya'),
(579,117,3,'Spain'),
(580,117,5,'Spanien'),
(581,117,7,'???????'),
(582,117,4,'Spanien'),
(583,118,1,''),
(584,118,2,'Les temps libres'),
(585,118,3,'Free times'),
(586,118,4,''),
(587,118,5,''),
(588,118,6,''),
(589,118,7,''),
(590,119,1,''),
(591,119,2,'La plage'),
(592,119,3,'Beach'),
(593,119,4,''),
(594,119,5,''),
(595,119,6,''),
(596,119,7,''),
(597,120,2,'Prendre un bain de solei'),
(598,120,1,'Ayrit'),
(599,120,3,'Sunbathe'),
(600,120,5,'Die Sonne'),
(601,120,7,'?????'),
(602,120,4,'Sola'),
(603,121,2,'Le parasol'),
(604,121,1,'Agoobar'),
(605,121,3,'Shade'),
(606,121,5,'Der Schatten'),
(607,121,7,' ?????'),
(608,121,4,'Skugga'),
(609,122,2,'La mer'),
(610,122,1,'Bada'),
(611,122,3,'Sea'),
(612,122,5,'Das Meer'),
(613,122,7,'?????'),
(614,122,4,'Hav'),
(615,123,2,'Le sable'),
(616,123,1,'Buuré'),
(617,123,3,'Sand'),
(618,123,5,'Der Sand'),
(619,123,7,'???'),
(620,123,4,'Sand'),
(621,124,2,'La vague'),
(622,124,1,'Haffó'),
(623,124,3,'Wave'),
(624,124,5,'Die Welle'),
(625,124,7,'????'),
(626,124,4,'Våg'),
(627,125,1,''),
(628,125,2,'Le concert'),
(629,125,3,'Concert'),
(630,125,4,''),
(631,125,5,''),
(632,125,6,''),
(633,125,7,''),
(634,126,2,'La chanson'),
(635,126,1,'Gada'),
(636,126,3,'Song'),
(637,126,5,'Das lied'),
(638,126,7,'?????'),
(639,126,4,'Sång'),
(640,127,2,'La dance'),
(641,127,1,'Dansi'),
(642,127,3,'Dance'),
(643,127,5,'Der Tanz'),
(644,127,7,'? ??'),
(645,127,4,'Dans'),
(646,128,2,'Le chanteur'),
(647,128,1,'Gadabe'),
(648,128,3,'Singer'),
(649,128,5,'Der Saenger'),
(650,128,7,'???? ?????'),
(651,128,4,'Sångare'),
(652,129,2,'Les paroles'),
(653,129,1,'Qangor'),
(654,129,3,'Words'),
(655,129,5,'Das Wort'),
(656,129,7,'????? ?????'),
(657,129,4,'Ord');
/*!40000 ALTER TABLE `texttranslations` ENABLE KEYS */;

-- 
-- Definition of themes
-- 

DROP TABLE IF EXISTS `themes`;
CREATE TABLE IF NOT EXISTS `themes` (
  `ThemeID` int(11) NOT NULL AUTO_INCREMENT,
  `Resources_ResourceID` int(11) NOT NULL,
  PRIMARY KEY (`ThemeID`),
  KEY `fk_Themes_Resource1_idx` (`Resources_ResourceID`),
  CONSTRAINT `fk_Themes_Resource1` FOREIGN KEY (`Resources_ResourceID`) REFERENCES `resources` (`ResourceID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table themes
-- 

/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes`(`ThemeID`,`Resources_ResourceID`) VALUES
(1,1),
(2,178);
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;

-- 
-- Definition of times
-- 

DROP TABLE IF EXISTS `times`;
CREATE TABLE IF NOT EXISTS `times` (
  `TimeID` int(11) NOT NULL AUTO_INCREMENT,
  `TDay` int(11) DEFAULT NULL,
  `TMonth` int(11) DEFAULT NULL,
  `TYear` int(11) DEFAULT NULL,
  PRIMARY KEY (`TimeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table times
-- 

/*!40000 ALTER TABLE `times` DISABLE KEYS */;

/*!40000 ALTER TABLE `times` ENABLE KEYS */;

-- 
-- Definition of users
-- 

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Login` varchar(45) NOT NULL,
  `Languages_LanguageID` int(11) NOT NULL,
  `PeriodicGoals_PeriodicGoalID` int(11) NOT NULL,
  `ConnectionNb` int(11) NOT NULL DEFAULT '0',
  `PW` varchar(255) NOT NULL,
  `TimeSpent` int(11) DEFAULT NULL,
  `IsSynchroActivated` tinyint(4) NOT NULL,
  `Difficulties_DifficultyID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Login_UNIQUE` (`Login`),
  KEY `fk_User_NativeLanguage1_idx` (`Languages_LanguageID`),
  KEY `fk_User_PeriodicGoal1_idx` (`PeriodicGoals_PeriodicGoalID`),
  KEY `fk_Users_Difficulties1_idx` (`Difficulties_DifficultyID`),
  CONSTRAINT `fk_User_NativeLanguage1` FOREIGN KEY (`Languages_LanguageID`) REFERENCES `languages` (`LanguageID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_PeriodicGoal1` FOREIGN KEY (`PeriodicGoals_PeriodicGoalID`) REFERENCES `periodicgoals` (`PeriodicGoalID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Users_Difficulties1` FOREIGN KEY (`Difficulties_DifficultyID`) REFERENCES `difficulties` (`DifficultyID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table users
-- 

/*!40000 ALTER TABLE `users` DISABLE KEYS */;

/*!40000 ALTER TABLE `users` ENABLE KEYS */;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


-- Dump completed on 2017-08-25 17:13:50
-- Total time: 0:0:0:1:250 (d:h:m:s:ms)
